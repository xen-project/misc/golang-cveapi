# BadRequestDetailsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Msg** | Pointer to **string** | Invalid value | [optional] 
**Param** | Pointer to **string** | The name of the parameter with the error | [optional] 
**Location** | Pointer to **string** | The location of the parameter | [optional] 

## Methods

### NewBadRequestDetailsInner

`func NewBadRequestDetailsInner() *BadRequestDetailsInner`

NewBadRequestDetailsInner instantiates a new BadRequestDetailsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBadRequestDetailsInnerWithDefaults

`func NewBadRequestDetailsInnerWithDefaults() *BadRequestDetailsInner`

NewBadRequestDetailsInnerWithDefaults instantiates a new BadRequestDetailsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMsg

`func (o *BadRequestDetailsInner) GetMsg() string`

GetMsg returns the Msg field if non-nil, zero value otherwise.

### GetMsgOk

`func (o *BadRequestDetailsInner) GetMsgOk() (*string, bool)`

GetMsgOk returns a tuple with the Msg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMsg

`func (o *BadRequestDetailsInner) SetMsg(v string)`

SetMsg sets Msg field to given value.

### HasMsg

`func (o *BadRequestDetailsInner) HasMsg() bool`

HasMsg returns a boolean if a field has been set.

### GetParam

`func (o *BadRequestDetailsInner) GetParam() string`

GetParam returns the Param field if non-nil, zero value otherwise.

### GetParamOk

`func (o *BadRequestDetailsInner) GetParamOk() (*string, bool)`

GetParamOk returns a tuple with the Param field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParam

`func (o *BadRequestDetailsInner) SetParam(v string)`

SetParam sets Param field to given value.

### HasParam

`func (o *BadRequestDetailsInner) HasParam() bool`

HasParam returns a boolean if a field has been set.

### GetLocation

`func (o *BadRequestDetailsInner) GetLocation() string`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *BadRequestDetailsInner) GetLocationOk() (*string, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *BadRequestDetailsInner) SetLocation(v string)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *BadRequestDetailsInner) HasLocation() bool`

HasLocation returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


