# \CVEIDApi

All URIs are relative to *https://cveawg.mitre.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CveIdGetFiltered**](CVEIDApi.md#CveIdGetFiltered) | **Get** /cve-id | Retrieves information about CVE IDs after applying the query parameters as filters (accessible to all registered users)
[**CveIdGetSingle**](CVEIDApi.md#CveIdGetSingle) | **Get** /cve-id/{id} | Retrieves information about the specified CVE ID (accessible to all users)
[**CveIdRangeCreate**](CVEIDApi.md#CveIdRangeCreate) | **Post** /cve-id-range/{year} | Creates a CVE-ID-Range for the specified year (accessible to Secretariat)
[**CveIdReserve**](CVEIDApi.md#CveIdReserve) | **Post** /cve-id | Reserves CVE IDs for the organization provided in the short_name query parameter (accessible to CNAs and Secretariat)
[**CveIdUpdateSingle**](CVEIDApi.md#CveIdUpdateSingle) | **Put** /cve-id/{id} | Updates information related to the specified CVE ID (accessible to CNAs and Secretariat)



## CveIdGetFiltered

> ListCveIdsResponse CveIdGetFiltered(ctx).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).State(state).CveIdYear(cveIdYear).TimeReservedLt(timeReservedLt).TimeReservedGt(timeReservedGt).TimeModifiedLt(timeModifiedLt).TimeModifiedGt(timeModifiedGt).Page(page).Execute()

Retrieves information about CVE IDs after applying the query parameters as filters (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    state := "state_example" // string | Filter by state  (optional)
    cveIdYear := "cveIdYear_example" // string | Filter by the year of the CVE IDs (optional)
    timeReservedLt := time.Now() // time.Time | Most recent reserved timestamp to retrieve. Include with all requests potentially returning multiple pages of CVE IDs to avoid issues if new IDs are reserved during use. <br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    timeReservedGt := time.Now() // time.Time | Earliest CVE ID reserved timestamp to retrieve <br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    timeModifiedLt := time.Now() // time.Time | Most recent modified timestamp to retrieve. Include with all requests using a time_modified.gt filter potentially returning multiple pages of CVE IDs. This will avoid issues if IDs are reserved or modified during use.<br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    timeModifiedGt := time.Now() // time.Time | Earliest CVE ID modified timestamp to retrieve <br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    page := int32(56) // int32 | The current page in the paginator (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVEIDApi.CveIdGetFiltered(context.Background()).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).State(state).CveIdYear(cveIdYear).TimeReservedLt(timeReservedLt).TimeReservedGt(timeReservedGt).TimeModifiedLt(timeModifiedLt).TimeModifiedGt(timeModifiedGt).Page(page).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVEIDApi.CveIdGetFiltered``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveIdGetFiltered`: ListCveIdsResponse
    fmt.Fprintf(os.Stdout, "Response from `CVEIDApi.CveIdGetFiltered`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCveIdGetFilteredRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **state** | **string** | Filter by state  | 
 **cveIdYear** | **string** | Filter by the year of the CVE IDs | 
 **timeReservedLt** | **time.Time** | Most recent reserved timestamp to retrieve. Include with all requests potentially returning multiple pages of CVE IDs to avoid issues if new IDs are reserved during use. &lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **timeReservedGt** | **time.Time** | Earliest CVE ID reserved timestamp to retrieve &lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **timeModifiedLt** | **time.Time** | Most recent modified timestamp to retrieve. Include with all requests using a time_modified.gt filter potentially returning multiple pages of CVE IDs. This will avoid issues if IDs are reserved or modified during use.&lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **timeModifiedGt** | **time.Time** | Earliest CVE ID modified timestamp to retrieve &lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **page** | **int32** | The current page in the paginator | 

### Return type

[**ListCveIdsResponse**](ListCveIdsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveIdGetSingle

> GetCveIdResponse CveIdGetSingle(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Retrieves information about the specified CVE ID (accessible to all users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The id of the CVE ID information to retrieve
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVEIDApi.CveIdGetSingle(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVEIDApi.CveIdGetSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveIdGetSingle`: GetCveIdResponse
    fmt.Fprintf(os.Stdout, "Response from `CVEIDApi.CveIdGetSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The id of the CVE ID information to retrieve | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveIdGetSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

[**GetCveIdResponse**](GetCveIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveIdRangeCreate

> CveIdRangeCreate(ctx, year).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Creates a CVE-ID-Range for the specified year (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    year := int32(56) // int32 | The year of the CVE-ID-Range
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CVEIDApi.CveIdRangeCreate(context.Background(), year).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVEIDApi.CveIdRangeCreate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**year** | **int32** | The year of the CVE-ID-Range | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveIdRangeCreateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveIdReserve

> CreateCveIdsResponse CveIdReserve(ctx).Amount(amount).CveYear(cveYear).ShortName(shortName).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).BatchType(batchType).Execute()

Reserves CVE IDs for the organization provided in the short_name query parameter (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    amount := int32(56) // int32 | Quantity of CVE IDs to reserve
    cveYear := int32(56) // int32 | The year the CVE IDs will be reserved for (i.e., 1999, ..., currentYear + 1)
    shortName := "shortName_example" // string | The CNA that will own the reserved CVE IDs
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    batchType := "batchType_example" // string | Required when amount is greater than one, determines whether the reserved CVE IDs should be sequential or non-sequential (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVEIDApi.CveIdReserve(context.Background()).Amount(amount).CveYear(cveYear).ShortName(shortName).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).BatchType(batchType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVEIDApi.CveIdReserve``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveIdReserve`: CreateCveIdsResponse
    fmt.Fprintf(os.Stdout, "Response from `CVEIDApi.CveIdReserve`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCveIdReserveRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amount** | **int32** | Quantity of CVE IDs to reserve | 
 **cveYear** | **int32** | The year the CVE IDs will be reserved for (i.e., 1999, ..., currentYear + 1) | 
 **shortName** | **string** | The CNA that will own the reserved CVE IDs | 
 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **batchType** | **string** | Required when amount is greater than one, determines whether the reserved CVE IDs should be sequential or non-sequential | 

### Return type

[**CreateCveIdsResponse**](CreateCveIdsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveIdUpdateSingle

> UpdateCveIdResponse CveIdUpdateSingle(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Org(org).State(state).Execute()

Updates information related to the specified CVE ID (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The id of the CVE ID to update
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    org := "org_example" // string | The shortname of the new owning_cna for the CVE ID (optional)
    state := "state_example" // string | The new state for the CVE ID (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVEIDApi.CveIdUpdateSingle(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Org(org).State(state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVEIDApi.CveIdUpdateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveIdUpdateSingle`: UpdateCveIdResponse
    fmt.Fprintf(os.Stdout, "Response from `CVEIDApi.CveIdUpdateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The id of the CVE ID to update | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveIdUpdateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **org** | **string** | The shortname of the new owning_cna for the CVE ID | 
 **state** | **string** | The new state for the CVE ID | 

### Return type

[**UpdateCveIdResponse**](UpdateCveIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

