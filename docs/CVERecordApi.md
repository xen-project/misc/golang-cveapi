# \CVERecordApi

All URIs are relative to *https://cveawg.mitre.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CveCnaCreateReject**](CVERecordApi.md#CveCnaCreateReject) | **Post** /cve/{id}/reject | Creates a rejected CVE Record for the specified ID if no record yet exists (accessible to CNAs and Secretariat)
[**CveCnaCreateSingle**](CVERecordApi.md#CveCnaCreateSingle) | **Post** /cve/{id}/cna | Creates a CVE Record from CNA Container JSON for the specified ID (accessible to CNAs and Secretariat)
[**CveCnaUpdateReject**](CVERecordApi.md#CveCnaUpdateReject) | **Put** /cve/{id}/reject | Updates an existing CVE Record with a rejected record for the specified ID (accessible to CNAs and Secretariat)
[**CveCnaUpdateSingle**](CVERecordApi.md#CveCnaUpdateSingle) | **Put** /cve/{id}/cna | Updates the CVE Record from CNA Container JSON for the specified ID (accessible to CNAs and Secretariat)
[**CveGetFiltered**](CVERecordApi.md#CveGetFiltered) | **Get** /cve | Retrieves all CVE Records after applying the query parameters as filters (accessible to Secretariat)
[**CveGetSingle**](CVERecordApi.md#CveGetSingle) | **Get** /cve/{id} | Returns a CVE Record by CVE ID (accessible to all users)
[**CveSubmit**](CVERecordApi.md#CveSubmit) | **Post** /cve/{id} | Creates a CVE Record from full CVE Record JSON for the specified ID (accessible to Secretariat.)
[**CveUpdateSingle**](CVERecordApi.md#CveUpdateSingle) | **Put** /cve/{id} | Updates a CVE Record from full CVE Record JSON for the specified ID (accessible to Secretariat.)



## CveCnaCreateReject

> CreateCveRecordRejectionResponse CveCnaCreateReject(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateCveRecordRejectionRequest(createCveRecordRejectionRequest).Execute()

Creates a rejected CVE Record for the specified ID if no record yet exists (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the record being rejected
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    createCveRecordRejectionRequest := *openapiclient.NewCreateCveRecordRejectionRequest(*openapiclient.NewUpdateCveRecordRejectionRequestCnaContainer([]openapiclient.CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner{*openapiclient.NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner()})) // CreateCveRecordRejectionRequest | Note: providerMetadata is set by the server. If provided, it will be overwritten.

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveCnaCreateReject(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateCveRecordRejectionRequest(createCveRecordRejectionRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveCnaCreateReject``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveCnaCreateReject`: CreateCveRecordRejectionResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveCnaCreateReject`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the record being rejected | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveCnaCreateRejectRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **createCveRecordRejectionRequest** | [**CreateCveRecordRejectionRequest**](CreateCveRecordRejectionRequest.md) | Note: providerMetadata is set by the server. If provided, it will be overwritten. | 

### Return type

[**CreateCveRecordRejectionResponse**](CreateCveRecordRejectionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveCnaCreateSingle

> CreateCveRecordResponse CveCnaCreateSingle(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CveRecordMinimumRequest(cveRecordMinimumRequest).Execute()

Creates a CVE Record from CNA Container JSON for the specified ID (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the record being created
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    cveRecordMinimumRequest := *openapiclient.NewCveRecordMinimumRequest(*openapiclient.NewCveRecordMinimumRequestCnaContainer([]openapiclient.CreateFullCveRecordRequestContainersCnaAffectedInner{*openapiclient.NewCreateFullCveRecordRequestContainersCnaAffectedInner()}, []openapiclient.GetCveRecordResponseContainersCnaDescriptionsInner{*openapiclient.NewGetCveRecordResponseContainersCnaDescriptionsInner()}, []openapiclient.GetCveRecordResponseContainersCnaReferencesInner{*openapiclient.NewGetCveRecordResponseContainersCnaReferencesInner()})) // CveRecordMinimumRequest | Note: providerMetadata is set by the server. If provided, it will be overwritten.

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveCnaCreateSingle(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CveRecordMinimumRequest(cveRecordMinimumRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveCnaCreateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveCnaCreateSingle`: CreateCveRecordResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveCnaCreateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the record being created | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveCnaCreateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **cveRecordMinimumRequest** | [**CveRecordMinimumRequest**](CveRecordMinimumRequest.md) | Note: providerMetadata is set by the server. If provided, it will be overwritten. | 

### Return type

[**CreateCveRecordResponse**](CreateCveRecordResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveCnaUpdateReject

> UpdateCveRecordRejectionResponse CveCnaUpdateReject(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).UpdateCveRecordRejectionRequest(updateCveRecordRejectionRequest).Execute()

Updates an existing CVE Record with a rejected record for the specified ID (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the record being rejected
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    updateCveRecordRejectionRequest := *openapiclient.NewUpdateCveRecordRejectionRequest(*openapiclient.NewUpdateCveRecordRejectionRequestCnaContainer([]openapiclient.CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner{*openapiclient.NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner()})) // UpdateCveRecordRejectionRequest | Note: providerMetadata is set by the server. If provided, it will be overwritten.

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveCnaUpdateReject(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).UpdateCveRecordRejectionRequest(updateCveRecordRejectionRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveCnaUpdateReject``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveCnaUpdateReject`: UpdateCveRecordRejectionResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveCnaUpdateReject`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the record being rejected | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveCnaUpdateRejectRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **updateCveRecordRejectionRequest** | [**UpdateCveRecordRejectionRequest**](UpdateCveRecordRejectionRequest.md) | Note: providerMetadata is set by the server. If provided, it will be overwritten. | 

### Return type

[**UpdateCveRecordRejectionResponse**](UpdateCveRecordRejectionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveCnaUpdateSingle

> UpdateFullCveRecordResponse CveCnaUpdateSingle(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CveRecordMinimumRequest(cveRecordMinimumRequest).Execute()

Updates the CVE Record from CNA Container JSON for the specified ID (accessible to CNAs and Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for which the record is being updated
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    cveRecordMinimumRequest := *openapiclient.NewCveRecordMinimumRequest(*openapiclient.NewCveRecordMinimumRequestCnaContainer([]openapiclient.CreateFullCveRecordRequestContainersCnaAffectedInner{*openapiclient.NewCreateFullCveRecordRequestContainersCnaAffectedInner()}, []openapiclient.GetCveRecordResponseContainersCnaDescriptionsInner{*openapiclient.NewGetCveRecordResponseContainersCnaDescriptionsInner()}, []openapiclient.GetCveRecordResponseContainersCnaReferencesInner{*openapiclient.NewGetCveRecordResponseContainersCnaReferencesInner()})) // CveRecordMinimumRequest | Note: providerMetadata is set by the server. If provided, it will be overwritten.

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveCnaUpdateSingle(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CveRecordMinimumRequest(cveRecordMinimumRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveCnaUpdateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveCnaUpdateSingle`: UpdateFullCveRecordResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveCnaUpdateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for which the record is being updated | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveCnaUpdateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **cveRecordMinimumRequest** | [**CveRecordMinimumRequest**](CveRecordMinimumRequest.md) | Note: providerMetadata is set by the server. If provided, it will be overwritten. | 

### Return type

[**UpdateFullCveRecordResponse**](UpdateFullCveRecordResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveGetFiltered

> CveGetFiltered200Response CveGetFiltered(ctx).TimeModifiedLt(timeModifiedLt).TimeModifiedGt(timeModifiedGt).State(state).CountOnly(countOnly).AssignerShortName(assignerShortName).Assigner(assigner).Page(page).Execute()

Retrieves all CVE Records after applying the query parameters as filters (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    timeModifiedLt := time.Now() // time.Time | Most recent CVE record modified timestamp to retrieve <br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    timeModifiedGt := time.Now() // time.Time | Earliest CVE record modified timestamp to retrieve <br><br> <i>Timestamp format</i> : yyyy-MM-ddTHH:mm:ssZZZZ (optional)
    state := "state_example" // string | Filter by state (optional)
    countOnly := true // bool | Get count of records that match query. Accepted values are 1, true, or yes to indicate true, and 0, false, or no to indicate false (optional)
    assignerShortName := "assignerShortName_example" // string | Filter by assignerShortName (optional)
    assigner := "assigner_example" // string | Filter by assigner org UUID (optional)
    page := int32(56) // int32 | The current page in the paginator (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveGetFiltered(context.Background()).TimeModifiedLt(timeModifiedLt).TimeModifiedGt(timeModifiedGt).State(state).CountOnly(countOnly).AssignerShortName(assignerShortName).Assigner(assigner).Page(page).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveGetFiltered``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveGetFiltered`: CveGetFiltered200Response
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveGetFiltered`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCveGetFilteredRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeModifiedLt** | **time.Time** | Most recent CVE record modified timestamp to retrieve &lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **timeModifiedGt** | **time.Time** | Earliest CVE record modified timestamp to retrieve &lt;br&gt;&lt;br&gt; &lt;i&gt;Timestamp format&lt;/i&gt; : yyyy-MM-ddTHH:mm:ssZZZZ | 
 **state** | **string** | Filter by state | 
 **countOnly** | **bool** | Get count of records that match query. Accepted values are 1, true, or yes to indicate true, and 0, false, or no to indicate false | 
 **assignerShortName** | **string** | Filter by assignerShortName | 
 **assigner** | **string** | Filter by assigner org UUID | 
 **page** | **int32** | The current page in the paginator | 

### Return type

[**CveGetFiltered200Response**](CveGetFiltered200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveGetSingle

> CveGetSingle200Response CveGetSingle(ctx, id).Execute()

Returns a CVE Record by CVE ID (accessible to all users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the Record to be retrieved

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveGetSingle(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveGetSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveGetSingle`: CveGetSingle200Response
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveGetSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the Record to be retrieved | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveGetSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**CveGetSingle200Response**](CveGetSingle200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveSubmit

> CreateCveRecordResponse CveSubmit(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateFullCveRecordRequest(createFullCveRecordRequest).Execute()

Creates a CVE Record from full CVE Record JSON for the specified ID (accessible to Secretariat.)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the record being submitted
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    createFullCveRecordRequest := *openapiclient.NewCreateFullCveRecordRequest() // CreateFullCveRecordRequest | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveSubmit(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateFullCveRecordRequest(createFullCveRecordRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveSubmit``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveSubmit`: CreateCveRecordResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveSubmit`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the record being submitted | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveSubmitRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **createFullCveRecordRequest** | [**CreateFullCveRecordRequest**](CreateFullCveRecordRequest.md) |  | 

### Return type

[**CreateCveRecordResponse**](CreateCveRecordResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CveUpdateSingle

> UpdateFullCveRecordResponse CveUpdateSingle(ctx, id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateFullCveRecordRequest(createFullCveRecordRequest).Execute()

Updates a CVE Record from full CVE Record JSON for the specified ID (accessible to Secretariat.)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    id := "id_example" // string | The CVE ID for the record being updated
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    createFullCveRecordRequest := *openapiclient.NewCreateFullCveRecordRequest() // CreateFullCveRecordRequest | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CVERecordApi.CveUpdateSingle(context.Background(), id).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateFullCveRecordRequest(createFullCveRecordRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CVERecordApi.CveUpdateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CveUpdateSingle`: UpdateFullCveRecordResponse
    fmt.Fprintf(os.Stdout, "Response from `CVERecordApi.CveUpdateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The CVE ID for the record being updated | 

### Other Parameters

Other parameters are passed through a pointer to a apiCveUpdateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **createFullCveRecordRequest** | [**CreateFullCveRecordRequest**](CreateFullCveRecordRequest.md) |  | 

### Return type

[**UpdateFullCveRecordResponse**](UpdateFullCveRecordResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

