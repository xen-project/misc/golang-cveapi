# CreateCveIdsPartialResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Error** | Pointer to **string** |  | [optional] 
**Message** | Pointer to **string** |  | [optional] 
**Details** | Pointer to [**CreateCveIdsPartialResponseDetails**](CreateCveIdsPartialResponseDetails.md) |  | [optional] 
**CveIds** | Pointer to [**[]CreateCveIdsPartialResponseCveIdsInner**](CreateCveIdsPartialResponseCveIdsInner.md) |  | [optional] 

## Methods

### NewCreateCveIdsPartialResponse

`func NewCreateCveIdsPartialResponse() *CreateCveIdsPartialResponse`

NewCreateCveIdsPartialResponse instantiates a new CreateCveIdsPartialResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveIdsPartialResponseWithDefaults

`func NewCreateCveIdsPartialResponseWithDefaults() *CreateCveIdsPartialResponse`

NewCreateCveIdsPartialResponseWithDefaults instantiates a new CreateCveIdsPartialResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetError

`func (o *CreateCveIdsPartialResponse) GetError() string`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *CreateCveIdsPartialResponse) GetErrorOk() (*string, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *CreateCveIdsPartialResponse) SetError(v string)`

SetError sets Error field to given value.

### HasError

`func (o *CreateCveIdsPartialResponse) HasError() bool`

HasError returns a boolean if a field has been set.

### GetMessage

`func (o *CreateCveIdsPartialResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CreateCveIdsPartialResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CreateCveIdsPartialResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CreateCveIdsPartialResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetDetails

`func (o *CreateCveIdsPartialResponse) GetDetails() CreateCveIdsPartialResponseDetails`

GetDetails returns the Details field if non-nil, zero value otherwise.

### GetDetailsOk

`func (o *CreateCveIdsPartialResponse) GetDetailsOk() (*CreateCveIdsPartialResponseDetails, bool)`

GetDetailsOk returns a tuple with the Details field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDetails

`func (o *CreateCveIdsPartialResponse) SetDetails(v CreateCveIdsPartialResponseDetails)`

SetDetails sets Details field to given value.

### HasDetails

`func (o *CreateCveIdsPartialResponse) HasDetails() bool`

HasDetails returns a boolean if a field has been set.

### GetCveIds

`func (o *CreateCveIdsPartialResponse) GetCveIds() []CreateCveIdsPartialResponseCveIdsInner`

GetCveIds returns the CveIds field if non-nil, zero value otherwise.

### GetCveIdsOk

`func (o *CreateCveIdsPartialResponse) GetCveIdsOk() (*[]CreateCveIdsPartialResponseCveIdsInner, bool)`

GetCveIdsOk returns a tuple with the CveIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveIds

`func (o *CreateCveIdsPartialResponse) SetCveIds(v []CreateCveIdsPartialResponseCveIdsInner)`

SetCveIds sets CveIds field to given value.

### HasCveIds

`func (o *CreateCveIdsPartialResponse) HasCveIds() bool`

HasCveIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


