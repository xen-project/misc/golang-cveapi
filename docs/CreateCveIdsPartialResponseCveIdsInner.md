# CreateCveIdsPartialResponseCveIdsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CveId** | Pointer to **string** |  | [optional] 
**CveYear** | Pointer to **string** |  | [optional] 
**OwningCna** | Pointer to **string** | The shortname for the organization that owns the CVE ID | [optional] 
**State** | Pointer to **string** |  | [optional] 
**RequestedBy** | Pointer to [**ListCveIdsResponseCveIdsInnerRequestedBy**](ListCveIdsResponseCveIdsInnerRequestedBy.md) |  | [optional] 
**Reserved** | Pointer to **time.Time** | The time the ID was reserved | [optional] 

## Methods

### NewCreateCveIdsPartialResponseCveIdsInner

`func NewCreateCveIdsPartialResponseCveIdsInner() *CreateCveIdsPartialResponseCveIdsInner`

NewCreateCveIdsPartialResponseCveIdsInner instantiates a new CreateCveIdsPartialResponseCveIdsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveIdsPartialResponseCveIdsInnerWithDefaults

`func NewCreateCveIdsPartialResponseCveIdsInnerWithDefaults() *CreateCveIdsPartialResponseCveIdsInner`

NewCreateCveIdsPartialResponseCveIdsInnerWithDefaults instantiates a new CreateCveIdsPartialResponseCveIdsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCveId

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetCveId() string`

GetCveId returns the CveId field if non-nil, zero value otherwise.

### GetCveIdOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetCveIdOk() (*string, bool)`

GetCveIdOk returns a tuple with the CveId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveId

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetCveId(v string)`

SetCveId sets CveId field to given value.

### HasCveId

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasCveId() bool`

HasCveId returns a boolean if a field has been set.

### GetCveYear

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetCveYear() string`

GetCveYear returns the CveYear field if non-nil, zero value otherwise.

### GetCveYearOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetCveYearOk() (*string, bool)`

GetCveYearOk returns a tuple with the CveYear field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveYear

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetCveYear(v string)`

SetCveYear sets CveYear field to given value.

### HasCveYear

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasCveYear() bool`

HasCveYear returns a boolean if a field has been set.

### GetOwningCna

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetOwningCna() string`

GetOwningCna returns the OwningCna field if non-nil, zero value otherwise.

### GetOwningCnaOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetOwningCnaOk() (*string, bool)`

GetOwningCnaOk returns a tuple with the OwningCna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwningCna

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetOwningCna(v string)`

SetOwningCna sets OwningCna field to given value.

### HasOwningCna

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasOwningCna() bool`

HasOwningCna returns a boolean if a field has been set.

### GetState

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasState() bool`

HasState returns a boolean if a field has been set.

### GetRequestedBy

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetRequestedBy() ListCveIdsResponseCveIdsInnerRequestedBy`

GetRequestedBy returns the RequestedBy field if non-nil, zero value otherwise.

### GetRequestedByOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetRequestedByOk() (*ListCveIdsResponseCveIdsInnerRequestedBy, bool)`

GetRequestedByOk returns a tuple with the RequestedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedBy

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetRequestedBy(v ListCveIdsResponseCveIdsInnerRequestedBy)`

SetRequestedBy sets RequestedBy field to given value.

### HasRequestedBy

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasRequestedBy() bool`

HasRequestedBy returns a boolean if a field has been set.

### GetReserved

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetReserved() time.Time`

GetReserved returns the Reserved field if non-nil, zero value otherwise.

### GetReservedOk

`func (o *CreateCveIdsPartialResponseCveIdsInner) GetReservedOk() (*time.Time, bool)`

GetReservedOk returns a tuple with the Reserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReserved

`func (o *CreateCveIdsPartialResponseCveIdsInner) SetReserved(v time.Time)`

SetReserved sets Reserved field to given value.

### HasReserved

`func (o *CreateCveIdsPartialResponseCveIdsInner) HasReserved() bool`

HasReserved returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


