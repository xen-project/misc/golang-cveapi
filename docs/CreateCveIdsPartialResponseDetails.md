# CreateCveIdsPartialResponseDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AmountReserved** | Pointer to **int32** | The quantity of IDs reserved in the system | [optional] 

## Methods

### NewCreateCveIdsPartialResponseDetails

`func NewCreateCveIdsPartialResponseDetails() *CreateCveIdsPartialResponseDetails`

NewCreateCveIdsPartialResponseDetails instantiates a new CreateCveIdsPartialResponseDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveIdsPartialResponseDetailsWithDefaults

`func NewCreateCveIdsPartialResponseDetailsWithDefaults() *CreateCveIdsPartialResponseDetails`

NewCreateCveIdsPartialResponseDetailsWithDefaults instantiates a new CreateCveIdsPartialResponseDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAmountReserved

`func (o *CreateCveIdsPartialResponseDetails) GetAmountReserved() int32`

GetAmountReserved returns the AmountReserved field if non-nil, zero value otherwise.

### GetAmountReservedOk

`func (o *CreateCveIdsPartialResponseDetails) GetAmountReservedOk() (*int32, bool)`

GetAmountReservedOk returns a tuple with the AmountReserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmountReserved

`func (o *CreateCveIdsPartialResponseDetails) SetAmountReserved(v int32)`

SetAmountReserved sets AmountReserved field to given value.

### HasAmountReserved

`func (o *CreateCveIdsPartialResponseDetails) HasAmountReserved() bool`

HasAmountReserved returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


