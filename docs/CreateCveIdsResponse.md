# CreateCveIdsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CveIds** | Pointer to [**[]CreateCveIdsResponseCveIdsInner**](CreateCveIdsResponseCveIdsInner.md) |  | [optional] 
**Meta** | Pointer to [**CreateCveIdsResponseMeta**](CreateCveIdsResponseMeta.md) |  | [optional] 

## Methods

### NewCreateCveIdsResponse

`func NewCreateCveIdsResponse() *CreateCveIdsResponse`

NewCreateCveIdsResponse instantiates a new CreateCveIdsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveIdsResponseWithDefaults

`func NewCreateCveIdsResponseWithDefaults() *CreateCveIdsResponse`

NewCreateCveIdsResponseWithDefaults instantiates a new CreateCveIdsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCveIds

`func (o *CreateCveIdsResponse) GetCveIds() []CreateCveIdsResponseCveIdsInner`

GetCveIds returns the CveIds field if non-nil, zero value otherwise.

### GetCveIdsOk

`func (o *CreateCveIdsResponse) GetCveIdsOk() (*[]CreateCveIdsResponseCveIdsInner, bool)`

GetCveIdsOk returns a tuple with the CveIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveIds

`func (o *CreateCveIdsResponse) SetCveIds(v []CreateCveIdsResponseCveIdsInner)`

SetCveIds sets CveIds field to given value.

### HasCveIds

`func (o *CreateCveIdsResponse) HasCveIds() bool`

HasCveIds returns a boolean if a field has been set.

### GetMeta

`func (o *CreateCveIdsResponse) GetMeta() CreateCveIdsResponseMeta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *CreateCveIdsResponse) GetMetaOk() (*CreateCveIdsResponseMeta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *CreateCveIdsResponse) SetMeta(v CreateCveIdsResponseMeta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *CreateCveIdsResponse) HasMeta() bool`

HasMeta returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


