# CreateCveIdsResponseMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RemainingQuota** | Pointer to **int32** |  | [optional] 

## Methods

### NewCreateCveIdsResponseMeta

`func NewCreateCveIdsResponseMeta() *CreateCveIdsResponseMeta`

NewCreateCveIdsResponseMeta instantiates a new CreateCveIdsResponseMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveIdsResponseMetaWithDefaults

`func NewCreateCveIdsResponseMetaWithDefaults() *CreateCveIdsResponseMeta`

NewCreateCveIdsResponseMetaWithDefaults instantiates a new CreateCveIdsResponseMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRemainingQuota

`func (o *CreateCveIdsResponseMeta) GetRemainingQuota() int32`

GetRemainingQuota returns the RemainingQuota field if non-nil, zero value otherwise.

### GetRemainingQuotaOk

`func (o *CreateCveIdsResponseMeta) GetRemainingQuotaOk() (*int32, bool)`

GetRemainingQuotaOk returns a tuple with the RemainingQuota field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemainingQuota

`func (o *CreateCveIdsResponseMeta) SetRemainingQuota(v int32)`

SetRemainingQuota sets RemainingQuota field to given value.

### HasRemainingQuota

`func (o *CreateCveIdsResponseMeta) HasRemainingQuota() bool`

HasRemainingQuota returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


