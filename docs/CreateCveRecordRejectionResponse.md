# CreateCveRecordRejectionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Created** | Pointer to [**CreateCveRecordRejectionResponseCreated**](CreateCveRecordRejectionResponseCreated.md) |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponse

`func NewCreateCveRecordRejectionResponse() *CreateCveRecordRejectionResponse`

NewCreateCveRecordRejectionResponse instantiates a new CreateCveRecordRejectionResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseWithDefaults

`func NewCreateCveRecordRejectionResponseWithDefaults() *CreateCveRecordRejectionResponse`

NewCreateCveRecordRejectionResponseWithDefaults instantiates a new CreateCveRecordRejectionResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *CreateCveRecordRejectionResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CreateCveRecordRejectionResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CreateCveRecordRejectionResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CreateCveRecordRejectionResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCreated

`func (o *CreateCveRecordRejectionResponse) GetCreated() CreateCveRecordRejectionResponseCreated`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *CreateCveRecordRejectionResponse) GetCreatedOk() (*CreateCveRecordRejectionResponseCreated, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *CreateCveRecordRejectionResponse) SetCreated(v CreateCveRecordRejectionResponseCreated)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *CreateCveRecordRejectionResponse) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


