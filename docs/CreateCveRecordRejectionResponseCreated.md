# CreateCveRecordRejectionResponseCreated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Containers** | Pointer to [**CreateCveRecordRejectionResponseCreatedContainers**](CreateCveRecordRejectionResponseCreatedContainers.md) |  | [optional] 
**CveMetadata** | Pointer to [**GetCveRecordResponseCveMetadata**](GetCveRecordResponseCveMetadata.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DataVersion** | Pointer to **string** |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponseCreated

`func NewCreateCveRecordRejectionResponseCreated() *CreateCveRecordRejectionResponseCreated`

NewCreateCveRecordRejectionResponseCreated instantiates a new CreateCveRecordRejectionResponseCreated object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedWithDefaults() *CreateCveRecordRejectionResponseCreated`

NewCreateCveRecordRejectionResponseCreatedWithDefaults instantiates a new CreateCveRecordRejectionResponseCreated object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContainers

`func (o *CreateCveRecordRejectionResponseCreated) GetContainers() CreateCveRecordRejectionResponseCreatedContainers`

GetContainers returns the Containers field if non-nil, zero value otherwise.

### GetContainersOk

`func (o *CreateCveRecordRejectionResponseCreated) GetContainersOk() (*CreateCveRecordRejectionResponseCreatedContainers, bool)`

GetContainersOk returns a tuple with the Containers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContainers

`func (o *CreateCveRecordRejectionResponseCreated) SetContainers(v CreateCveRecordRejectionResponseCreatedContainers)`

SetContainers sets Containers field to given value.

### HasContainers

`func (o *CreateCveRecordRejectionResponseCreated) HasContainers() bool`

HasContainers returns a boolean if a field has been set.

### GetCveMetadata

`func (o *CreateCveRecordRejectionResponseCreated) GetCveMetadata() GetCveRecordResponseCveMetadata`

GetCveMetadata returns the CveMetadata field if non-nil, zero value otherwise.

### GetCveMetadataOk

`func (o *CreateCveRecordRejectionResponseCreated) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool)`

GetCveMetadataOk returns a tuple with the CveMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveMetadata

`func (o *CreateCveRecordRejectionResponseCreated) SetCveMetadata(v GetCveRecordResponseCveMetadata)`

SetCveMetadata sets CveMetadata field to given value.

### HasCveMetadata

`func (o *CreateCveRecordRejectionResponseCreated) HasCveMetadata() bool`

HasCveMetadata returns a boolean if a field has been set.

### GetDataType

`func (o *CreateCveRecordRejectionResponseCreated) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *CreateCveRecordRejectionResponseCreated) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *CreateCveRecordRejectionResponseCreated) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *CreateCveRecordRejectionResponseCreated) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDataVersion

`func (o *CreateCveRecordRejectionResponseCreated) GetDataVersion() string`

GetDataVersion returns the DataVersion field if non-nil, zero value otherwise.

### GetDataVersionOk

`func (o *CreateCveRecordRejectionResponseCreated) GetDataVersionOk() (*string, bool)`

GetDataVersionOk returns a tuple with the DataVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataVersion

`func (o *CreateCveRecordRejectionResponseCreated) SetDataVersion(v string)`

SetDataVersion sets DataVersion field to given value.

### HasDataVersion

`func (o *CreateCveRecordRejectionResponseCreated) HasDataVersion() bool`

HasDataVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


