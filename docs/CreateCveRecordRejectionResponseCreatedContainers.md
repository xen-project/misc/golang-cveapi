# CreateCveRecordRejectionResponseCreatedContainers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cna** | Pointer to [**CreateCveRecordRejectionResponseCreatedContainersCna**](CreateCveRecordRejectionResponseCreatedContainersCna.md) |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponseCreatedContainers

`func NewCreateCveRecordRejectionResponseCreatedContainers() *CreateCveRecordRejectionResponseCreatedContainers`

NewCreateCveRecordRejectionResponseCreatedContainers instantiates a new CreateCveRecordRejectionResponseCreatedContainers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedContainersWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedContainersWithDefaults() *CreateCveRecordRejectionResponseCreatedContainers`

NewCreateCveRecordRejectionResponseCreatedContainersWithDefaults instantiates a new CreateCveRecordRejectionResponseCreatedContainers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCna

`func (o *CreateCveRecordRejectionResponseCreatedContainers) GetCna() CreateCveRecordRejectionResponseCreatedContainersCna`

GetCna returns the Cna field if non-nil, zero value otherwise.

### GetCnaOk

`func (o *CreateCveRecordRejectionResponseCreatedContainers) GetCnaOk() (*CreateCveRecordRejectionResponseCreatedContainersCna, bool)`

GetCnaOk returns a tuple with the Cna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCna

`func (o *CreateCveRecordRejectionResponseCreatedContainers) SetCna(v CreateCveRecordRejectionResponseCreatedContainersCna)`

SetCna sets Cna field to given value.

### HasCna

`func (o *CreateCveRecordRejectionResponseCreatedContainers) HasCna() bool`

HasCna returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


