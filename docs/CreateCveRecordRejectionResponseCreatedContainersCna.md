# CreateCveRecordRejectionResponseCreatedContainersCna

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RejectedReasons** | Pointer to [**[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner**](CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner.md) |  | [optional] 
**ReplacedBy** | Pointer to **[]string** |  | [optional] 
**ProviderMetadata** | Pointer to [**CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata**](CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata.md) |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponseCreatedContainersCna

`func NewCreateCveRecordRejectionResponseCreatedContainersCna() *CreateCveRecordRejectionResponseCreatedContainersCna`

NewCreateCveRecordRejectionResponseCreatedContainersCna instantiates a new CreateCveRecordRejectionResponseCreatedContainersCna object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedContainersCnaWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaWithDefaults() *CreateCveRecordRejectionResponseCreatedContainersCna`

NewCreateCveRecordRejectionResponseCreatedContainersCnaWithDefaults instantiates a new CreateCveRecordRejectionResponseCreatedContainersCna object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRejectedReasons

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetRejectedReasons() []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner`

GetRejectedReasons returns the RejectedReasons field if non-nil, zero value otherwise.

### GetRejectedReasonsOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetRejectedReasonsOk() (*[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner, bool)`

GetRejectedReasonsOk returns a tuple with the RejectedReasons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedReasons

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) SetRejectedReasons(v []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner)`

SetRejectedReasons sets RejectedReasons field to given value.

### HasRejectedReasons

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) HasRejectedReasons() bool`

HasRejectedReasons returns a boolean if a field has been set.

### GetReplacedBy

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetReplacedBy() []string`

GetReplacedBy returns the ReplacedBy field if non-nil, zero value otherwise.

### GetReplacedByOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetReplacedByOk() (*[]string, bool)`

GetReplacedByOk returns a tuple with the ReplacedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReplacedBy

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) SetReplacedBy(v []string)`

SetReplacedBy sets ReplacedBy field to given value.

### HasReplacedBy

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) HasReplacedBy() bool`

HasReplacedBy returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetProviderMetadata() CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) GetProviderMetadataOk() (*CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) SetProviderMetadata(v CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *CreateCveRecordRejectionResponseCreatedContainersCna) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


