# CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrgId** | Pointer to **string** |  | [optional] 
**ShortName** | Pointer to **string** |  | [optional] 
**DateUpdated** | Pointer to **time.Time** |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata() *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata`

NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadataWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadataWithDefaults() *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata`

NewCreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadataWithDefaults instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrgId

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.

### HasOrgId

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) HasOrgId() bool`

HasOrgId returns a boolean if a field has been set.

### GetShortName

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetShortName() string`

GetShortName returns the ShortName field if non-nil, zero value otherwise.

### GetShortNameOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetShortNameOk() (*string, bool)`

GetShortNameOk returns a tuple with the ShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortName

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) SetShortName(v string)`

SetShortName sets ShortName field to given value.

### HasShortName

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) HasShortName() bool`

HasShortName returns a boolean if a field has been set.

### GetDateUpdated

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetDateUpdated() time.Time`

GetDateUpdated returns the DateUpdated field if non-nil, zero value otherwise.

### GetDateUpdatedOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) GetDateUpdatedOk() (*time.Time, bool)`

GetDateUpdatedOk returns a tuple with the DateUpdated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateUpdated

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) SetDateUpdated(v time.Time)`

SetDateUpdated sets DateUpdated field to given value.

### HasDateUpdated

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) HasDateUpdated() bool`

HasDateUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


