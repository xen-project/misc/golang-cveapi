# CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Lang** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 
**SupportingMedia** | Pointer to [**[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner**](CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner.md) |  | [optional] 

## Methods

### NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner() *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner`

NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerWithDefaults() *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner`

NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerWithDefaults instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLang

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetLang() string`

GetLang returns the Lang field if non-nil, zero value otherwise.

### GetLangOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetLangOk() (*string, bool)`

GetLangOk returns a tuple with the Lang field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLang

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) SetLang(v string)`

SetLang sets Lang field to given value.

### HasLang

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) HasLang() bool`

HasLang returns a boolean if a field has been set.

### GetValue

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetSupportingMedia

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetSupportingMedia() []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner`

GetSupportingMedia returns the SupportingMedia field if non-nil, zero value otherwise.

### GetSupportingMediaOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) GetSupportingMediaOk() (*[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner, bool)`

GetSupportingMediaOk returns a tuple with the SupportingMedia field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupportingMedia

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) SetSupportingMedia(v []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner)`

SetSupportingMedia sets SupportingMedia field to given value.

### HasSupportingMedia

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner) HasSupportingMedia() bool`

HasSupportingMedia returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


