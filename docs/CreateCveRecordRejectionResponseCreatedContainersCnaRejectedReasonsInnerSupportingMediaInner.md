# CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | RFC2046 compliant IANA Media type for eg., text/markdown, text/html. | 
**Base64** | Pointer to **bool** | If true then the value field contains the media data encoded in base64. If false then the value field contains the UTF-8 media content. | [optional] [default to false]
**Value** | **string** | Supporting media content, up to 16K. If base64 is true, this field stores base64 encoded data. | 

## Methods

### NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner(type_ string, value string, ) *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner`

NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInnerWithDefaults

`func NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInnerWithDefaults() *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner`

NewCreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInnerWithDefaults instantiates a new CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) SetType(v string)`

SetType sets Type field to given value.


### GetBase64

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetBase64() bool`

GetBase64 returns the Base64 field if non-nil, zero value otherwise.

### GetBase64Ok

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetBase64Ok() (*bool, bool)`

GetBase64Ok returns a tuple with the Base64 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBase64

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) SetBase64(v bool)`

SetBase64 sets Base64 field to given value.

### HasBase64

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) HasBase64() bool`

HasBase64 returns a boolean if a field has been set.

### GetValue

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInnerSupportingMediaInner) SetValue(v string)`

SetValue sets Value field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


