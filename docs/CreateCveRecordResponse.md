# CreateCveRecordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Created** | Pointer to [**CreateCveRecordResponseCreated**](CreateCveRecordResponseCreated.md) |  | [optional] 

## Methods

### NewCreateCveRecordResponse

`func NewCreateCveRecordResponse() *CreateCveRecordResponse`

NewCreateCveRecordResponse instantiates a new CreateCveRecordResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordResponseWithDefaults

`func NewCreateCveRecordResponseWithDefaults() *CreateCveRecordResponse`

NewCreateCveRecordResponseWithDefaults instantiates a new CreateCveRecordResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *CreateCveRecordResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CreateCveRecordResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CreateCveRecordResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CreateCveRecordResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCreated

`func (o *CreateCveRecordResponse) GetCreated() CreateCveRecordResponseCreated`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *CreateCveRecordResponse) GetCreatedOk() (*CreateCveRecordResponseCreated, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *CreateCveRecordResponse) SetCreated(v CreateCveRecordResponseCreated)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *CreateCveRecordResponse) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


