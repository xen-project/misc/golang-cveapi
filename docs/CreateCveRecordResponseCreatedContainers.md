# CreateCveRecordResponseCreatedContainers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cna** | Pointer to [**CreateCveRecordResponseCreatedContainersCna**](CreateCveRecordResponseCreatedContainersCna.md) |  | [optional] 

## Methods

### NewCreateCveRecordResponseCreatedContainers

`func NewCreateCveRecordResponseCreatedContainers() *CreateCveRecordResponseCreatedContainers`

NewCreateCveRecordResponseCreatedContainers instantiates a new CreateCveRecordResponseCreatedContainers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordResponseCreatedContainersWithDefaults

`func NewCreateCveRecordResponseCreatedContainersWithDefaults() *CreateCveRecordResponseCreatedContainers`

NewCreateCveRecordResponseCreatedContainersWithDefaults instantiates a new CreateCveRecordResponseCreatedContainers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCna

`func (o *CreateCveRecordResponseCreatedContainers) GetCna() CreateCveRecordResponseCreatedContainersCna`

GetCna returns the Cna field if non-nil, zero value otherwise.

### GetCnaOk

`func (o *CreateCveRecordResponseCreatedContainers) GetCnaOk() (*CreateCveRecordResponseCreatedContainersCna, bool)`

GetCnaOk returns a tuple with the Cna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCna

`func (o *CreateCveRecordResponseCreatedContainers) SetCna(v CreateCveRecordResponseCreatedContainersCna)`

SetCna sets Cna field to given value.

### HasCna

`func (o *CreateCveRecordResponseCreatedContainers) HasCna() bool`

HasCna returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


