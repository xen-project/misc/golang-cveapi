# CreateCveRecordResponseCreatedContainersCna

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Affected** | Pointer to [**CreateFullCveRecordRequestContainersCnaAffectedInner**](CreateFullCveRecordRequestContainersCnaAffectedInner.md) |  | [optional] 
**Descriptions** | Pointer to [**[]GetCveRecordResponseContainersCnaDescriptionsInner**](GetCveRecordResponseContainersCnaDescriptionsInner.md) |  | [optional] 
**ProblemTypes** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInner**](GetCveRecordResponseContainersCnaProblemTypesInner.md) |  | [optional] 
**ProviderMetadata** | Pointer to [**CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata**](CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata.md) |  | [optional] 
**References** | Pointer to [**[]GetCveRecordResponseContainersCnaReferencesInner**](GetCveRecordResponseContainersCnaReferencesInner.md) |  | [optional] 

## Methods

### NewCreateCveRecordResponseCreatedContainersCna

`func NewCreateCveRecordResponseCreatedContainersCna() *CreateCveRecordResponseCreatedContainersCna`

NewCreateCveRecordResponseCreatedContainersCna instantiates a new CreateCveRecordResponseCreatedContainersCna object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateCveRecordResponseCreatedContainersCnaWithDefaults

`func NewCreateCveRecordResponseCreatedContainersCnaWithDefaults() *CreateCveRecordResponseCreatedContainersCna`

NewCreateCveRecordResponseCreatedContainersCnaWithDefaults instantiates a new CreateCveRecordResponseCreatedContainersCna object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffected

`func (o *CreateCveRecordResponseCreatedContainersCna) GetAffected() CreateFullCveRecordRequestContainersCnaAffectedInner`

GetAffected returns the Affected field if non-nil, zero value otherwise.

### GetAffectedOk

`func (o *CreateCveRecordResponseCreatedContainersCna) GetAffectedOk() (*CreateFullCveRecordRequestContainersCnaAffectedInner, bool)`

GetAffectedOk returns a tuple with the Affected field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffected

`func (o *CreateCveRecordResponseCreatedContainersCna) SetAffected(v CreateFullCveRecordRequestContainersCnaAffectedInner)`

SetAffected sets Affected field to given value.

### HasAffected

`func (o *CreateCveRecordResponseCreatedContainersCna) HasAffected() bool`

HasAffected returns a boolean if a field has been set.

### GetDescriptions

`func (o *CreateCveRecordResponseCreatedContainersCna) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *CreateCveRecordResponseCreatedContainersCna) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *CreateCveRecordResponseCreatedContainersCna) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.

### HasDescriptions

`func (o *CreateCveRecordResponseCreatedContainersCna) HasDescriptions() bool`

HasDescriptions returns a boolean if a field has been set.

### GetProblemTypes

`func (o *CreateCveRecordResponseCreatedContainersCna) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner`

GetProblemTypes returns the ProblemTypes field if non-nil, zero value otherwise.

### GetProblemTypesOk

`func (o *CreateCveRecordResponseCreatedContainersCna) GetProblemTypesOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInner, bool)`

GetProblemTypesOk returns a tuple with the ProblemTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProblemTypes

`func (o *CreateCveRecordResponseCreatedContainersCna) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner)`

SetProblemTypes sets ProblemTypes field to given value.

### HasProblemTypes

`func (o *CreateCveRecordResponseCreatedContainersCna) HasProblemTypes() bool`

HasProblemTypes returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *CreateCveRecordResponseCreatedContainersCna) GetProviderMetadata() CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *CreateCveRecordResponseCreatedContainersCna) GetProviderMetadataOk() (*CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *CreateCveRecordResponseCreatedContainersCna) SetProviderMetadata(v CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *CreateCveRecordResponseCreatedContainersCna) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.

### GetReferences

`func (o *CreateCveRecordResponseCreatedContainersCna) GetReferences() []GetCveRecordResponseContainersCnaReferencesInner`

GetReferences returns the References field if non-nil, zero value otherwise.

### GetReferencesOk

`func (o *CreateCveRecordResponseCreatedContainersCna) GetReferencesOk() (*[]GetCveRecordResponseContainersCnaReferencesInner, bool)`

GetReferencesOk returns a tuple with the References field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferences

`func (o *CreateCveRecordResponseCreatedContainersCna) SetReferences(v []GetCveRecordResponseContainersCnaReferencesInner)`

SetReferences sets References field to given value.

### HasReferences

`func (o *CreateCveRecordResponseCreatedContainersCna) HasReferences() bool`

HasReferences returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


