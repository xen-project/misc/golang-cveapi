# CreateFullCveRecordRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Containers** | Pointer to [**CreateFullCveRecordRequestContainers**](CreateFullCveRecordRequestContainers.md) |  | [optional] 
**CveMetadata** | Pointer to [**GetCveRecordResponseCveMetadata**](GetCveRecordResponseCveMetadata.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DateVersion** | Pointer to **string** |  | [optional] 

## Methods

### NewCreateFullCveRecordRequest

`func NewCreateFullCveRecordRequest() *CreateFullCveRecordRequest`

NewCreateFullCveRecordRequest instantiates a new CreateFullCveRecordRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateFullCveRecordRequestWithDefaults

`func NewCreateFullCveRecordRequestWithDefaults() *CreateFullCveRecordRequest`

NewCreateFullCveRecordRequestWithDefaults instantiates a new CreateFullCveRecordRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContainers

`func (o *CreateFullCveRecordRequest) GetContainers() CreateFullCveRecordRequestContainers`

GetContainers returns the Containers field if non-nil, zero value otherwise.

### GetContainersOk

`func (o *CreateFullCveRecordRequest) GetContainersOk() (*CreateFullCveRecordRequestContainers, bool)`

GetContainersOk returns a tuple with the Containers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContainers

`func (o *CreateFullCveRecordRequest) SetContainers(v CreateFullCveRecordRequestContainers)`

SetContainers sets Containers field to given value.

### HasContainers

`func (o *CreateFullCveRecordRequest) HasContainers() bool`

HasContainers returns a boolean if a field has been set.

### GetCveMetadata

`func (o *CreateFullCveRecordRequest) GetCveMetadata() GetCveRecordResponseCveMetadata`

GetCveMetadata returns the CveMetadata field if non-nil, zero value otherwise.

### GetCveMetadataOk

`func (o *CreateFullCveRecordRequest) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool)`

GetCveMetadataOk returns a tuple with the CveMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveMetadata

`func (o *CreateFullCveRecordRequest) SetCveMetadata(v GetCveRecordResponseCveMetadata)`

SetCveMetadata sets CveMetadata field to given value.

### HasCveMetadata

`func (o *CreateFullCveRecordRequest) HasCveMetadata() bool`

HasCveMetadata returns a boolean if a field has been set.

### GetDataType

`func (o *CreateFullCveRecordRequest) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *CreateFullCveRecordRequest) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *CreateFullCveRecordRequest) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *CreateFullCveRecordRequest) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDateVersion

`func (o *CreateFullCveRecordRequest) GetDateVersion() string`

GetDateVersion returns the DateVersion field if non-nil, zero value otherwise.

### GetDateVersionOk

`func (o *CreateFullCveRecordRequest) GetDateVersionOk() (*string, bool)`

GetDateVersionOk returns a tuple with the DateVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateVersion

`func (o *CreateFullCveRecordRequest) SetDateVersion(v string)`

SetDateVersion sets DateVersion field to given value.

### HasDateVersion

`func (o *CreateFullCveRecordRequest) HasDateVersion() bool`

HasDateVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


