# CreateFullCveRecordRequestContainers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cna** | [**CreateFullCveRecordRequestContainersCna**](CreateFullCveRecordRequestContainersCna.md) |  | 

## Methods

### NewCreateFullCveRecordRequestContainers

`func NewCreateFullCveRecordRequestContainers(cna CreateFullCveRecordRequestContainersCna, ) *CreateFullCveRecordRequestContainers`

NewCreateFullCveRecordRequestContainers instantiates a new CreateFullCveRecordRequestContainers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateFullCveRecordRequestContainersWithDefaults

`func NewCreateFullCveRecordRequestContainersWithDefaults() *CreateFullCveRecordRequestContainers`

NewCreateFullCveRecordRequestContainersWithDefaults instantiates a new CreateFullCveRecordRequestContainers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCna

`func (o *CreateFullCveRecordRequestContainers) GetCna() CreateFullCveRecordRequestContainersCna`

GetCna returns the Cna field if non-nil, zero value otherwise.

### GetCnaOk

`func (o *CreateFullCveRecordRequestContainers) GetCnaOk() (*CreateFullCveRecordRequestContainersCna, bool)`

GetCnaOk returns a tuple with the Cna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCna

`func (o *CreateFullCveRecordRequestContainers) SetCna(v CreateFullCveRecordRequestContainersCna)`

SetCna sets Cna field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


