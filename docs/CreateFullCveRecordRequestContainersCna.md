# CreateFullCveRecordRequestContainersCna

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Affected** | [**[]CreateFullCveRecordRequestContainersCnaAffectedInner**](CreateFullCveRecordRequestContainersCnaAffectedInner.md) |  | 
**Descriptions** | [**[]GetCveRecordResponseContainersCnaDescriptionsInner**](GetCveRecordResponseContainersCnaDescriptionsInner.md) |  | 
**ProblemTypes** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInner**](GetCveRecordResponseContainersCnaProblemTypesInner.md) |  | [optional] 
**ProviderMetadata** | [**CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata**](CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata.md) |  | 
**References** | [**[]GetCveRecordResponseContainersCnaReferencesInner**](GetCveRecordResponseContainersCnaReferencesInner.md) |  | 
**DateAssigned** | Pointer to **time.Time** |  | [optional] 
**DatePublic** | Pointer to **time.Time** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Impacts** | Pointer to **[]string** |  | [optional] 
**Metrics** | Pointer to **[]string** |  | [optional] 
**Configurations** | Pointer to **[]string** |  | [optional] 
**Workarounds** | Pointer to **[]string** |  | [optional] 
**Solutions** | Pointer to **[]string** |  | [optional] 
**Exploits** | Pointer to **[]string** |  | [optional] 
**Timeline** | Pointer to **[]string** |  | [optional] 
**Credits** | Pointer to **[]string** |  | [optional] 
**Source** | Pointer to **string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**TaxonomyMappings** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCreateFullCveRecordRequestContainersCna

`func NewCreateFullCveRecordRequestContainersCna(affected []CreateFullCveRecordRequestContainersCnaAffectedInner, descriptions []GetCveRecordResponseContainersCnaDescriptionsInner, providerMetadata CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata, references []GetCveRecordResponseContainersCnaReferencesInner, ) *CreateFullCveRecordRequestContainersCna`

NewCreateFullCveRecordRequestContainersCna instantiates a new CreateFullCveRecordRequestContainersCna object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateFullCveRecordRequestContainersCnaWithDefaults

`func NewCreateFullCveRecordRequestContainersCnaWithDefaults() *CreateFullCveRecordRequestContainersCna`

NewCreateFullCveRecordRequestContainersCnaWithDefaults instantiates a new CreateFullCveRecordRequestContainersCna object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffected

`func (o *CreateFullCveRecordRequestContainersCna) GetAffected() []CreateFullCveRecordRequestContainersCnaAffectedInner`

GetAffected returns the Affected field if non-nil, zero value otherwise.

### GetAffectedOk

`func (o *CreateFullCveRecordRequestContainersCna) GetAffectedOk() (*[]CreateFullCveRecordRequestContainersCnaAffectedInner, bool)`

GetAffectedOk returns a tuple with the Affected field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffected

`func (o *CreateFullCveRecordRequestContainersCna) SetAffected(v []CreateFullCveRecordRequestContainersCnaAffectedInner)`

SetAffected sets Affected field to given value.


### GetDescriptions

`func (o *CreateFullCveRecordRequestContainersCna) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *CreateFullCveRecordRequestContainersCna) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.


### GetProblemTypes

`func (o *CreateFullCveRecordRequestContainersCna) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner`

GetProblemTypes returns the ProblemTypes field if non-nil, zero value otherwise.

### GetProblemTypesOk

`func (o *CreateFullCveRecordRequestContainersCna) GetProblemTypesOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInner, bool)`

GetProblemTypesOk returns a tuple with the ProblemTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProblemTypes

`func (o *CreateFullCveRecordRequestContainersCna) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner)`

SetProblemTypes sets ProblemTypes field to given value.

### HasProblemTypes

`func (o *CreateFullCveRecordRequestContainersCna) HasProblemTypes() bool`

HasProblemTypes returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *CreateFullCveRecordRequestContainersCna) GetProviderMetadata() CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *CreateFullCveRecordRequestContainersCna) GetProviderMetadataOk() (*CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *CreateFullCveRecordRequestContainersCna) SetProviderMetadata(v CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.


### GetReferences

`func (o *CreateFullCveRecordRequestContainersCna) GetReferences() []GetCveRecordResponseContainersCnaReferencesInner`

GetReferences returns the References field if non-nil, zero value otherwise.

### GetReferencesOk

`func (o *CreateFullCveRecordRequestContainersCna) GetReferencesOk() (*[]GetCveRecordResponseContainersCnaReferencesInner, bool)`

GetReferencesOk returns a tuple with the References field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferences

`func (o *CreateFullCveRecordRequestContainersCna) SetReferences(v []GetCveRecordResponseContainersCnaReferencesInner)`

SetReferences sets References field to given value.


### GetDateAssigned

`func (o *CreateFullCveRecordRequestContainersCna) GetDateAssigned() time.Time`

GetDateAssigned returns the DateAssigned field if non-nil, zero value otherwise.

### GetDateAssignedOk

`func (o *CreateFullCveRecordRequestContainersCna) GetDateAssignedOk() (*time.Time, bool)`

GetDateAssignedOk returns a tuple with the DateAssigned field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateAssigned

`func (o *CreateFullCveRecordRequestContainersCna) SetDateAssigned(v time.Time)`

SetDateAssigned sets DateAssigned field to given value.

### HasDateAssigned

`func (o *CreateFullCveRecordRequestContainersCna) HasDateAssigned() bool`

HasDateAssigned returns a boolean if a field has been set.

### GetDatePublic

`func (o *CreateFullCveRecordRequestContainersCna) GetDatePublic() time.Time`

GetDatePublic returns the DatePublic field if non-nil, zero value otherwise.

### GetDatePublicOk

`func (o *CreateFullCveRecordRequestContainersCna) GetDatePublicOk() (*time.Time, bool)`

GetDatePublicOk returns a tuple with the DatePublic field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDatePublic

`func (o *CreateFullCveRecordRequestContainersCna) SetDatePublic(v time.Time)`

SetDatePublic sets DatePublic field to given value.

### HasDatePublic

`func (o *CreateFullCveRecordRequestContainersCna) HasDatePublic() bool`

HasDatePublic returns a boolean if a field has been set.

### GetTitle

`func (o *CreateFullCveRecordRequestContainersCna) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *CreateFullCveRecordRequestContainersCna) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *CreateFullCveRecordRequestContainersCna) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *CreateFullCveRecordRequestContainersCna) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetImpacts

`func (o *CreateFullCveRecordRequestContainersCna) GetImpacts() []interface{}`

GetImpacts returns the Impacts field if non-nil, zero value otherwise.

### GetImpactsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetImpactsOk() (*[]interface{}, bool)`

GetImpactsOk returns a tuple with the Impacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImpacts

`func (o *CreateFullCveRecordRequestContainersCna) SetImpacts(v []interface{})`

SetImpacts sets Impacts field to given value.

### HasImpacts

`func (o *CreateFullCveRecordRequestContainersCna) HasImpacts() bool`

HasImpacts returns a boolean if a field has been set.

### GetMetrics

`func (o *CreateFullCveRecordRequestContainersCna) GetMetrics() []interface{}`

GetMetrics returns the Metrics field if non-nil, zero value otherwise.

### GetMetricsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetMetricsOk() (*[]interface{}, bool)`

GetMetricsOk returns a tuple with the Metrics field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetrics

`func (o *CreateFullCveRecordRequestContainersCna) SetMetrics(v []interface{})`

SetMetrics sets Metrics field to given value.

### HasMetrics

`func (o *CreateFullCveRecordRequestContainersCna) HasMetrics() bool`

HasMetrics returns a boolean if a field has been set.

### GetConfigurations

`func (o *CreateFullCveRecordRequestContainersCna) GetConfigurations() []interface{}`

GetConfigurations returns the Configurations field if non-nil, zero value otherwise.

### GetConfigurationsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetConfigurationsOk() (*[]interface{}, bool)`

GetConfigurationsOk returns a tuple with the Configurations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfigurations

`func (o *CreateFullCveRecordRequestContainersCna) SetConfigurations(v []interface{})`

SetConfigurations sets Configurations field to given value.

### HasConfigurations

`func (o *CreateFullCveRecordRequestContainersCna) HasConfigurations() bool`

HasConfigurations returns a boolean if a field has been set.

### GetWorkarounds

`func (o *CreateFullCveRecordRequestContainersCna) GetWorkarounds() []interface{}`

GetWorkarounds returns the Workarounds field if non-nil, zero value otherwise.

### GetWorkaroundsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetWorkaroundsOk() (*[]interface{}, bool)`

GetWorkaroundsOk returns a tuple with the Workarounds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWorkarounds

`func (o *CreateFullCveRecordRequestContainersCna) SetWorkarounds(v []interface{})`

SetWorkarounds sets Workarounds field to given value.

### HasWorkarounds

`func (o *CreateFullCveRecordRequestContainersCna) HasWorkarounds() bool`

HasWorkarounds returns a boolean if a field has been set.

### GetSolutions

`func (o *CreateFullCveRecordRequestContainersCna) GetSolutions() []interface{}`

GetSolutions returns the Solutions field if non-nil, zero value otherwise.

### GetSolutionsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetSolutionsOk() (*[]interface{}, bool)`

GetSolutionsOk returns a tuple with the Solutions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSolutions

`func (o *CreateFullCveRecordRequestContainersCna) SetSolutions(v []interface{})`

SetSolutions sets Solutions field to given value.

### HasSolutions

`func (o *CreateFullCveRecordRequestContainersCna) HasSolutions() bool`

HasSolutions returns a boolean if a field has been set.

### GetExploits

`func (o *CreateFullCveRecordRequestContainersCna) GetExploits() []interface{}`

GetExploits returns the Exploits field if non-nil, zero value otherwise.

### GetExploitsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetExploitsOk() (*[]interface{}, bool)`

GetExploitsOk returns a tuple with the Exploits field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExploits

`func (o *CreateFullCveRecordRequestContainersCna) SetExploits(v []interface{})`

SetExploits sets Exploits field to given value.

### HasExploits

`func (o *CreateFullCveRecordRequestContainersCna) HasExploits() bool`

HasExploits returns a boolean if a field has been set.

### GetTimeline

`func (o *CreateFullCveRecordRequestContainersCna) GetTimeline() []interface{}`

GetTimeline returns the Timeline field if non-nil, zero value otherwise.

### GetTimelineOk

`func (o *CreateFullCveRecordRequestContainersCna) GetTimelineOk() (*[]interface{}, bool)`

GetTimelineOk returns a tuple with the Timeline field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeline

`func (o *CreateFullCveRecordRequestContainersCna) SetTimeline(v []interface{})`

SetTimeline sets Timeline field to given value.

### HasTimeline

`func (o *CreateFullCveRecordRequestContainersCna) HasTimeline() bool`

HasTimeline returns a boolean if a field has been set.

### GetCredits

`func (o *CreateFullCveRecordRequestContainersCna) GetCredits() []interface{}`

GetCredits returns the Credits field if non-nil, zero value otherwise.

### GetCreditsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetCreditsOk() (*[]interface{}, bool)`

GetCreditsOk returns a tuple with the Credits field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCredits

`func (o *CreateFullCveRecordRequestContainersCna) SetCredits(v []interface{})`

SetCredits sets Credits field to given value.

### HasCredits

`func (o *CreateFullCveRecordRequestContainersCna) HasCredits() bool`

HasCredits returns a boolean if a field has been set.

### GetSource

`func (o *CreateFullCveRecordRequestContainersCna) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *CreateFullCveRecordRequestContainersCna) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *CreateFullCveRecordRequestContainersCna) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *CreateFullCveRecordRequestContainersCna) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetTags

`func (o *CreateFullCveRecordRequestContainersCna) GetTags() []interface{}`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetTagsOk() (*[]interface{}, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CreateFullCveRecordRequestContainersCna) SetTags(v []interface{})`

SetTags sets Tags field to given value.

### HasTags

`func (o *CreateFullCveRecordRequestContainersCna) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetTaxonomyMappings

`func (o *CreateFullCveRecordRequestContainersCna) GetTaxonomyMappings() []interface{}`

GetTaxonomyMappings returns the TaxonomyMappings field if non-nil, zero value otherwise.

### GetTaxonomyMappingsOk

`func (o *CreateFullCveRecordRequestContainersCna) GetTaxonomyMappingsOk() (*[]interface{}, bool)`

GetTaxonomyMappingsOk returns a tuple with the TaxonomyMappings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaxonomyMappings

`func (o *CreateFullCveRecordRequestContainersCna) SetTaxonomyMappings(v []interface{})`

SetTaxonomyMappings sets TaxonomyMappings field to given value.

### HasTaxonomyMappings

`func (o *CreateFullCveRecordRequestContainersCna) HasTaxonomyMappings() bool`

HasTaxonomyMappings returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


