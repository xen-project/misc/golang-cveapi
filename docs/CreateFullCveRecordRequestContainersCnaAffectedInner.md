# CreateFullCveRecordRequestContainersCnaAffectedInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Vendor** | Pointer to **string** |  | [optional] 
**Product** | Pointer to **string** |  | [optional] 
**Versions** | Pointer to [**[]GetCveRecordResponseContainersCnaAffectedVersionsInner**](GetCveRecordResponseContainersCnaAffectedVersionsInner.md) |  | [optional] 

## Methods

### NewCreateFullCveRecordRequestContainersCnaAffectedInner

`func NewCreateFullCveRecordRequestContainersCnaAffectedInner() *CreateFullCveRecordRequestContainersCnaAffectedInner`

NewCreateFullCveRecordRequestContainersCnaAffectedInner instantiates a new CreateFullCveRecordRequestContainersCnaAffectedInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateFullCveRecordRequestContainersCnaAffectedInnerWithDefaults

`func NewCreateFullCveRecordRequestContainersCnaAffectedInnerWithDefaults() *CreateFullCveRecordRequestContainersCnaAffectedInner`

NewCreateFullCveRecordRequestContainersCnaAffectedInnerWithDefaults instantiates a new CreateFullCveRecordRequestContainersCnaAffectedInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVendor

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetVendor() string`

GetVendor returns the Vendor field if non-nil, zero value otherwise.

### GetVendorOk

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetVendorOk() (*string, bool)`

GetVendorOk returns a tuple with the Vendor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVendor

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) SetVendor(v string)`

SetVendor sets Vendor field to given value.

### HasVendor

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) HasVendor() bool`

HasVendor returns a boolean if a field has been set.

### GetProduct

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetProduct() string`

GetProduct returns the Product field if non-nil, zero value otherwise.

### GetProductOk

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetProductOk() (*string, bool)`

GetProductOk returns a tuple with the Product field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProduct

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) SetProduct(v string)`

SetProduct sets Product field to given value.

### HasProduct

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) HasProduct() bool`

HasProduct returns a boolean if a field has been set.

### GetVersions

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetVersions() []GetCveRecordResponseContainersCnaAffectedVersionsInner`

GetVersions returns the Versions field if non-nil, zero value otherwise.

### GetVersionsOk

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) GetVersionsOk() (*[]GetCveRecordResponseContainersCnaAffectedVersionsInner, bool)`

GetVersionsOk returns a tuple with the Versions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersions

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) SetVersions(v []GetCveRecordResponseContainersCnaAffectedVersionsInner)`

SetVersions sets Versions field to given value.

### HasVersions

`func (o *CreateFullCveRecordRequestContainersCnaAffectedInner) HasVersions() bool`

HasVersions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


