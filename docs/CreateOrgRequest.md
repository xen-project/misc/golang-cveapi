# CreateOrgRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ShortName** | **string** | The short name of the organization | 
**Name** | **string** | The name of the organization | 
**Authority** | Pointer to [**ListOrgsResponseOrganizationsInnerAuthority**](ListOrgsResponseOrganizationsInnerAuthority.md) |  | [optional] 
**Policies** | Pointer to [**CreateOrgRequestPolicies**](CreateOrgRequestPolicies.md) |  | [optional] 

## Methods

### NewCreateOrgRequest

`func NewCreateOrgRequest(shortName string, name string, ) *CreateOrgRequest`

NewCreateOrgRequest instantiates a new CreateOrgRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateOrgRequestWithDefaults

`func NewCreateOrgRequestWithDefaults() *CreateOrgRequest`

NewCreateOrgRequestWithDefaults instantiates a new CreateOrgRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetShortName

`func (o *CreateOrgRequest) GetShortName() string`

GetShortName returns the ShortName field if non-nil, zero value otherwise.

### GetShortNameOk

`func (o *CreateOrgRequest) GetShortNameOk() (*string, bool)`

GetShortNameOk returns a tuple with the ShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortName

`func (o *CreateOrgRequest) SetShortName(v string)`

SetShortName sets ShortName field to given value.


### GetName

`func (o *CreateOrgRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateOrgRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateOrgRequest) SetName(v string)`

SetName sets Name field to given value.


### GetAuthority

`func (o *CreateOrgRequest) GetAuthority() ListOrgsResponseOrganizationsInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *CreateOrgRequest) GetAuthorityOk() (*ListOrgsResponseOrganizationsInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *CreateOrgRequest) SetAuthority(v ListOrgsResponseOrganizationsInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *CreateOrgRequest) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetPolicies

`func (o *CreateOrgRequest) GetPolicies() CreateOrgRequestPolicies`

GetPolicies returns the Policies field if non-nil, zero value otherwise.

### GetPoliciesOk

`func (o *CreateOrgRequest) GetPoliciesOk() (*CreateOrgRequestPolicies, bool)`

GetPoliciesOk returns a tuple with the Policies field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolicies

`func (o *CreateOrgRequest) SetPolicies(v CreateOrgRequestPolicies)`

SetPolicies sets Policies field to given value.

### HasPolicies

`func (o *CreateOrgRequest) HasPolicies() bool`

HasPolicies returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


