# CreateOrgResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** | Success description | [optional] 
**Created** | Pointer to [**CreateOrgResponseCreated**](CreateOrgResponseCreated.md) |  | [optional] 

## Methods

### NewCreateOrgResponse

`func NewCreateOrgResponse() *CreateOrgResponse`

NewCreateOrgResponse instantiates a new CreateOrgResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateOrgResponseWithDefaults

`func NewCreateOrgResponseWithDefaults() *CreateOrgResponse`

NewCreateOrgResponseWithDefaults instantiates a new CreateOrgResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *CreateOrgResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CreateOrgResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CreateOrgResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CreateOrgResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCreated

`func (o *CreateOrgResponse) GetCreated() CreateOrgResponseCreated`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *CreateOrgResponse) GetCreatedOk() (*CreateOrgResponseCreated, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *CreateOrgResponse) SetCreated(v CreateOrgResponseCreated)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *CreateOrgResponse) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


