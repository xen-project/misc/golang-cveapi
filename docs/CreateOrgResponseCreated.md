# CreateOrgResponseCreated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of the organization. | [optional] 
**ShortName** | Pointer to **string** | The short name of the organization. | [optional] 
**UUID** | Pointer to **string** | The identifier of the organization. | [optional] 
**Policies** | Pointer to [**CreateOrgResponseCreatedPolicies**](CreateOrgResponseCreatedPolicies.md) |  | [optional] 
**Authority** | Pointer to [**CreateOrgResponseCreatedAuthority**](CreateOrgResponseCreatedAuthority.md) |  | [optional] 
**Time** | Pointer to [**CreateOrgResponseCreatedTime**](CreateOrgResponseCreatedTime.md) |  | [optional] 

## Methods

### NewCreateOrgResponseCreated

`func NewCreateOrgResponseCreated() *CreateOrgResponseCreated`

NewCreateOrgResponseCreated instantiates a new CreateOrgResponseCreated object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateOrgResponseCreatedWithDefaults

`func NewCreateOrgResponseCreatedWithDefaults() *CreateOrgResponseCreated`

NewCreateOrgResponseCreatedWithDefaults instantiates a new CreateOrgResponseCreated object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateOrgResponseCreated) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateOrgResponseCreated) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateOrgResponseCreated) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CreateOrgResponseCreated) HasName() bool`

HasName returns a boolean if a field has been set.

### GetShortName

`func (o *CreateOrgResponseCreated) GetShortName() string`

GetShortName returns the ShortName field if non-nil, zero value otherwise.

### GetShortNameOk

`func (o *CreateOrgResponseCreated) GetShortNameOk() (*string, bool)`

GetShortNameOk returns a tuple with the ShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortName

`func (o *CreateOrgResponseCreated) SetShortName(v string)`

SetShortName sets ShortName field to given value.

### HasShortName

`func (o *CreateOrgResponseCreated) HasShortName() bool`

HasShortName returns a boolean if a field has been set.

### GetUUID

`func (o *CreateOrgResponseCreated) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *CreateOrgResponseCreated) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *CreateOrgResponseCreated) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *CreateOrgResponseCreated) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetPolicies

`func (o *CreateOrgResponseCreated) GetPolicies() CreateOrgResponseCreatedPolicies`

GetPolicies returns the Policies field if non-nil, zero value otherwise.

### GetPoliciesOk

`func (o *CreateOrgResponseCreated) GetPoliciesOk() (*CreateOrgResponseCreatedPolicies, bool)`

GetPoliciesOk returns a tuple with the Policies field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolicies

`func (o *CreateOrgResponseCreated) SetPolicies(v CreateOrgResponseCreatedPolicies)`

SetPolicies sets Policies field to given value.

### HasPolicies

`func (o *CreateOrgResponseCreated) HasPolicies() bool`

HasPolicies returns a boolean if a field has been set.

### GetAuthority

`func (o *CreateOrgResponseCreated) GetAuthority() CreateOrgResponseCreatedAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *CreateOrgResponseCreated) GetAuthorityOk() (*CreateOrgResponseCreatedAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *CreateOrgResponseCreated) SetAuthority(v CreateOrgResponseCreatedAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *CreateOrgResponseCreated) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *CreateOrgResponseCreated) GetTime() CreateOrgResponseCreatedTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *CreateOrgResponseCreated) GetTimeOk() (*CreateOrgResponseCreatedTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *CreateOrgResponseCreated) SetTime(v CreateOrgResponseCreatedTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *CreateOrgResponseCreated) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


