# CreateOrgResponseCreatedAuthority

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActiveRoles** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCreateOrgResponseCreatedAuthority

`func NewCreateOrgResponseCreatedAuthority() *CreateOrgResponseCreatedAuthority`

NewCreateOrgResponseCreatedAuthority instantiates a new CreateOrgResponseCreatedAuthority object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateOrgResponseCreatedAuthorityWithDefaults

`func NewCreateOrgResponseCreatedAuthorityWithDefaults() *CreateOrgResponseCreatedAuthority`

NewCreateOrgResponseCreatedAuthorityWithDefaults instantiates a new CreateOrgResponseCreatedAuthority object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetActiveRoles

`func (o *CreateOrgResponseCreatedAuthority) GetActiveRoles() []string`

GetActiveRoles returns the ActiveRoles field if non-nil, zero value otherwise.

### GetActiveRolesOk

`func (o *CreateOrgResponseCreatedAuthority) GetActiveRolesOk() (*[]string, bool)`

GetActiveRolesOk returns a tuple with the ActiveRoles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActiveRoles

`func (o *CreateOrgResponseCreatedAuthority) SetActiveRoles(v []string)`

SetActiveRoles sets ActiveRoles field to given value.

### HasActiveRoles

`func (o *CreateOrgResponseCreatedAuthority) HasActiveRoles() bool`

HasActiveRoles returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


