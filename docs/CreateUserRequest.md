# CreateUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | Preferably the user&#39;s email address. Must be 3-128 characters in length; allowed characters are alphanumeric and -_@. | 
**Name** | Pointer to [**ListUsersResponseUsersInnerName**](ListUsersResponseUsersInnerName.md) |  | [optional] 
**Authority** | Pointer to [**ListUsersResponseUsersInnerAuthority**](ListUsersResponseUsersInnerAuthority.md) |  | [optional] 

## Methods

### NewCreateUserRequest

`func NewCreateUserRequest(username string, ) *CreateUserRequest`

NewCreateUserRequest instantiates a new CreateUserRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateUserRequestWithDefaults

`func NewCreateUserRequestWithDefaults() *CreateUserRequest`

NewCreateUserRequestWithDefaults instantiates a new CreateUserRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *CreateUserRequest) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *CreateUserRequest) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *CreateUserRequest) SetUsername(v string)`

SetUsername sets Username field to given value.


### GetName

`func (o *CreateUserRequest) GetName() ListUsersResponseUsersInnerName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateUserRequest) GetNameOk() (*ListUsersResponseUsersInnerName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateUserRequest) SetName(v ListUsersResponseUsersInnerName)`

SetName sets Name field to given value.

### HasName

`func (o *CreateUserRequest) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAuthority

`func (o *CreateUserRequest) GetAuthority() ListUsersResponseUsersInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *CreateUserRequest) GetAuthorityOk() (*ListUsersResponseUsersInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *CreateUserRequest) SetAuthority(v ListUsersResponseUsersInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *CreateUserRequest) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


