# CreateUserResponseCreated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | Pointer to **string** | The user name of the user | [optional] 
**OrgUUID** | Pointer to **string** | The identifier of the organization the user belongs to | [optional] 
**UUID** | Pointer to **string** | The identifier of the user | [optional] 
**Active** | Pointer to **string** | The user is an active user of the organization | [optional] 
**Name** | Pointer to [**ListUsersResponseUsersInnerName**](ListUsersResponseUsersInnerName.md) |  | [optional] 
**Secret** | Pointer to **string** | The API key of the user | [optional] 
**Authority** | Pointer to [**ListUsersResponseUsersInnerAuthority**](ListUsersResponseUsersInnerAuthority.md) |  | [optional] 
**Time** | Pointer to [**ListUsersResponseUsersInnerTime**](ListUsersResponseUsersInnerTime.md) |  | [optional] 

## Methods

### NewCreateUserResponseCreated

`func NewCreateUserResponseCreated() *CreateUserResponseCreated`

NewCreateUserResponseCreated instantiates a new CreateUserResponseCreated object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateUserResponseCreatedWithDefaults

`func NewCreateUserResponseCreatedWithDefaults() *CreateUserResponseCreated`

NewCreateUserResponseCreatedWithDefaults instantiates a new CreateUserResponseCreated object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *CreateUserResponseCreated) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *CreateUserResponseCreated) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *CreateUserResponseCreated) SetUsername(v string)`

SetUsername sets Username field to given value.

### HasUsername

`func (o *CreateUserResponseCreated) HasUsername() bool`

HasUsername returns a boolean if a field has been set.

### GetOrgUUID

`func (o *CreateUserResponseCreated) GetOrgUUID() string`

GetOrgUUID returns the OrgUUID field if non-nil, zero value otherwise.

### GetOrgUUIDOk

`func (o *CreateUserResponseCreated) GetOrgUUIDOk() (*string, bool)`

GetOrgUUIDOk returns a tuple with the OrgUUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgUUID

`func (o *CreateUserResponseCreated) SetOrgUUID(v string)`

SetOrgUUID sets OrgUUID field to given value.

### HasOrgUUID

`func (o *CreateUserResponseCreated) HasOrgUUID() bool`

HasOrgUUID returns a boolean if a field has been set.

### GetUUID

`func (o *CreateUserResponseCreated) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *CreateUserResponseCreated) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *CreateUserResponseCreated) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *CreateUserResponseCreated) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetActive

`func (o *CreateUserResponseCreated) GetActive() string`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *CreateUserResponseCreated) GetActiveOk() (*string, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *CreateUserResponseCreated) SetActive(v string)`

SetActive sets Active field to given value.

### HasActive

`func (o *CreateUserResponseCreated) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetName

`func (o *CreateUserResponseCreated) GetName() ListUsersResponseUsersInnerName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateUserResponseCreated) GetNameOk() (*ListUsersResponseUsersInnerName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateUserResponseCreated) SetName(v ListUsersResponseUsersInnerName)`

SetName sets Name field to given value.

### HasName

`func (o *CreateUserResponseCreated) HasName() bool`

HasName returns a boolean if a field has been set.

### GetSecret

`func (o *CreateUserResponseCreated) GetSecret() string`

GetSecret returns the Secret field if non-nil, zero value otherwise.

### GetSecretOk

`func (o *CreateUserResponseCreated) GetSecretOk() (*string, bool)`

GetSecretOk returns a tuple with the Secret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecret

`func (o *CreateUserResponseCreated) SetSecret(v string)`

SetSecret sets Secret field to given value.

### HasSecret

`func (o *CreateUserResponseCreated) HasSecret() bool`

HasSecret returns a boolean if a field has been set.

### GetAuthority

`func (o *CreateUserResponseCreated) GetAuthority() ListUsersResponseUsersInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *CreateUserResponseCreated) GetAuthorityOk() (*ListUsersResponseUsersInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *CreateUserResponseCreated) SetAuthority(v ListUsersResponseUsersInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *CreateUserResponseCreated) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *CreateUserResponseCreated) GetTime() ListUsersResponseUsersInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *CreateUserResponseCreated) GetTimeOk() (*ListUsersResponseUsersInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *CreateUserResponseCreated) SetTime(v ListUsersResponseUsersInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *CreateUserResponseCreated) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


