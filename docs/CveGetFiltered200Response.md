# CveGetFiltered200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCount** | Pointer to **int32** |  | [optional] 
**ItemsPerPage** | Pointer to **int32** |  | [optional] 
**PageCount** | Pointer to **int32** |  | [optional] 
**CurrentPage** | Pointer to **int32** |  | [optional] 
**PrevPage** | Pointer to **int32** |  | [optional] 
**NextPage** | Pointer to **int32** |  | [optional] 
**CveRecords** | Pointer to [**[]ListCveRecordsResponseCveRecordsInner**](ListCveRecordsResponseCveRecordsInner.md) |  | [optional] 
**Message** | Pointer to **string** |  | [optional] 
**Created** | Pointer to [**CreateCveRecordRejectionResponseCreated**](CreateCveRecordRejectionResponseCreated.md) |  | [optional] 

## Methods

### NewCveGetFiltered200Response

`func NewCveGetFiltered200Response() *CveGetFiltered200Response`

NewCveGetFiltered200Response instantiates a new CveGetFiltered200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCveGetFiltered200ResponseWithDefaults

`func NewCveGetFiltered200ResponseWithDefaults() *CveGetFiltered200Response`

NewCveGetFiltered200ResponseWithDefaults instantiates a new CveGetFiltered200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotalCount

`func (o *CveGetFiltered200Response) GetTotalCount() int32`

GetTotalCount returns the TotalCount field if non-nil, zero value otherwise.

### GetTotalCountOk

`func (o *CveGetFiltered200Response) GetTotalCountOk() (*int32, bool)`

GetTotalCountOk returns a tuple with the TotalCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalCount

`func (o *CveGetFiltered200Response) SetTotalCount(v int32)`

SetTotalCount sets TotalCount field to given value.

### HasTotalCount

`func (o *CveGetFiltered200Response) HasTotalCount() bool`

HasTotalCount returns a boolean if a field has been set.

### GetItemsPerPage

`func (o *CveGetFiltered200Response) GetItemsPerPage() int32`

GetItemsPerPage returns the ItemsPerPage field if non-nil, zero value otherwise.

### GetItemsPerPageOk

`func (o *CveGetFiltered200Response) GetItemsPerPageOk() (*int32, bool)`

GetItemsPerPageOk returns a tuple with the ItemsPerPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemsPerPage

`func (o *CveGetFiltered200Response) SetItemsPerPage(v int32)`

SetItemsPerPage sets ItemsPerPage field to given value.

### HasItemsPerPage

`func (o *CveGetFiltered200Response) HasItemsPerPage() bool`

HasItemsPerPage returns a boolean if a field has been set.

### GetPageCount

`func (o *CveGetFiltered200Response) GetPageCount() int32`

GetPageCount returns the PageCount field if non-nil, zero value otherwise.

### GetPageCountOk

`func (o *CveGetFiltered200Response) GetPageCountOk() (*int32, bool)`

GetPageCountOk returns a tuple with the PageCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageCount

`func (o *CveGetFiltered200Response) SetPageCount(v int32)`

SetPageCount sets PageCount field to given value.

### HasPageCount

`func (o *CveGetFiltered200Response) HasPageCount() bool`

HasPageCount returns a boolean if a field has been set.

### GetCurrentPage

`func (o *CveGetFiltered200Response) GetCurrentPage() int32`

GetCurrentPage returns the CurrentPage field if non-nil, zero value otherwise.

### GetCurrentPageOk

`func (o *CveGetFiltered200Response) GetCurrentPageOk() (*int32, bool)`

GetCurrentPageOk returns a tuple with the CurrentPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentPage

`func (o *CveGetFiltered200Response) SetCurrentPage(v int32)`

SetCurrentPage sets CurrentPage field to given value.

### HasCurrentPage

`func (o *CveGetFiltered200Response) HasCurrentPage() bool`

HasCurrentPage returns a boolean if a field has been set.

### GetPrevPage

`func (o *CveGetFiltered200Response) GetPrevPage() int32`

GetPrevPage returns the PrevPage field if non-nil, zero value otherwise.

### GetPrevPageOk

`func (o *CveGetFiltered200Response) GetPrevPageOk() (*int32, bool)`

GetPrevPageOk returns a tuple with the PrevPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevPage

`func (o *CveGetFiltered200Response) SetPrevPage(v int32)`

SetPrevPage sets PrevPage field to given value.

### HasPrevPage

`func (o *CveGetFiltered200Response) HasPrevPage() bool`

HasPrevPage returns a boolean if a field has been set.

### GetNextPage

`func (o *CveGetFiltered200Response) GetNextPage() int32`

GetNextPage returns the NextPage field if non-nil, zero value otherwise.

### GetNextPageOk

`func (o *CveGetFiltered200Response) GetNextPageOk() (*int32, bool)`

GetNextPageOk returns a tuple with the NextPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextPage

`func (o *CveGetFiltered200Response) SetNextPage(v int32)`

SetNextPage sets NextPage field to given value.

### HasNextPage

`func (o *CveGetFiltered200Response) HasNextPage() bool`

HasNextPage returns a boolean if a field has been set.

### GetCveRecords

`func (o *CveGetFiltered200Response) GetCveRecords() []ListCveRecordsResponseCveRecordsInner`

GetCveRecords returns the CveRecords field if non-nil, zero value otherwise.

### GetCveRecordsOk

`func (o *CveGetFiltered200Response) GetCveRecordsOk() (*[]ListCveRecordsResponseCveRecordsInner, bool)`

GetCveRecordsOk returns a tuple with the CveRecords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveRecords

`func (o *CveGetFiltered200Response) SetCveRecords(v []ListCveRecordsResponseCveRecordsInner)`

SetCveRecords sets CveRecords field to given value.

### HasCveRecords

`func (o *CveGetFiltered200Response) HasCveRecords() bool`

HasCveRecords returns a boolean if a field has been set.

### GetMessage

`func (o *CveGetFiltered200Response) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CveGetFiltered200Response) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CveGetFiltered200Response) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CveGetFiltered200Response) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCreated

`func (o *CveGetFiltered200Response) GetCreated() CreateCveRecordRejectionResponseCreated`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *CveGetFiltered200Response) GetCreatedOk() (*CreateCveRecordRejectionResponseCreated, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *CveGetFiltered200Response) SetCreated(v CreateCveRecordRejectionResponseCreated)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *CveGetFiltered200Response) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


