# CveGetSingle200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Containers** | Pointer to [**GetCveRecordResponseContainers**](GetCveRecordResponseContainers.md) |  | [optional] 
**CveMetadata** | Pointer to [**GetCveRecordResponseCveMetadata**](GetCveRecordResponseCveMetadata.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DateVersion** | Pointer to **int32** |  | [optional] 
**Message** | Pointer to **string** |  | [optional] 
**Created** | Pointer to [**CreateCveRecordRejectionResponseCreated**](CreateCveRecordRejectionResponseCreated.md) |  | [optional] 

## Methods

### NewCveGetSingle200Response

`func NewCveGetSingle200Response() *CveGetSingle200Response`

NewCveGetSingle200Response instantiates a new CveGetSingle200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCveGetSingle200ResponseWithDefaults

`func NewCveGetSingle200ResponseWithDefaults() *CveGetSingle200Response`

NewCveGetSingle200ResponseWithDefaults instantiates a new CveGetSingle200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContainers

`func (o *CveGetSingle200Response) GetContainers() GetCveRecordResponseContainers`

GetContainers returns the Containers field if non-nil, zero value otherwise.

### GetContainersOk

`func (o *CveGetSingle200Response) GetContainersOk() (*GetCveRecordResponseContainers, bool)`

GetContainersOk returns a tuple with the Containers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContainers

`func (o *CveGetSingle200Response) SetContainers(v GetCveRecordResponseContainers)`

SetContainers sets Containers field to given value.

### HasContainers

`func (o *CveGetSingle200Response) HasContainers() bool`

HasContainers returns a boolean if a field has been set.

### GetCveMetadata

`func (o *CveGetSingle200Response) GetCveMetadata() GetCveRecordResponseCveMetadata`

GetCveMetadata returns the CveMetadata field if non-nil, zero value otherwise.

### GetCveMetadataOk

`func (o *CveGetSingle200Response) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool)`

GetCveMetadataOk returns a tuple with the CveMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveMetadata

`func (o *CveGetSingle200Response) SetCveMetadata(v GetCveRecordResponseCveMetadata)`

SetCveMetadata sets CveMetadata field to given value.

### HasCveMetadata

`func (o *CveGetSingle200Response) HasCveMetadata() bool`

HasCveMetadata returns a boolean if a field has been set.

### GetDataType

`func (o *CveGetSingle200Response) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *CveGetSingle200Response) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *CveGetSingle200Response) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *CveGetSingle200Response) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDateVersion

`func (o *CveGetSingle200Response) GetDateVersion() int32`

GetDateVersion returns the DateVersion field if non-nil, zero value otherwise.

### GetDateVersionOk

`func (o *CveGetSingle200Response) GetDateVersionOk() (*int32, bool)`

GetDateVersionOk returns a tuple with the DateVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateVersion

`func (o *CveGetSingle200Response) SetDateVersion(v int32)`

SetDateVersion sets DateVersion field to given value.

### HasDateVersion

`func (o *CveGetSingle200Response) HasDateVersion() bool`

HasDateVersion returns a boolean if a field has been set.

### GetMessage

`func (o *CveGetSingle200Response) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CveGetSingle200Response) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CveGetSingle200Response) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CveGetSingle200Response) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCreated

`func (o *CveGetSingle200Response) GetCreated() CreateCveRecordRejectionResponseCreated`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *CveGetSingle200Response) GetCreatedOk() (*CreateCveRecordRejectionResponseCreated, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *CveGetSingle200Response) SetCreated(v CreateCveRecordRejectionResponseCreated)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *CveGetSingle200Response) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


