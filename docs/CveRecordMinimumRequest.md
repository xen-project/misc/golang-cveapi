# CveRecordMinimumRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CnaContainer** | [**CveRecordMinimumRequestCnaContainer**](CveRecordMinimumRequestCnaContainer.md) |  | 

## Methods

### NewCveRecordMinimumRequest

`func NewCveRecordMinimumRequest(cnaContainer CveRecordMinimumRequestCnaContainer, ) *CveRecordMinimumRequest`

NewCveRecordMinimumRequest instantiates a new CveRecordMinimumRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCveRecordMinimumRequestWithDefaults

`func NewCveRecordMinimumRequestWithDefaults() *CveRecordMinimumRequest`

NewCveRecordMinimumRequestWithDefaults instantiates a new CveRecordMinimumRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCnaContainer

`func (o *CveRecordMinimumRequest) GetCnaContainer() CveRecordMinimumRequestCnaContainer`

GetCnaContainer returns the CnaContainer field if non-nil, zero value otherwise.

### GetCnaContainerOk

`func (o *CveRecordMinimumRequest) GetCnaContainerOk() (*CveRecordMinimumRequestCnaContainer, bool)`

GetCnaContainerOk returns a tuple with the CnaContainer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCnaContainer

`func (o *CveRecordMinimumRequest) SetCnaContainer(v CveRecordMinimumRequestCnaContainer)`

SetCnaContainer sets CnaContainer field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


