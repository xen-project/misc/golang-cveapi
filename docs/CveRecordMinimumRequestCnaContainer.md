# CveRecordMinimumRequestCnaContainer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Affected** | [**[]CreateFullCveRecordRequestContainersCnaAffectedInner**](CreateFullCveRecordRequestContainersCnaAffectedInner.md) |  | 
**Descriptions** | [**[]GetCveRecordResponseContainersCnaDescriptionsInner**](GetCveRecordResponseContainersCnaDescriptionsInner.md) |  | 
**ProblemTypes** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInner**](GetCveRecordResponseContainersCnaProblemTypesInner.md) |  | [optional] 
**ProviderMetadata** | Pointer to [**GetCveRecordResponseContainersCnaProviderMetadata**](GetCveRecordResponseContainersCnaProviderMetadata.md) |  | [optional] 
**References** | [**[]GetCveRecordResponseContainersCnaReferencesInner**](GetCveRecordResponseContainersCnaReferencesInner.md) |  | 

## Methods

### NewCveRecordMinimumRequestCnaContainer

`func NewCveRecordMinimumRequestCnaContainer(affected []CreateFullCveRecordRequestContainersCnaAffectedInner, descriptions []GetCveRecordResponseContainersCnaDescriptionsInner, references []GetCveRecordResponseContainersCnaReferencesInner, ) *CveRecordMinimumRequestCnaContainer`

NewCveRecordMinimumRequestCnaContainer instantiates a new CveRecordMinimumRequestCnaContainer object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCveRecordMinimumRequestCnaContainerWithDefaults

`func NewCveRecordMinimumRequestCnaContainerWithDefaults() *CveRecordMinimumRequestCnaContainer`

NewCveRecordMinimumRequestCnaContainerWithDefaults instantiates a new CveRecordMinimumRequestCnaContainer object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffected

`func (o *CveRecordMinimumRequestCnaContainer) GetAffected() []CreateFullCveRecordRequestContainersCnaAffectedInner`

GetAffected returns the Affected field if non-nil, zero value otherwise.

### GetAffectedOk

`func (o *CveRecordMinimumRequestCnaContainer) GetAffectedOk() (*[]CreateFullCveRecordRequestContainersCnaAffectedInner, bool)`

GetAffectedOk returns a tuple with the Affected field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffected

`func (o *CveRecordMinimumRequestCnaContainer) SetAffected(v []CreateFullCveRecordRequestContainersCnaAffectedInner)`

SetAffected sets Affected field to given value.


### GetDescriptions

`func (o *CveRecordMinimumRequestCnaContainer) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *CveRecordMinimumRequestCnaContainer) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *CveRecordMinimumRequestCnaContainer) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.


### GetProblemTypes

`func (o *CveRecordMinimumRequestCnaContainer) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner`

GetProblemTypes returns the ProblemTypes field if non-nil, zero value otherwise.

### GetProblemTypesOk

`func (o *CveRecordMinimumRequestCnaContainer) GetProblemTypesOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInner, bool)`

GetProblemTypesOk returns a tuple with the ProblemTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProblemTypes

`func (o *CveRecordMinimumRequestCnaContainer) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner)`

SetProblemTypes sets ProblemTypes field to given value.

### HasProblemTypes

`func (o *CveRecordMinimumRequestCnaContainer) HasProblemTypes() bool`

HasProblemTypes returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *CveRecordMinimumRequestCnaContainer) GetProviderMetadata() GetCveRecordResponseContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *CveRecordMinimumRequestCnaContainer) GetProviderMetadataOk() (*GetCveRecordResponseContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *CveRecordMinimumRequestCnaContainer) SetProviderMetadata(v GetCveRecordResponseContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *CveRecordMinimumRequestCnaContainer) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.

### GetReferences

`func (o *CveRecordMinimumRequestCnaContainer) GetReferences() []GetCveRecordResponseContainersCnaReferencesInner`

GetReferences returns the References field if non-nil, zero value otherwise.

### GetReferencesOk

`func (o *CveRecordMinimumRequestCnaContainer) GetReferencesOk() (*[]GetCveRecordResponseContainersCnaReferencesInner, bool)`

GetReferencesOk returns a tuple with the References field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferences

`func (o *CveRecordMinimumRequestCnaContainer) SetReferences(v []GetCveRecordResponseContainersCnaReferencesInner)`

SetReferences sets References field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


