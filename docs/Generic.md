# Generic

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Error** | Pointer to **string** | Error name | [optional] 
**Message** | Pointer to **string** | Error description | [optional] 

## Methods

### NewGeneric

`func NewGeneric() *Generic`

NewGeneric instantiates a new Generic object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGenericWithDefaults

`func NewGenericWithDefaults() *Generic`

NewGenericWithDefaults instantiates a new Generic object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetError

`func (o *Generic) GetError() string`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *Generic) GetErrorOk() (*string, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *Generic) SetError(v string)`

SetError sets Error field to given value.

### HasError

`func (o *Generic) HasError() bool`

HasError returns a boolean if a field has been set.

### GetMessage

`func (o *Generic) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *Generic) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *Generic) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *Generic) HasMessage() bool`

HasMessage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


