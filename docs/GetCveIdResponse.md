# GetCveIdResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**CveId** | Pointer to **string** | ^CVE-[0-9]{4}-[0-9]{4,}$ | [optional] 
**CveYear** | Pointer to **string** | ^[0-9]{4}$ | [optional] 
**OwningCna** | Pointer to **string** | The shortname for the organization that owns the CVE ID | [optional] 
**State** | Pointer to **string** |  | [optional] 
**RequestedBy** | Pointer to [**ListCveIdsResponseCveIdsInnerRequestedBy**](ListCveIdsResponseCveIdsInnerRequestedBy.md) |  | [optional] 
**Reserved** | Pointer to **time.Time** | The time the ID was reserved | [optional] 
**Time** | Pointer to [**ListCveIdsResponseCveIdsInnerTime**](ListCveIdsResponseCveIdsInnerTime.md) |  | [optional] 

## Methods

### NewGetCveIdResponse

`func NewGetCveIdResponse() *GetCveIdResponse`

NewGetCveIdResponse instantiates a new GetCveIdResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveIdResponseWithDefaults

`func NewGetCveIdResponseWithDefaults() *GetCveIdResponse`

NewGetCveIdResponseWithDefaults instantiates a new GetCveIdResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *GetCveIdResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *GetCveIdResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *GetCveIdResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *GetCveIdResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCveId

`func (o *GetCveIdResponse) GetCveId() string`

GetCveId returns the CveId field if non-nil, zero value otherwise.

### GetCveIdOk

`func (o *GetCveIdResponse) GetCveIdOk() (*string, bool)`

GetCveIdOk returns a tuple with the CveId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveId

`func (o *GetCveIdResponse) SetCveId(v string)`

SetCveId sets CveId field to given value.

### HasCveId

`func (o *GetCveIdResponse) HasCveId() bool`

HasCveId returns a boolean if a field has been set.

### GetCveYear

`func (o *GetCveIdResponse) GetCveYear() string`

GetCveYear returns the CveYear field if non-nil, zero value otherwise.

### GetCveYearOk

`func (o *GetCveIdResponse) GetCveYearOk() (*string, bool)`

GetCveYearOk returns a tuple with the CveYear field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveYear

`func (o *GetCveIdResponse) SetCveYear(v string)`

SetCveYear sets CveYear field to given value.

### HasCveYear

`func (o *GetCveIdResponse) HasCveYear() bool`

HasCveYear returns a boolean if a field has been set.

### GetOwningCna

`func (o *GetCveIdResponse) GetOwningCna() string`

GetOwningCna returns the OwningCna field if non-nil, zero value otherwise.

### GetOwningCnaOk

`func (o *GetCveIdResponse) GetOwningCnaOk() (*string, bool)`

GetOwningCnaOk returns a tuple with the OwningCna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwningCna

`func (o *GetCveIdResponse) SetOwningCna(v string)`

SetOwningCna sets OwningCna field to given value.

### HasOwningCna

`func (o *GetCveIdResponse) HasOwningCna() bool`

HasOwningCna returns a boolean if a field has been set.

### GetState

`func (o *GetCveIdResponse) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *GetCveIdResponse) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *GetCveIdResponse) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *GetCveIdResponse) HasState() bool`

HasState returns a boolean if a field has been set.

### GetRequestedBy

`func (o *GetCveIdResponse) GetRequestedBy() ListCveIdsResponseCveIdsInnerRequestedBy`

GetRequestedBy returns the RequestedBy field if non-nil, zero value otherwise.

### GetRequestedByOk

`func (o *GetCveIdResponse) GetRequestedByOk() (*ListCveIdsResponseCveIdsInnerRequestedBy, bool)`

GetRequestedByOk returns a tuple with the RequestedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedBy

`func (o *GetCveIdResponse) SetRequestedBy(v ListCveIdsResponseCveIdsInnerRequestedBy)`

SetRequestedBy sets RequestedBy field to given value.

### HasRequestedBy

`func (o *GetCveIdResponse) HasRequestedBy() bool`

HasRequestedBy returns a boolean if a field has been set.

### GetReserved

`func (o *GetCveIdResponse) GetReserved() time.Time`

GetReserved returns the Reserved field if non-nil, zero value otherwise.

### GetReservedOk

`func (o *GetCveIdResponse) GetReservedOk() (*time.Time, bool)`

GetReservedOk returns a tuple with the Reserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReserved

`func (o *GetCveIdResponse) SetReserved(v time.Time)`

SetReserved sets Reserved field to given value.

### HasReserved

`func (o *GetCveIdResponse) HasReserved() bool`

HasReserved returns a boolean if a field has been set.

### GetTime

`func (o *GetCveIdResponse) GetTime() ListCveIdsResponseCveIdsInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *GetCveIdResponse) GetTimeOk() (*ListCveIdsResponseCveIdsInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *GetCveIdResponse) SetTime(v ListCveIdsResponseCveIdsInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *GetCveIdResponse) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


