# GetCveRecordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Containers** | Pointer to [**GetCveRecordResponseContainers**](GetCveRecordResponseContainers.md) |  | [optional] 
**CveMetadata** | Pointer to [**GetCveRecordResponseCveMetadata**](GetCveRecordResponseCveMetadata.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DateVersion** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetCveRecordResponse

`func NewGetCveRecordResponse() *GetCveRecordResponse`

NewGetCveRecordResponse instantiates a new GetCveRecordResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseWithDefaults

`func NewGetCveRecordResponseWithDefaults() *GetCveRecordResponse`

NewGetCveRecordResponseWithDefaults instantiates a new GetCveRecordResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContainers

`func (o *GetCveRecordResponse) GetContainers() GetCveRecordResponseContainers`

GetContainers returns the Containers field if non-nil, zero value otherwise.

### GetContainersOk

`func (o *GetCveRecordResponse) GetContainersOk() (*GetCveRecordResponseContainers, bool)`

GetContainersOk returns a tuple with the Containers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContainers

`func (o *GetCveRecordResponse) SetContainers(v GetCveRecordResponseContainers)`

SetContainers sets Containers field to given value.

### HasContainers

`func (o *GetCveRecordResponse) HasContainers() bool`

HasContainers returns a boolean if a field has been set.

### GetCveMetadata

`func (o *GetCveRecordResponse) GetCveMetadata() GetCveRecordResponseCveMetadata`

GetCveMetadata returns the CveMetadata field if non-nil, zero value otherwise.

### GetCveMetadataOk

`func (o *GetCveRecordResponse) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool)`

GetCveMetadataOk returns a tuple with the CveMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveMetadata

`func (o *GetCveRecordResponse) SetCveMetadata(v GetCveRecordResponseCveMetadata)`

SetCveMetadata sets CveMetadata field to given value.

### HasCveMetadata

`func (o *GetCveRecordResponse) HasCveMetadata() bool`

HasCveMetadata returns a boolean if a field has been set.

### GetDataType

`func (o *GetCveRecordResponse) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *GetCveRecordResponse) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *GetCveRecordResponse) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *GetCveRecordResponse) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDateVersion

`func (o *GetCveRecordResponse) GetDateVersion() int32`

GetDateVersion returns the DateVersion field if non-nil, zero value otherwise.

### GetDateVersionOk

`func (o *GetCveRecordResponse) GetDateVersionOk() (*int32, bool)`

GetDateVersionOk returns a tuple with the DateVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateVersion

`func (o *GetCveRecordResponse) SetDateVersion(v int32)`

SetDateVersion sets DateVersion field to given value.

### HasDateVersion

`func (o *GetCveRecordResponse) HasDateVersion() bool`

HasDateVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


