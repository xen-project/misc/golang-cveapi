# GetCveRecordResponseContainersCna

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Affected** | Pointer to [**GetCveRecordResponseContainersCnaAffected**](GetCveRecordResponseContainersCnaAffected.md) |  | [optional] 
**Descriptions** | Pointer to [**[]GetCveRecordResponseContainersCnaDescriptionsInner**](GetCveRecordResponseContainersCnaDescriptionsInner.md) |  | [optional] 
**ProblemTypes** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInner**](GetCveRecordResponseContainersCnaProblemTypesInner.md) |  | [optional] 
**ProviderMetadata** | Pointer to [**GetCveRecordResponseContainersCnaProviderMetadata**](GetCveRecordResponseContainersCnaProviderMetadata.md) |  | [optional] 
**References** | Pointer to [**[]GetCveRecordResponseContainersCnaReferencesInner**](GetCveRecordResponseContainersCnaReferencesInner.md) |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCna

`func NewGetCveRecordResponseContainersCna() *GetCveRecordResponseContainersCna`

NewGetCveRecordResponseContainersCna instantiates a new GetCveRecordResponseContainersCna object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaWithDefaults

`func NewGetCveRecordResponseContainersCnaWithDefaults() *GetCveRecordResponseContainersCna`

NewGetCveRecordResponseContainersCnaWithDefaults instantiates a new GetCveRecordResponseContainersCna object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffected

`func (o *GetCveRecordResponseContainersCna) GetAffected() GetCveRecordResponseContainersCnaAffected`

GetAffected returns the Affected field if non-nil, zero value otherwise.

### GetAffectedOk

`func (o *GetCveRecordResponseContainersCna) GetAffectedOk() (*GetCveRecordResponseContainersCnaAffected, bool)`

GetAffectedOk returns a tuple with the Affected field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffected

`func (o *GetCveRecordResponseContainersCna) SetAffected(v GetCveRecordResponseContainersCnaAffected)`

SetAffected sets Affected field to given value.

### HasAffected

`func (o *GetCveRecordResponseContainersCna) HasAffected() bool`

HasAffected returns a boolean if a field has been set.

### GetDescriptions

`func (o *GetCveRecordResponseContainersCna) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *GetCveRecordResponseContainersCna) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *GetCveRecordResponseContainersCna) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.

### HasDescriptions

`func (o *GetCveRecordResponseContainersCna) HasDescriptions() bool`

HasDescriptions returns a boolean if a field has been set.

### GetProblemTypes

`func (o *GetCveRecordResponseContainersCna) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner`

GetProblemTypes returns the ProblemTypes field if non-nil, zero value otherwise.

### GetProblemTypesOk

`func (o *GetCveRecordResponseContainersCna) GetProblemTypesOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInner, bool)`

GetProblemTypesOk returns a tuple with the ProblemTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProblemTypes

`func (o *GetCveRecordResponseContainersCna) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner)`

SetProblemTypes sets ProblemTypes field to given value.

### HasProblemTypes

`func (o *GetCveRecordResponseContainersCna) HasProblemTypes() bool`

HasProblemTypes returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *GetCveRecordResponseContainersCna) GetProviderMetadata() GetCveRecordResponseContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *GetCveRecordResponseContainersCna) GetProviderMetadataOk() (*GetCveRecordResponseContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *GetCveRecordResponseContainersCna) SetProviderMetadata(v GetCveRecordResponseContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *GetCveRecordResponseContainersCna) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.

### GetReferences

`func (o *GetCveRecordResponseContainersCna) GetReferences() []GetCveRecordResponseContainersCnaReferencesInner`

GetReferences returns the References field if non-nil, zero value otherwise.

### GetReferencesOk

`func (o *GetCveRecordResponseContainersCna) GetReferencesOk() (*[]GetCveRecordResponseContainersCnaReferencesInner, bool)`

GetReferencesOk returns a tuple with the References field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferences

`func (o *GetCveRecordResponseContainersCna) SetReferences(v []GetCveRecordResponseContainersCnaReferencesInner)`

SetReferences sets References field to given value.

### HasReferences

`func (o *GetCveRecordResponseContainersCna) HasReferences() bool`

HasReferences returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


