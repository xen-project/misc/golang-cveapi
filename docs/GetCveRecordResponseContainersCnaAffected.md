# GetCveRecordResponseContainersCnaAffected

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Vendor** | Pointer to **string** |  | [optional] 
**Versions** | Pointer to [**[]GetCveRecordResponseContainersCnaAffectedVersionsInner**](GetCveRecordResponseContainersCnaAffectedVersionsInner.md) |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaAffected

`func NewGetCveRecordResponseContainersCnaAffected() *GetCveRecordResponseContainersCnaAffected`

NewGetCveRecordResponseContainersCnaAffected instantiates a new GetCveRecordResponseContainersCnaAffected object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaAffectedWithDefaults

`func NewGetCveRecordResponseContainersCnaAffectedWithDefaults() *GetCveRecordResponseContainersCnaAffected`

NewGetCveRecordResponseContainersCnaAffectedWithDefaults instantiates a new GetCveRecordResponseContainersCnaAffected object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVendor

`func (o *GetCveRecordResponseContainersCnaAffected) GetVendor() string`

GetVendor returns the Vendor field if non-nil, zero value otherwise.

### GetVendorOk

`func (o *GetCveRecordResponseContainersCnaAffected) GetVendorOk() (*string, bool)`

GetVendorOk returns a tuple with the Vendor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVendor

`func (o *GetCveRecordResponseContainersCnaAffected) SetVendor(v string)`

SetVendor sets Vendor field to given value.

### HasVendor

`func (o *GetCveRecordResponseContainersCnaAffected) HasVendor() bool`

HasVendor returns a boolean if a field has been set.

### GetVersions

`func (o *GetCveRecordResponseContainersCnaAffected) GetVersions() []GetCveRecordResponseContainersCnaAffectedVersionsInner`

GetVersions returns the Versions field if non-nil, zero value otherwise.

### GetVersionsOk

`func (o *GetCveRecordResponseContainersCnaAffected) GetVersionsOk() (*[]GetCveRecordResponseContainersCnaAffectedVersionsInner, bool)`

GetVersionsOk returns a tuple with the Versions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersions

`func (o *GetCveRecordResponseContainersCnaAffected) SetVersions(v []GetCveRecordResponseContainersCnaAffectedVersionsInner)`

SetVersions sets Versions field to given value.

### HasVersions

`func (o *GetCveRecordResponseContainersCnaAffected) HasVersions() bool`

HasVersions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


