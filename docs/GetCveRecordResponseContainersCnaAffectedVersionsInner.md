# GetCveRecordResponseContainersCnaAffectedVersionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Version** | Pointer to **string** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaAffectedVersionsInner

`func NewGetCveRecordResponseContainersCnaAffectedVersionsInner() *GetCveRecordResponseContainersCnaAffectedVersionsInner`

NewGetCveRecordResponseContainersCnaAffectedVersionsInner instantiates a new GetCveRecordResponseContainersCnaAffectedVersionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaAffectedVersionsInnerWithDefaults

`func NewGetCveRecordResponseContainersCnaAffectedVersionsInnerWithDefaults() *GetCveRecordResponseContainersCnaAffectedVersionsInner`

NewGetCveRecordResponseContainersCnaAffectedVersionsInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaAffectedVersionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVersion

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) SetVersion(v string)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) HasVersion() bool`

HasVersion returns a boolean if a field has been set.

### GetStatus

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *GetCveRecordResponseContainersCnaAffectedVersionsInner) HasStatus() bool`

HasStatus returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


