# GetCveRecordResponseContainersCnaProblemTypesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Descriptions** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner**](GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner.md) |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaProblemTypesInner

`func NewGetCveRecordResponseContainersCnaProblemTypesInner() *GetCveRecordResponseContainersCnaProblemTypesInner`

NewGetCveRecordResponseContainersCnaProblemTypesInner instantiates a new GetCveRecordResponseContainersCnaProblemTypesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaProblemTypesInnerWithDefaults

`func NewGetCveRecordResponseContainersCnaProblemTypesInnerWithDefaults() *GetCveRecordResponseContainersCnaProblemTypesInner`

NewGetCveRecordResponseContainersCnaProblemTypesInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaProblemTypesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescriptions

`func (o *GetCveRecordResponseContainersCnaProblemTypesInner) GetDescriptions() []GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *GetCveRecordResponseContainersCnaProblemTypesInner) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *GetCveRecordResponseContainersCnaProblemTypesInner) SetDescriptions(v []GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.

### HasDescriptions

`func (o *GetCveRecordResponseContainersCnaProblemTypesInner) HasDescriptions() bool`

HasDescriptions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


