# GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** |  | [optional] 
**Lang** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner

`func NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner() *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner`

NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner instantiates a new GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInnerWithDefaults

`func NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInnerWithDefaults() *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner`

NewGetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetLang

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetLang() string`

GetLang returns the Lang field if non-nil, zero value otherwise.

### GetLangOk

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetLangOk() (*string, bool)`

GetLangOk returns a tuple with the Lang field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLang

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) SetLang(v string)`

SetLang sets Lang field to given value.

### HasLang

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) HasLang() bool`

HasLang returns a boolean if a field has been set.

### GetType

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *GetCveRecordResponseContainersCnaProblemTypesInnerDescriptionsInner) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


