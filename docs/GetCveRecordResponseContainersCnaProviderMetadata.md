# GetCveRecordResponseContainersCnaProviderMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrgId** | Pointer to **string** |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaProviderMetadata

`func NewGetCveRecordResponseContainersCnaProviderMetadata() *GetCveRecordResponseContainersCnaProviderMetadata`

NewGetCveRecordResponseContainersCnaProviderMetadata instantiates a new GetCveRecordResponseContainersCnaProviderMetadata object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaProviderMetadataWithDefaults

`func NewGetCveRecordResponseContainersCnaProviderMetadataWithDefaults() *GetCveRecordResponseContainersCnaProviderMetadata`

NewGetCveRecordResponseContainersCnaProviderMetadataWithDefaults instantiates a new GetCveRecordResponseContainersCnaProviderMetadata object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrgId

`func (o *GetCveRecordResponseContainersCnaProviderMetadata) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *GetCveRecordResponseContainersCnaProviderMetadata) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *GetCveRecordResponseContainersCnaProviderMetadata) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.

### HasOrgId

`func (o *GetCveRecordResponseContainersCnaProviderMetadata) HasOrgId() bool`

HasOrgId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


