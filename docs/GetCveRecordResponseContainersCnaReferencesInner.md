# GetCveRecordResponseContainersCnaReferencesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 

## Methods

### NewGetCveRecordResponseContainersCnaReferencesInner

`func NewGetCveRecordResponseContainersCnaReferencesInner() *GetCveRecordResponseContainersCnaReferencesInner`

NewGetCveRecordResponseContainersCnaReferencesInner instantiates a new GetCveRecordResponseContainersCnaReferencesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseContainersCnaReferencesInnerWithDefaults

`func NewGetCveRecordResponseContainersCnaReferencesInnerWithDefaults() *GetCveRecordResponseContainersCnaReferencesInner`

NewGetCveRecordResponseContainersCnaReferencesInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaReferencesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GetCveRecordResponseContainersCnaReferencesInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *GetCveRecordResponseContainersCnaReferencesInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetTags

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *GetCveRecordResponseContainersCnaReferencesInner) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *GetCveRecordResponseContainersCnaReferencesInner) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetUrl

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *GetCveRecordResponseContainersCnaReferencesInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *GetCveRecordResponseContainersCnaReferencesInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *GetCveRecordResponseContainersCnaReferencesInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


