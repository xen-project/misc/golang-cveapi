# GetCveRecordResponseCveMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssignerOrgId** | Pointer to **string** |  | [optional] 
**CveId** | Pointer to **string** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 
**AssignerShortName** | Pointer to **string** |  | [optional] 
**RequesterUserId** | Pointer to **string** |  | [optional] 
**DateReserved** | Pointer to **string** |  | [optional] 
**DatePublished** | Pointer to **string** |  | [optional] 

## Methods

### NewGetCveRecordResponseCveMetadata

`func NewGetCveRecordResponseCveMetadata() *GetCveRecordResponseCveMetadata`

NewGetCveRecordResponseCveMetadata instantiates a new GetCveRecordResponseCveMetadata object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCveRecordResponseCveMetadataWithDefaults

`func NewGetCveRecordResponseCveMetadataWithDefaults() *GetCveRecordResponseCveMetadata`

NewGetCveRecordResponseCveMetadataWithDefaults instantiates a new GetCveRecordResponseCveMetadata object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAssignerOrgId

`func (o *GetCveRecordResponseCveMetadata) GetAssignerOrgId() string`

GetAssignerOrgId returns the AssignerOrgId field if non-nil, zero value otherwise.

### GetAssignerOrgIdOk

`func (o *GetCveRecordResponseCveMetadata) GetAssignerOrgIdOk() (*string, bool)`

GetAssignerOrgIdOk returns a tuple with the AssignerOrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssignerOrgId

`func (o *GetCveRecordResponseCveMetadata) SetAssignerOrgId(v string)`

SetAssignerOrgId sets AssignerOrgId field to given value.

### HasAssignerOrgId

`func (o *GetCveRecordResponseCveMetadata) HasAssignerOrgId() bool`

HasAssignerOrgId returns a boolean if a field has been set.

### GetCveId

`func (o *GetCveRecordResponseCveMetadata) GetCveId() string`

GetCveId returns the CveId field if non-nil, zero value otherwise.

### GetCveIdOk

`func (o *GetCveRecordResponseCveMetadata) GetCveIdOk() (*string, bool)`

GetCveIdOk returns a tuple with the CveId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveId

`func (o *GetCveRecordResponseCveMetadata) SetCveId(v string)`

SetCveId sets CveId field to given value.

### HasCveId

`func (o *GetCveRecordResponseCveMetadata) HasCveId() bool`

HasCveId returns a boolean if a field has been set.

### GetState

`func (o *GetCveRecordResponseCveMetadata) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *GetCveRecordResponseCveMetadata) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *GetCveRecordResponseCveMetadata) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *GetCveRecordResponseCveMetadata) HasState() bool`

HasState returns a boolean if a field has been set.

### GetAssignerShortName

`func (o *GetCveRecordResponseCveMetadata) GetAssignerShortName() string`

GetAssignerShortName returns the AssignerShortName field if non-nil, zero value otherwise.

### GetAssignerShortNameOk

`func (o *GetCveRecordResponseCveMetadata) GetAssignerShortNameOk() (*string, bool)`

GetAssignerShortNameOk returns a tuple with the AssignerShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssignerShortName

`func (o *GetCveRecordResponseCveMetadata) SetAssignerShortName(v string)`

SetAssignerShortName sets AssignerShortName field to given value.

### HasAssignerShortName

`func (o *GetCveRecordResponseCveMetadata) HasAssignerShortName() bool`

HasAssignerShortName returns a boolean if a field has been set.

### GetRequesterUserId

`func (o *GetCveRecordResponseCveMetadata) GetRequesterUserId() string`

GetRequesterUserId returns the RequesterUserId field if non-nil, zero value otherwise.

### GetRequesterUserIdOk

`func (o *GetCveRecordResponseCveMetadata) GetRequesterUserIdOk() (*string, bool)`

GetRequesterUserIdOk returns a tuple with the RequesterUserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequesterUserId

`func (o *GetCveRecordResponseCveMetadata) SetRequesterUserId(v string)`

SetRequesterUserId sets RequesterUserId field to given value.

### HasRequesterUserId

`func (o *GetCveRecordResponseCveMetadata) HasRequesterUserId() bool`

HasRequesterUserId returns a boolean if a field has been set.

### GetDateReserved

`func (o *GetCveRecordResponseCveMetadata) GetDateReserved() string`

GetDateReserved returns the DateReserved field if non-nil, zero value otherwise.

### GetDateReservedOk

`func (o *GetCveRecordResponseCveMetadata) GetDateReservedOk() (*string, bool)`

GetDateReservedOk returns a tuple with the DateReserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateReserved

`func (o *GetCveRecordResponseCveMetadata) SetDateReserved(v string)`

SetDateReserved sets DateReserved field to given value.

### HasDateReserved

`func (o *GetCveRecordResponseCveMetadata) HasDateReserved() bool`

HasDateReserved returns a boolean if a field has been set.

### GetDatePublished

`func (o *GetCveRecordResponseCveMetadata) GetDatePublished() string`

GetDatePublished returns the DatePublished field if non-nil, zero value otherwise.

### GetDatePublishedOk

`func (o *GetCveRecordResponseCveMetadata) GetDatePublishedOk() (*string, bool)`

GetDatePublishedOk returns a tuple with the DatePublished field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDatePublished

`func (o *GetCveRecordResponseCveMetadata) SetDatePublished(v string)`

SetDatePublished sets DatePublished field to given value.

### HasDatePublished

`func (o *GetCveRecordResponseCveMetadata) HasDatePublished() bool`

HasDatePublished returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


