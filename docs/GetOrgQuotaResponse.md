# GetOrgQuotaResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IdQuota** | Pointer to **int32** | The number of CVE IDs the organization is allowed to have in the RESERVED state at one time. | [optional] 
**TotalReserved** | Pointer to **int32** | The total number of CVE IDs across all years that the organization has in the RESERVED state. | [optional] 
**Available** | Pointer to **int32** | The number of CVE IDs that can be reserved by the organization. (e.g., id_quota - total_reserved) | [optional] 

## Methods

### NewGetOrgQuotaResponse

`func NewGetOrgQuotaResponse() *GetOrgQuotaResponse`

NewGetOrgQuotaResponse instantiates a new GetOrgQuotaResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrgQuotaResponseWithDefaults

`func NewGetOrgQuotaResponseWithDefaults() *GetOrgQuotaResponse`

NewGetOrgQuotaResponseWithDefaults instantiates a new GetOrgQuotaResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIdQuota

`func (o *GetOrgQuotaResponse) GetIdQuota() int32`

GetIdQuota returns the IdQuota field if non-nil, zero value otherwise.

### GetIdQuotaOk

`func (o *GetOrgQuotaResponse) GetIdQuotaOk() (*int32, bool)`

GetIdQuotaOk returns a tuple with the IdQuota field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIdQuota

`func (o *GetOrgQuotaResponse) SetIdQuota(v int32)`

SetIdQuota sets IdQuota field to given value.

### HasIdQuota

`func (o *GetOrgQuotaResponse) HasIdQuota() bool`

HasIdQuota returns a boolean if a field has been set.

### GetTotalReserved

`func (o *GetOrgQuotaResponse) GetTotalReserved() int32`

GetTotalReserved returns the TotalReserved field if non-nil, zero value otherwise.

### GetTotalReservedOk

`func (o *GetOrgQuotaResponse) GetTotalReservedOk() (*int32, bool)`

GetTotalReservedOk returns a tuple with the TotalReserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalReserved

`func (o *GetOrgQuotaResponse) SetTotalReserved(v int32)`

SetTotalReserved sets TotalReserved field to given value.

### HasTotalReserved

`func (o *GetOrgQuotaResponse) HasTotalReserved() bool`

HasTotalReserved returns a boolean if a field has been set.

### GetAvailable

`func (o *GetOrgQuotaResponse) GetAvailable() int32`

GetAvailable returns the Available field if non-nil, zero value otherwise.

### GetAvailableOk

`func (o *GetOrgQuotaResponse) GetAvailableOk() (*int32, bool)`

GetAvailableOk returns a tuple with the Available field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvailable

`func (o *GetOrgQuotaResponse) SetAvailable(v int32)`

SetAvailable sets Available field to given value.

### HasAvailable

`func (o *GetOrgQuotaResponse) HasAvailable() bool`

HasAvailable returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


