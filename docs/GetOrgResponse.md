# GetOrgResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of the organization. | [optional] 
**ShortName** | Pointer to **string** | The short name of the organization. | [optional] 
**UUID** | Pointer to **string** | The identifier of the organization. | [optional] 
**Policies** | Pointer to [**CreateOrgResponseCreatedPolicies**](CreateOrgResponseCreatedPolicies.md) |  | [optional] 
**Authority** | Pointer to [**CreateOrgResponseCreatedAuthority**](CreateOrgResponseCreatedAuthority.md) |  | [optional] 
**Time** | Pointer to [**CreateOrgResponseCreatedTime**](CreateOrgResponseCreatedTime.md) |  | [optional] 

## Methods

### NewGetOrgResponse

`func NewGetOrgResponse() *GetOrgResponse`

NewGetOrgResponse instantiates a new GetOrgResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrgResponseWithDefaults

`func NewGetOrgResponseWithDefaults() *GetOrgResponse`

NewGetOrgResponseWithDefaults instantiates a new GetOrgResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *GetOrgResponse) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GetOrgResponse) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GetOrgResponse) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *GetOrgResponse) HasName() bool`

HasName returns a boolean if a field has been set.

### GetShortName

`func (o *GetOrgResponse) GetShortName() string`

GetShortName returns the ShortName field if non-nil, zero value otherwise.

### GetShortNameOk

`func (o *GetOrgResponse) GetShortNameOk() (*string, bool)`

GetShortNameOk returns a tuple with the ShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortName

`func (o *GetOrgResponse) SetShortName(v string)`

SetShortName sets ShortName field to given value.

### HasShortName

`func (o *GetOrgResponse) HasShortName() bool`

HasShortName returns a boolean if a field has been set.

### GetUUID

`func (o *GetOrgResponse) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *GetOrgResponse) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *GetOrgResponse) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *GetOrgResponse) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetPolicies

`func (o *GetOrgResponse) GetPolicies() CreateOrgResponseCreatedPolicies`

GetPolicies returns the Policies field if non-nil, zero value otherwise.

### GetPoliciesOk

`func (o *GetOrgResponse) GetPoliciesOk() (*CreateOrgResponseCreatedPolicies, bool)`

GetPoliciesOk returns a tuple with the Policies field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolicies

`func (o *GetOrgResponse) SetPolicies(v CreateOrgResponseCreatedPolicies)`

SetPolicies sets Policies field to given value.

### HasPolicies

`func (o *GetOrgResponse) HasPolicies() bool`

HasPolicies returns a boolean if a field has been set.

### GetAuthority

`func (o *GetOrgResponse) GetAuthority() CreateOrgResponseCreatedAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *GetOrgResponse) GetAuthorityOk() (*CreateOrgResponseCreatedAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *GetOrgResponse) SetAuthority(v CreateOrgResponseCreatedAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *GetOrgResponse) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *GetOrgResponse) GetTime() CreateOrgResponseCreatedTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *GetOrgResponse) GetTimeOk() (*CreateOrgResponseCreatedTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *GetOrgResponse) SetTime(v CreateOrgResponseCreatedTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *GetOrgResponse) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


