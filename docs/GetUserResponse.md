# GetUserResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | Pointer to **string** | The user name of the user | [optional] 
**OrgUUID** | Pointer to **string** | The identifier of the organization the user belongs to | [optional] 
**UUID** | Pointer to **string** | The identifier of the user | [optional] 
**Active** | Pointer to **string** | The user is an active user of the organization | [optional] 
**Name** | Pointer to [**ListUsersResponseUsersInnerName**](ListUsersResponseUsersInnerName.md) |  | [optional] 
**Authority** | Pointer to [**ListUsersResponseUsersInnerAuthority**](ListUsersResponseUsersInnerAuthority.md) |  | [optional] 
**Time** | Pointer to [**ListUsersResponseUsersInnerTime**](ListUsersResponseUsersInnerTime.md) |  | [optional] 

## Methods

### NewGetUserResponse

`func NewGetUserResponse() *GetUserResponse`

NewGetUserResponse instantiates a new GetUserResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetUserResponseWithDefaults

`func NewGetUserResponseWithDefaults() *GetUserResponse`

NewGetUserResponseWithDefaults instantiates a new GetUserResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *GetUserResponse) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *GetUserResponse) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *GetUserResponse) SetUsername(v string)`

SetUsername sets Username field to given value.

### HasUsername

`func (o *GetUserResponse) HasUsername() bool`

HasUsername returns a boolean if a field has been set.

### GetOrgUUID

`func (o *GetUserResponse) GetOrgUUID() string`

GetOrgUUID returns the OrgUUID field if non-nil, zero value otherwise.

### GetOrgUUIDOk

`func (o *GetUserResponse) GetOrgUUIDOk() (*string, bool)`

GetOrgUUIDOk returns a tuple with the OrgUUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgUUID

`func (o *GetUserResponse) SetOrgUUID(v string)`

SetOrgUUID sets OrgUUID field to given value.

### HasOrgUUID

`func (o *GetUserResponse) HasOrgUUID() bool`

HasOrgUUID returns a boolean if a field has been set.

### GetUUID

`func (o *GetUserResponse) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *GetUserResponse) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *GetUserResponse) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *GetUserResponse) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetActive

`func (o *GetUserResponse) GetActive() string`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *GetUserResponse) GetActiveOk() (*string, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *GetUserResponse) SetActive(v string)`

SetActive sets Active field to given value.

### HasActive

`func (o *GetUserResponse) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetName

`func (o *GetUserResponse) GetName() ListUsersResponseUsersInnerName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GetUserResponse) GetNameOk() (*ListUsersResponseUsersInnerName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GetUserResponse) SetName(v ListUsersResponseUsersInnerName)`

SetName sets Name field to given value.

### HasName

`func (o *GetUserResponse) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAuthority

`func (o *GetUserResponse) GetAuthority() ListUsersResponseUsersInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *GetUserResponse) GetAuthorityOk() (*ListUsersResponseUsersInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *GetUserResponse) SetAuthority(v ListUsersResponseUsersInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *GetUserResponse) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *GetUserResponse) GetTime() ListUsersResponseUsersInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *GetUserResponse) GetTimeOk() (*ListUsersResponseUsersInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *GetUserResponse) SetTime(v ListUsersResponseUsersInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *GetUserResponse) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


