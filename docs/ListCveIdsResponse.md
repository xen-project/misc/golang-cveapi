# ListCveIdsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCount** | Pointer to **int32** | Total CVE records found | [optional] 
**ItemsPerPage** | Pointer to **int32** | Number of CVE records in a page | [optional] 
**PageCount** | Pointer to **int32** | Total number of pages | [optional] 
**CurrentPage** | Pointer to **int32** | Current page | [optional] 
**PrevPage** | Pointer to **int32** | Previous page | [optional] 
**NextPage** | Pointer to **int32** | Next page | [optional] 
**CveIds** | Pointer to [**[]ListCveIdsResponseCveIdsInner**](ListCveIdsResponseCveIdsInner.md) |  | [optional] 

## Methods

### NewListCveIdsResponse

`func NewListCveIdsResponse() *ListCveIdsResponse`

NewListCveIdsResponse instantiates a new ListCveIdsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveIdsResponseWithDefaults

`func NewListCveIdsResponseWithDefaults() *ListCveIdsResponse`

NewListCveIdsResponseWithDefaults instantiates a new ListCveIdsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotalCount

`func (o *ListCveIdsResponse) GetTotalCount() int32`

GetTotalCount returns the TotalCount field if non-nil, zero value otherwise.

### GetTotalCountOk

`func (o *ListCveIdsResponse) GetTotalCountOk() (*int32, bool)`

GetTotalCountOk returns a tuple with the TotalCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalCount

`func (o *ListCveIdsResponse) SetTotalCount(v int32)`

SetTotalCount sets TotalCount field to given value.

### HasTotalCount

`func (o *ListCveIdsResponse) HasTotalCount() bool`

HasTotalCount returns a boolean if a field has been set.

### GetItemsPerPage

`func (o *ListCveIdsResponse) GetItemsPerPage() int32`

GetItemsPerPage returns the ItemsPerPage field if non-nil, zero value otherwise.

### GetItemsPerPageOk

`func (o *ListCveIdsResponse) GetItemsPerPageOk() (*int32, bool)`

GetItemsPerPageOk returns a tuple with the ItemsPerPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemsPerPage

`func (o *ListCveIdsResponse) SetItemsPerPage(v int32)`

SetItemsPerPage sets ItemsPerPage field to given value.

### HasItemsPerPage

`func (o *ListCveIdsResponse) HasItemsPerPage() bool`

HasItemsPerPage returns a boolean if a field has been set.

### GetPageCount

`func (o *ListCveIdsResponse) GetPageCount() int32`

GetPageCount returns the PageCount field if non-nil, zero value otherwise.

### GetPageCountOk

`func (o *ListCveIdsResponse) GetPageCountOk() (*int32, bool)`

GetPageCountOk returns a tuple with the PageCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageCount

`func (o *ListCveIdsResponse) SetPageCount(v int32)`

SetPageCount sets PageCount field to given value.

### HasPageCount

`func (o *ListCveIdsResponse) HasPageCount() bool`

HasPageCount returns a boolean if a field has been set.

### GetCurrentPage

`func (o *ListCveIdsResponse) GetCurrentPage() int32`

GetCurrentPage returns the CurrentPage field if non-nil, zero value otherwise.

### GetCurrentPageOk

`func (o *ListCveIdsResponse) GetCurrentPageOk() (*int32, bool)`

GetCurrentPageOk returns a tuple with the CurrentPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentPage

`func (o *ListCveIdsResponse) SetCurrentPage(v int32)`

SetCurrentPage sets CurrentPage field to given value.

### HasCurrentPage

`func (o *ListCveIdsResponse) HasCurrentPage() bool`

HasCurrentPage returns a boolean if a field has been set.

### GetPrevPage

`func (o *ListCveIdsResponse) GetPrevPage() int32`

GetPrevPage returns the PrevPage field if non-nil, zero value otherwise.

### GetPrevPageOk

`func (o *ListCveIdsResponse) GetPrevPageOk() (*int32, bool)`

GetPrevPageOk returns a tuple with the PrevPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevPage

`func (o *ListCveIdsResponse) SetPrevPage(v int32)`

SetPrevPage sets PrevPage field to given value.

### HasPrevPage

`func (o *ListCveIdsResponse) HasPrevPage() bool`

HasPrevPage returns a boolean if a field has been set.

### GetNextPage

`func (o *ListCveIdsResponse) GetNextPage() int32`

GetNextPage returns the NextPage field if non-nil, zero value otherwise.

### GetNextPageOk

`func (o *ListCveIdsResponse) GetNextPageOk() (*int32, bool)`

GetNextPageOk returns a tuple with the NextPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextPage

`func (o *ListCveIdsResponse) SetNextPage(v int32)`

SetNextPage sets NextPage field to given value.

### HasNextPage

`func (o *ListCveIdsResponse) HasNextPage() bool`

HasNextPage returns a boolean if a field has been set.

### GetCveIds

`func (o *ListCveIdsResponse) GetCveIds() []ListCveIdsResponseCveIdsInner`

GetCveIds returns the CveIds field if non-nil, zero value otherwise.

### GetCveIdsOk

`func (o *ListCveIdsResponse) GetCveIdsOk() (*[]ListCveIdsResponseCveIdsInner, bool)`

GetCveIdsOk returns a tuple with the CveIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveIds

`func (o *ListCveIdsResponse) SetCveIds(v []ListCveIdsResponseCveIdsInner)`

SetCveIds sets CveIds field to given value.

### HasCveIds

`func (o *ListCveIdsResponse) HasCveIds() bool`

HasCveIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


