# ListCveIdsResponseCveIdsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CveId** | Pointer to **string** | ^CVE-[0-9]{4}-[0-9]{4,}$ | [optional] 
**CveYear** | Pointer to **string** | ^[0-9]{4}$ | [optional] 
**OwningCna** | Pointer to **string** | The shortname for the organization that owns the CVE ID | [optional] 
**State** | Pointer to **string** |  | [optional] 
**RequestedBy** | Pointer to [**ListCveIdsResponseCveIdsInnerRequestedBy**](ListCveIdsResponseCveIdsInnerRequestedBy.md) |  | [optional] 
**Reserved** | Pointer to **time.Time** | The time the ID was reserved | [optional] 
**Time** | Pointer to [**ListCveIdsResponseCveIdsInnerTime**](ListCveIdsResponseCveIdsInnerTime.md) |  | [optional] 

## Methods

### NewListCveIdsResponseCveIdsInner

`func NewListCveIdsResponseCveIdsInner() *ListCveIdsResponseCveIdsInner`

NewListCveIdsResponseCveIdsInner instantiates a new ListCveIdsResponseCveIdsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveIdsResponseCveIdsInnerWithDefaults

`func NewListCveIdsResponseCveIdsInnerWithDefaults() *ListCveIdsResponseCveIdsInner`

NewListCveIdsResponseCveIdsInnerWithDefaults instantiates a new ListCveIdsResponseCveIdsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCveId

`func (o *ListCveIdsResponseCveIdsInner) GetCveId() string`

GetCveId returns the CveId field if non-nil, zero value otherwise.

### GetCveIdOk

`func (o *ListCveIdsResponseCveIdsInner) GetCveIdOk() (*string, bool)`

GetCveIdOk returns a tuple with the CveId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveId

`func (o *ListCveIdsResponseCveIdsInner) SetCveId(v string)`

SetCveId sets CveId field to given value.

### HasCveId

`func (o *ListCveIdsResponseCveIdsInner) HasCveId() bool`

HasCveId returns a boolean if a field has been set.

### GetCveYear

`func (o *ListCveIdsResponseCveIdsInner) GetCveYear() string`

GetCveYear returns the CveYear field if non-nil, zero value otherwise.

### GetCveYearOk

`func (o *ListCveIdsResponseCveIdsInner) GetCveYearOk() (*string, bool)`

GetCveYearOk returns a tuple with the CveYear field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveYear

`func (o *ListCveIdsResponseCveIdsInner) SetCveYear(v string)`

SetCveYear sets CveYear field to given value.

### HasCveYear

`func (o *ListCveIdsResponseCveIdsInner) HasCveYear() bool`

HasCveYear returns a boolean if a field has been set.

### GetOwningCna

`func (o *ListCveIdsResponseCveIdsInner) GetOwningCna() string`

GetOwningCna returns the OwningCna field if non-nil, zero value otherwise.

### GetOwningCnaOk

`func (o *ListCveIdsResponseCveIdsInner) GetOwningCnaOk() (*string, bool)`

GetOwningCnaOk returns a tuple with the OwningCna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwningCna

`func (o *ListCveIdsResponseCveIdsInner) SetOwningCna(v string)`

SetOwningCna sets OwningCna field to given value.

### HasOwningCna

`func (o *ListCveIdsResponseCveIdsInner) HasOwningCna() bool`

HasOwningCna returns a boolean if a field has been set.

### GetState

`func (o *ListCveIdsResponseCveIdsInner) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *ListCveIdsResponseCveIdsInner) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *ListCveIdsResponseCveIdsInner) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *ListCveIdsResponseCveIdsInner) HasState() bool`

HasState returns a boolean if a field has been set.

### GetRequestedBy

`func (o *ListCveIdsResponseCveIdsInner) GetRequestedBy() ListCveIdsResponseCveIdsInnerRequestedBy`

GetRequestedBy returns the RequestedBy field if non-nil, zero value otherwise.

### GetRequestedByOk

`func (o *ListCveIdsResponseCveIdsInner) GetRequestedByOk() (*ListCveIdsResponseCveIdsInnerRequestedBy, bool)`

GetRequestedByOk returns a tuple with the RequestedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestedBy

`func (o *ListCveIdsResponseCveIdsInner) SetRequestedBy(v ListCveIdsResponseCveIdsInnerRequestedBy)`

SetRequestedBy sets RequestedBy field to given value.

### HasRequestedBy

`func (o *ListCveIdsResponseCveIdsInner) HasRequestedBy() bool`

HasRequestedBy returns a boolean if a field has been set.

### GetReserved

`func (o *ListCveIdsResponseCveIdsInner) GetReserved() time.Time`

GetReserved returns the Reserved field if non-nil, zero value otherwise.

### GetReservedOk

`func (o *ListCveIdsResponseCveIdsInner) GetReservedOk() (*time.Time, bool)`

GetReservedOk returns a tuple with the Reserved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReserved

`func (o *ListCveIdsResponseCveIdsInner) SetReserved(v time.Time)`

SetReserved sets Reserved field to given value.

### HasReserved

`func (o *ListCveIdsResponseCveIdsInner) HasReserved() bool`

HasReserved returns a boolean if a field has been set.

### GetTime

`func (o *ListCveIdsResponseCveIdsInner) GetTime() ListCveIdsResponseCveIdsInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *ListCveIdsResponseCveIdsInner) GetTimeOk() (*ListCveIdsResponseCveIdsInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *ListCveIdsResponseCveIdsInner) SetTime(v ListCveIdsResponseCveIdsInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *ListCveIdsResponseCveIdsInner) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


