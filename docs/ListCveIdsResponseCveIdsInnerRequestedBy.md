# ListCveIdsResponseCveIdsInnerRequestedBy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cna** | Pointer to **string** | The shortname for the organization of the user that requested the ID | [optional] 
**User** | Pointer to **string** | The username for the account that requested the ID | [optional] 

## Methods

### NewListCveIdsResponseCveIdsInnerRequestedBy

`func NewListCveIdsResponseCveIdsInnerRequestedBy() *ListCveIdsResponseCveIdsInnerRequestedBy`

NewListCveIdsResponseCveIdsInnerRequestedBy instantiates a new ListCveIdsResponseCveIdsInnerRequestedBy object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveIdsResponseCveIdsInnerRequestedByWithDefaults

`func NewListCveIdsResponseCveIdsInnerRequestedByWithDefaults() *ListCveIdsResponseCveIdsInnerRequestedBy`

NewListCveIdsResponseCveIdsInnerRequestedByWithDefaults instantiates a new ListCveIdsResponseCveIdsInnerRequestedBy object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCna

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) GetCna() string`

GetCna returns the Cna field if non-nil, zero value otherwise.

### GetCnaOk

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) GetCnaOk() (*string, bool)`

GetCnaOk returns a tuple with the Cna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCna

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) SetCna(v string)`

SetCna sets Cna field to given value.

### HasCna

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) HasCna() bool`

HasCna returns a boolean if a field has been set.

### GetUser

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) GetUser() string`

GetUser returns the User field if non-nil, zero value otherwise.

### GetUserOk

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) GetUserOk() (*string, bool)`

GetUserOk returns a tuple with the User field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUser

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) SetUser(v string)`

SetUser sets User field to given value.

### HasUser

`func (o *ListCveIdsResponseCveIdsInnerRequestedBy) HasUser() bool`

HasUser returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


