# ListCveIdsResponseCveIdsInnerTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | Pointer to **time.Time** | The time the ID was created | [optional] 
**Modified** | Pointer to **time.Time** | The last time the ID was modified | [optional] 

## Methods

### NewListCveIdsResponseCveIdsInnerTime

`func NewListCveIdsResponseCveIdsInnerTime() *ListCveIdsResponseCveIdsInnerTime`

NewListCveIdsResponseCveIdsInnerTime instantiates a new ListCveIdsResponseCveIdsInnerTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveIdsResponseCveIdsInnerTimeWithDefaults

`func NewListCveIdsResponseCveIdsInnerTimeWithDefaults() *ListCveIdsResponseCveIdsInnerTime`

NewListCveIdsResponseCveIdsInnerTimeWithDefaults instantiates a new ListCveIdsResponseCveIdsInnerTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCreated

`func (o *ListCveIdsResponseCveIdsInnerTime) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *ListCveIdsResponseCveIdsInnerTime) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *ListCveIdsResponseCveIdsInnerTime) SetCreated(v time.Time)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *ListCveIdsResponseCveIdsInnerTime) HasCreated() bool`

HasCreated returns a boolean if a field has been set.

### GetModified

`func (o *ListCveIdsResponseCveIdsInnerTime) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *ListCveIdsResponseCveIdsInnerTime) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *ListCveIdsResponseCveIdsInnerTime) SetModified(v time.Time)`

SetModified sets Modified field to given value.

### HasModified

`func (o *ListCveIdsResponseCveIdsInnerTime) HasModified() bool`

HasModified returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


