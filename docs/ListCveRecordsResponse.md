# ListCveRecordsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCount** | Pointer to **int32** |  | [optional] 
**ItemsPerPage** | Pointer to **int32** |  | [optional] 
**PageCount** | Pointer to **int32** |  | [optional] 
**CurrentPage** | Pointer to **int32** |  | [optional] 
**PrevPage** | Pointer to **int32** |  | [optional] 
**NextPage** | Pointer to **int32** |  | [optional] 
**CveRecords** | Pointer to [**[]ListCveRecordsResponseCveRecordsInner**](ListCveRecordsResponseCveRecordsInner.md) |  | [optional] 

## Methods

### NewListCveRecordsResponse

`func NewListCveRecordsResponse() *ListCveRecordsResponse`

NewListCveRecordsResponse instantiates a new ListCveRecordsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveRecordsResponseWithDefaults

`func NewListCveRecordsResponseWithDefaults() *ListCveRecordsResponse`

NewListCveRecordsResponseWithDefaults instantiates a new ListCveRecordsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotalCount

`func (o *ListCveRecordsResponse) GetTotalCount() int32`

GetTotalCount returns the TotalCount field if non-nil, zero value otherwise.

### GetTotalCountOk

`func (o *ListCveRecordsResponse) GetTotalCountOk() (*int32, bool)`

GetTotalCountOk returns a tuple with the TotalCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalCount

`func (o *ListCveRecordsResponse) SetTotalCount(v int32)`

SetTotalCount sets TotalCount field to given value.

### HasTotalCount

`func (o *ListCveRecordsResponse) HasTotalCount() bool`

HasTotalCount returns a boolean if a field has been set.

### GetItemsPerPage

`func (o *ListCveRecordsResponse) GetItemsPerPage() int32`

GetItemsPerPage returns the ItemsPerPage field if non-nil, zero value otherwise.

### GetItemsPerPageOk

`func (o *ListCveRecordsResponse) GetItemsPerPageOk() (*int32, bool)`

GetItemsPerPageOk returns a tuple with the ItemsPerPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemsPerPage

`func (o *ListCveRecordsResponse) SetItemsPerPage(v int32)`

SetItemsPerPage sets ItemsPerPage field to given value.

### HasItemsPerPage

`func (o *ListCveRecordsResponse) HasItemsPerPage() bool`

HasItemsPerPage returns a boolean if a field has been set.

### GetPageCount

`func (o *ListCveRecordsResponse) GetPageCount() int32`

GetPageCount returns the PageCount field if non-nil, zero value otherwise.

### GetPageCountOk

`func (o *ListCveRecordsResponse) GetPageCountOk() (*int32, bool)`

GetPageCountOk returns a tuple with the PageCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageCount

`func (o *ListCveRecordsResponse) SetPageCount(v int32)`

SetPageCount sets PageCount field to given value.

### HasPageCount

`func (o *ListCveRecordsResponse) HasPageCount() bool`

HasPageCount returns a boolean if a field has been set.

### GetCurrentPage

`func (o *ListCveRecordsResponse) GetCurrentPage() int32`

GetCurrentPage returns the CurrentPage field if non-nil, zero value otherwise.

### GetCurrentPageOk

`func (o *ListCveRecordsResponse) GetCurrentPageOk() (*int32, bool)`

GetCurrentPageOk returns a tuple with the CurrentPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentPage

`func (o *ListCveRecordsResponse) SetCurrentPage(v int32)`

SetCurrentPage sets CurrentPage field to given value.

### HasCurrentPage

`func (o *ListCveRecordsResponse) HasCurrentPage() bool`

HasCurrentPage returns a boolean if a field has been set.

### GetPrevPage

`func (o *ListCveRecordsResponse) GetPrevPage() int32`

GetPrevPage returns the PrevPage field if non-nil, zero value otherwise.

### GetPrevPageOk

`func (o *ListCveRecordsResponse) GetPrevPageOk() (*int32, bool)`

GetPrevPageOk returns a tuple with the PrevPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevPage

`func (o *ListCveRecordsResponse) SetPrevPage(v int32)`

SetPrevPage sets PrevPage field to given value.

### HasPrevPage

`func (o *ListCveRecordsResponse) HasPrevPage() bool`

HasPrevPage returns a boolean if a field has been set.

### GetNextPage

`func (o *ListCveRecordsResponse) GetNextPage() int32`

GetNextPage returns the NextPage field if non-nil, zero value otherwise.

### GetNextPageOk

`func (o *ListCveRecordsResponse) GetNextPageOk() (*int32, bool)`

GetNextPageOk returns a tuple with the NextPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextPage

`func (o *ListCveRecordsResponse) SetNextPage(v int32)`

SetNextPage sets NextPage field to given value.

### HasNextPage

`func (o *ListCveRecordsResponse) HasNextPage() bool`

HasNextPage returns a boolean if a field has been set.

### GetCveRecords

`func (o *ListCveRecordsResponse) GetCveRecords() []ListCveRecordsResponseCveRecordsInner`

GetCveRecords returns the CveRecords field if non-nil, zero value otherwise.

### GetCveRecordsOk

`func (o *ListCveRecordsResponse) GetCveRecordsOk() (*[]ListCveRecordsResponseCveRecordsInner, bool)`

GetCveRecordsOk returns a tuple with the CveRecords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveRecords

`func (o *ListCveRecordsResponse) SetCveRecords(v []ListCveRecordsResponseCveRecordsInner)`

SetCveRecords sets CveRecords field to given value.

### HasCveRecords

`func (o *ListCveRecordsResponse) HasCveRecords() bool`

HasCveRecords returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


