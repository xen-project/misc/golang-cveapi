# ListCveRecordsResponseCveRecordsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Containers** | Pointer to [**GetCveRecordResponseContainers**](GetCveRecordResponseContainers.md) |  | [optional] 
**CveMetadata** | Pointer to [**GetCveRecordResponseCveMetadata**](GetCveRecordResponseCveMetadata.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DataVersion** | Pointer to **int32** |  | [optional] 

## Methods

### NewListCveRecordsResponseCveRecordsInner

`func NewListCveRecordsResponseCveRecordsInner() *ListCveRecordsResponseCveRecordsInner`

NewListCveRecordsResponseCveRecordsInner instantiates a new ListCveRecordsResponseCveRecordsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListCveRecordsResponseCveRecordsInnerWithDefaults

`func NewListCveRecordsResponseCveRecordsInnerWithDefaults() *ListCveRecordsResponseCveRecordsInner`

NewListCveRecordsResponseCveRecordsInnerWithDefaults instantiates a new ListCveRecordsResponseCveRecordsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContainers

`func (o *ListCveRecordsResponseCveRecordsInner) GetContainers() GetCveRecordResponseContainers`

GetContainers returns the Containers field if non-nil, zero value otherwise.

### GetContainersOk

`func (o *ListCveRecordsResponseCveRecordsInner) GetContainersOk() (*GetCveRecordResponseContainers, bool)`

GetContainersOk returns a tuple with the Containers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContainers

`func (o *ListCveRecordsResponseCveRecordsInner) SetContainers(v GetCveRecordResponseContainers)`

SetContainers sets Containers field to given value.

### HasContainers

`func (o *ListCveRecordsResponseCveRecordsInner) HasContainers() bool`

HasContainers returns a boolean if a field has been set.

### GetCveMetadata

`func (o *ListCveRecordsResponseCveRecordsInner) GetCveMetadata() GetCveRecordResponseCveMetadata`

GetCveMetadata returns the CveMetadata field if non-nil, zero value otherwise.

### GetCveMetadataOk

`func (o *ListCveRecordsResponseCveRecordsInner) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool)`

GetCveMetadataOk returns a tuple with the CveMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCveMetadata

`func (o *ListCveRecordsResponseCveRecordsInner) SetCveMetadata(v GetCveRecordResponseCveMetadata)`

SetCveMetadata sets CveMetadata field to given value.

### HasCveMetadata

`func (o *ListCveRecordsResponseCveRecordsInner) HasCveMetadata() bool`

HasCveMetadata returns a boolean if a field has been set.

### GetDataType

`func (o *ListCveRecordsResponseCveRecordsInner) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *ListCveRecordsResponseCveRecordsInner) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *ListCveRecordsResponseCveRecordsInner) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *ListCveRecordsResponseCveRecordsInner) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDataVersion

`func (o *ListCveRecordsResponseCveRecordsInner) GetDataVersion() int32`

GetDataVersion returns the DataVersion field if non-nil, zero value otherwise.

### GetDataVersionOk

`func (o *ListCveRecordsResponseCveRecordsInner) GetDataVersionOk() (*int32, bool)`

GetDataVersionOk returns a tuple with the DataVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataVersion

`func (o *ListCveRecordsResponseCveRecordsInner) SetDataVersion(v int32)`

SetDataVersion sets DataVersion field to given value.

### HasDataVersion

`func (o *ListCveRecordsResponseCveRecordsInner) HasDataVersion() bool`

HasDataVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


