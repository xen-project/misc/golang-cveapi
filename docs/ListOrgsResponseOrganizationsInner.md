# ListOrgsResponseOrganizationsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of the organization | [optional] 
**ShortName** | Pointer to **string** | The short name of the organization | [optional] 
**UUID** | Pointer to **string** | The identifier of the organization | [optional] 
**Policies** | Pointer to [**ListOrgsResponseOrganizationsInnerPolicies**](ListOrgsResponseOrganizationsInnerPolicies.md) |  | [optional] 
**Authority** | Pointer to [**ListOrgsResponseOrganizationsInnerAuthority**](ListOrgsResponseOrganizationsInnerAuthority.md) |  | [optional] 
**Time** | Pointer to [**ListOrgsResponseOrganizationsInnerTime**](ListOrgsResponseOrganizationsInnerTime.md) |  | [optional] 

## Methods

### NewListOrgsResponseOrganizationsInner

`func NewListOrgsResponseOrganizationsInner() *ListOrgsResponseOrganizationsInner`

NewListOrgsResponseOrganizationsInner instantiates a new ListOrgsResponseOrganizationsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOrgsResponseOrganizationsInnerWithDefaults

`func NewListOrgsResponseOrganizationsInnerWithDefaults() *ListOrgsResponseOrganizationsInner`

NewListOrgsResponseOrganizationsInnerWithDefaults instantiates a new ListOrgsResponseOrganizationsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ListOrgsResponseOrganizationsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ListOrgsResponseOrganizationsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ListOrgsResponseOrganizationsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ListOrgsResponseOrganizationsInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetShortName

`func (o *ListOrgsResponseOrganizationsInner) GetShortName() string`

GetShortName returns the ShortName field if non-nil, zero value otherwise.

### GetShortNameOk

`func (o *ListOrgsResponseOrganizationsInner) GetShortNameOk() (*string, bool)`

GetShortNameOk returns a tuple with the ShortName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortName

`func (o *ListOrgsResponseOrganizationsInner) SetShortName(v string)`

SetShortName sets ShortName field to given value.

### HasShortName

`func (o *ListOrgsResponseOrganizationsInner) HasShortName() bool`

HasShortName returns a boolean if a field has been set.

### GetUUID

`func (o *ListOrgsResponseOrganizationsInner) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *ListOrgsResponseOrganizationsInner) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *ListOrgsResponseOrganizationsInner) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *ListOrgsResponseOrganizationsInner) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetPolicies

`func (o *ListOrgsResponseOrganizationsInner) GetPolicies() ListOrgsResponseOrganizationsInnerPolicies`

GetPolicies returns the Policies field if non-nil, zero value otherwise.

### GetPoliciesOk

`func (o *ListOrgsResponseOrganizationsInner) GetPoliciesOk() (*ListOrgsResponseOrganizationsInnerPolicies, bool)`

GetPoliciesOk returns a tuple with the Policies field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolicies

`func (o *ListOrgsResponseOrganizationsInner) SetPolicies(v ListOrgsResponseOrganizationsInnerPolicies)`

SetPolicies sets Policies field to given value.

### HasPolicies

`func (o *ListOrgsResponseOrganizationsInner) HasPolicies() bool`

HasPolicies returns a boolean if a field has been set.

### GetAuthority

`func (o *ListOrgsResponseOrganizationsInner) GetAuthority() ListOrgsResponseOrganizationsInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *ListOrgsResponseOrganizationsInner) GetAuthorityOk() (*ListOrgsResponseOrganizationsInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *ListOrgsResponseOrganizationsInner) SetAuthority(v ListOrgsResponseOrganizationsInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *ListOrgsResponseOrganizationsInner) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *ListOrgsResponseOrganizationsInner) GetTime() ListOrgsResponseOrganizationsInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *ListOrgsResponseOrganizationsInner) GetTimeOk() (*ListOrgsResponseOrganizationsInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *ListOrgsResponseOrganizationsInner) SetTime(v ListOrgsResponseOrganizationsInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *ListOrgsResponseOrganizationsInner) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


