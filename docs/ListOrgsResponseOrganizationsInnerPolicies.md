# ListOrgsResponseOrganizationsInnerPolicies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IdQuota** | Pointer to **int32** | The number of CVE IDs the organization is allowed to have in the RESERVED state at one time | [optional] 

## Methods

### NewListOrgsResponseOrganizationsInnerPolicies

`func NewListOrgsResponseOrganizationsInnerPolicies() *ListOrgsResponseOrganizationsInnerPolicies`

NewListOrgsResponseOrganizationsInnerPolicies instantiates a new ListOrgsResponseOrganizationsInnerPolicies object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOrgsResponseOrganizationsInnerPoliciesWithDefaults

`func NewListOrgsResponseOrganizationsInnerPoliciesWithDefaults() *ListOrgsResponseOrganizationsInnerPolicies`

NewListOrgsResponseOrganizationsInnerPoliciesWithDefaults instantiates a new ListOrgsResponseOrganizationsInnerPolicies object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIdQuota

`func (o *ListOrgsResponseOrganizationsInnerPolicies) GetIdQuota() int32`

GetIdQuota returns the IdQuota field if non-nil, zero value otherwise.

### GetIdQuotaOk

`func (o *ListOrgsResponseOrganizationsInnerPolicies) GetIdQuotaOk() (*int32, bool)`

GetIdQuotaOk returns a tuple with the IdQuota field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIdQuota

`func (o *ListOrgsResponseOrganizationsInnerPolicies) SetIdQuota(v int32)`

SetIdQuota sets IdQuota field to given value.

### HasIdQuota

`func (o *ListOrgsResponseOrganizationsInnerPolicies) HasIdQuota() bool`

HasIdQuota returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


