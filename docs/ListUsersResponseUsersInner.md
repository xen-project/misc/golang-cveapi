# ListUsersResponseUsersInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | Pointer to **string** | The user name of the user | [optional] 
**OrgUUID** | Pointer to **string** | The identifier of the organization the user belongs to | [optional] 
**UUID** | Pointer to **string** | The identifier of the user | [optional] 
**Active** | Pointer to **string** | The user is an active user of the organization | [optional] 
**Name** | Pointer to [**ListUsersResponseUsersInnerName**](ListUsersResponseUsersInnerName.md) |  | [optional] 
**Authority** | Pointer to [**ListUsersResponseUsersInnerAuthority**](ListUsersResponseUsersInnerAuthority.md) |  | [optional] 
**Time** | Pointer to [**ListUsersResponseUsersInnerTime**](ListUsersResponseUsersInnerTime.md) |  | [optional] 

## Methods

### NewListUsersResponseUsersInner

`func NewListUsersResponseUsersInner() *ListUsersResponseUsersInner`

NewListUsersResponseUsersInner instantiates a new ListUsersResponseUsersInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListUsersResponseUsersInnerWithDefaults

`func NewListUsersResponseUsersInnerWithDefaults() *ListUsersResponseUsersInner`

NewListUsersResponseUsersInnerWithDefaults instantiates a new ListUsersResponseUsersInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *ListUsersResponseUsersInner) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *ListUsersResponseUsersInner) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *ListUsersResponseUsersInner) SetUsername(v string)`

SetUsername sets Username field to given value.

### HasUsername

`func (o *ListUsersResponseUsersInner) HasUsername() bool`

HasUsername returns a boolean if a field has been set.

### GetOrgUUID

`func (o *ListUsersResponseUsersInner) GetOrgUUID() string`

GetOrgUUID returns the OrgUUID field if non-nil, zero value otherwise.

### GetOrgUUIDOk

`func (o *ListUsersResponseUsersInner) GetOrgUUIDOk() (*string, bool)`

GetOrgUUIDOk returns a tuple with the OrgUUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgUUID

`func (o *ListUsersResponseUsersInner) SetOrgUUID(v string)`

SetOrgUUID sets OrgUUID field to given value.

### HasOrgUUID

`func (o *ListUsersResponseUsersInner) HasOrgUUID() bool`

HasOrgUUID returns a boolean if a field has been set.

### GetUUID

`func (o *ListUsersResponseUsersInner) GetUUID() string`

GetUUID returns the UUID field if non-nil, zero value otherwise.

### GetUUIDOk

`func (o *ListUsersResponseUsersInner) GetUUIDOk() (*string, bool)`

GetUUIDOk returns a tuple with the UUID field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUUID

`func (o *ListUsersResponseUsersInner) SetUUID(v string)`

SetUUID sets UUID field to given value.

### HasUUID

`func (o *ListUsersResponseUsersInner) HasUUID() bool`

HasUUID returns a boolean if a field has been set.

### GetActive

`func (o *ListUsersResponseUsersInner) GetActive() string`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *ListUsersResponseUsersInner) GetActiveOk() (*string, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *ListUsersResponseUsersInner) SetActive(v string)`

SetActive sets Active field to given value.

### HasActive

`func (o *ListUsersResponseUsersInner) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetName

`func (o *ListUsersResponseUsersInner) GetName() ListUsersResponseUsersInnerName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ListUsersResponseUsersInner) GetNameOk() (*ListUsersResponseUsersInnerName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ListUsersResponseUsersInner) SetName(v ListUsersResponseUsersInnerName)`

SetName sets Name field to given value.

### HasName

`func (o *ListUsersResponseUsersInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAuthority

`func (o *ListUsersResponseUsersInner) GetAuthority() ListUsersResponseUsersInnerAuthority`

GetAuthority returns the Authority field if non-nil, zero value otherwise.

### GetAuthorityOk

`func (o *ListUsersResponseUsersInner) GetAuthorityOk() (*ListUsersResponseUsersInnerAuthority, bool)`

GetAuthorityOk returns a tuple with the Authority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthority

`func (o *ListUsersResponseUsersInner) SetAuthority(v ListUsersResponseUsersInnerAuthority)`

SetAuthority sets Authority field to given value.

### HasAuthority

`func (o *ListUsersResponseUsersInner) HasAuthority() bool`

HasAuthority returns a boolean if a field has been set.

### GetTime

`func (o *ListUsersResponseUsersInner) GetTime() ListUsersResponseUsersInnerTime`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *ListUsersResponseUsersInner) GetTimeOk() (*ListUsersResponseUsersInnerTime, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *ListUsersResponseUsersInner) SetTime(v ListUsersResponseUsersInnerTime)`

SetTime sets Time field to given value.

### HasTime

`func (o *ListUsersResponseUsersInner) HasTime() bool`

HasTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


