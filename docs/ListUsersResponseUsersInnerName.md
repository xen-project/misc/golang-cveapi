# ListUsersResponseUsersInnerName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**First** | Pointer to **string** | The first name of the user | [optional] 
**Last** | Pointer to **string** | The last name of the user | [optional] 
**Middle** | Pointer to **string** | The middle name of the user, if any | [optional] 
**Suffix** | Pointer to **string** | The suffix of the user, if any | [optional] 

## Methods

### NewListUsersResponseUsersInnerName

`func NewListUsersResponseUsersInnerName() *ListUsersResponseUsersInnerName`

NewListUsersResponseUsersInnerName instantiates a new ListUsersResponseUsersInnerName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListUsersResponseUsersInnerNameWithDefaults

`func NewListUsersResponseUsersInnerNameWithDefaults() *ListUsersResponseUsersInnerName`

NewListUsersResponseUsersInnerNameWithDefaults instantiates a new ListUsersResponseUsersInnerName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFirst

`func (o *ListUsersResponseUsersInnerName) GetFirst() string`

GetFirst returns the First field if non-nil, zero value otherwise.

### GetFirstOk

`func (o *ListUsersResponseUsersInnerName) GetFirstOk() (*string, bool)`

GetFirstOk returns a tuple with the First field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirst

`func (o *ListUsersResponseUsersInnerName) SetFirst(v string)`

SetFirst sets First field to given value.

### HasFirst

`func (o *ListUsersResponseUsersInnerName) HasFirst() bool`

HasFirst returns a boolean if a field has been set.

### GetLast

`func (o *ListUsersResponseUsersInnerName) GetLast() string`

GetLast returns the Last field if non-nil, zero value otherwise.

### GetLastOk

`func (o *ListUsersResponseUsersInnerName) GetLastOk() (*string, bool)`

GetLastOk returns a tuple with the Last field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLast

`func (o *ListUsersResponseUsersInnerName) SetLast(v string)`

SetLast sets Last field to given value.

### HasLast

`func (o *ListUsersResponseUsersInnerName) HasLast() bool`

HasLast returns a boolean if a field has been set.

### GetMiddle

`func (o *ListUsersResponseUsersInnerName) GetMiddle() string`

GetMiddle returns the Middle field if non-nil, zero value otherwise.

### GetMiddleOk

`func (o *ListUsersResponseUsersInnerName) GetMiddleOk() (*string, bool)`

GetMiddleOk returns a tuple with the Middle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMiddle

`func (o *ListUsersResponseUsersInnerName) SetMiddle(v string)`

SetMiddle sets Middle field to given value.

### HasMiddle

`func (o *ListUsersResponseUsersInnerName) HasMiddle() bool`

HasMiddle returns a boolean if a field has been set.

### GetSuffix

`func (o *ListUsersResponseUsersInnerName) GetSuffix() string`

GetSuffix returns the Suffix field if non-nil, zero value otherwise.

### GetSuffixOk

`func (o *ListUsersResponseUsersInnerName) GetSuffixOk() (*string, bool)`

GetSuffixOk returns a tuple with the Suffix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuffix

`func (o *ListUsersResponseUsersInnerName) SetSuffix(v string)`

SetSuffix sets Suffix field to given value.

### HasSuffix

`func (o *ListUsersResponseUsersInnerName) HasSuffix() bool`

HasSuffix returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


