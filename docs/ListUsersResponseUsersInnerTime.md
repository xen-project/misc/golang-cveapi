# ListUsersResponseUsersInnerTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | Pointer to **time.Time** | The time the user was created | [optional] 
**Modified** | Pointer to **time.Time** | The last time the user was modified | [optional] 

## Methods

### NewListUsersResponseUsersInnerTime

`func NewListUsersResponseUsersInnerTime() *ListUsersResponseUsersInnerTime`

NewListUsersResponseUsersInnerTime instantiates a new ListUsersResponseUsersInnerTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListUsersResponseUsersInnerTimeWithDefaults

`func NewListUsersResponseUsersInnerTimeWithDefaults() *ListUsersResponseUsersInnerTime`

NewListUsersResponseUsersInnerTimeWithDefaults instantiates a new ListUsersResponseUsersInnerTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCreated

`func (o *ListUsersResponseUsersInnerTime) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *ListUsersResponseUsersInnerTime) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *ListUsersResponseUsersInnerTime) SetCreated(v time.Time)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *ListUsersResponseUsersInnerTime) HasCreated() bool`

HasCreated returns a boolean if a field has been set.

### GetModified

`func (o *ListUsersResponseUsersInnerTime) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *ListUsersResponseUsersInnerTime) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *ListUsersResponseUsersInnerTime) SetModified(v time.Time)`

SetModified sets Modified field to given value.

### HasModified

`func (o *ListUsersResponseUsersInnerTime) HasModified() bool`

HasModified returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


