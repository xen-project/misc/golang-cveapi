# \OrganizationApi

All URIs are relative to *https://cveawg.mitre.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**OrgAll**](OrganizationApi.md#OrgAll) | **Get** /org | Retrieves all organizations (accessible to Secretariat)
[**OrgCreateSingle**](OrganizationApi.md#OrgCreateSingle) | **Post** /org | Creates an organization as specified in the request body (accessible to Secretariat)
[**OrgIdQuota**](OrganizationApi.md#OrgIdQuota) | **Get** /org/{shortname}/id_quota | Retrieves an organization&#39;s CVE ID quota (accessible to all registered users)
[**OrgSingle**](OrganizationApi.md#OrgSingle) | **Get** /org/{identifier} | Retrieves information about the organization specified by short name or UUID (accessible to all registered users)
[**OrgUpdateSingle**](OrganizationApi.md#OrgUpdateSingle) | **Put** /org/{shortname} | Updates information about the organization specified by short name (accessible to Secretariat)



## OrgAll

> ListOrgsResponse OrgAll(ctx).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()

Retrieves all organizations (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    page := int32(56) // int32 | The current page in the paginator (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationApi.OrgAll(context.Background()).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationApi.OrgAll``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrgAll`: ListOrgsResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationApi.OrgAll`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiOrgAllRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **page** | **int32** | The current page in the paginator | 

### Return type

[**ListOrgsResponse**](ListOrgsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrgCreateSingle

> CreateOrgResponse OrgCreateSingle(ctx).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateOrgRequest(createOrgRequest).Execute()

Creates an organization as specified in the request body (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    createOrgRequest := *openapiclient.NewCreateOrgRequest("ShortName_example", "Name_example") // CreateOrgRequest | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationApi.OrgCreateSingle(context.Background()).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateOrgRequest(createOrgRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationApi.OrgCreateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrgCreateSingle`: CreateOrgResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationApi.OrgCreateSingle`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiOrgCreateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **createOrgRequest** | [**CreateOrgRequest**](CreateOrgRequest.md) |  | 

### Return type

[**CreateOrgResponse**](CreateOrgResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrgIdQuota

> GetOrgQuotaResponse OrgIdQuota(ctx, shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Retrieves an organization's CVE ID quota (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationApi.OrgIdQuota(context.Background(), shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationApi.OrgIdQuota``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrgIdQuota`: GetOrgQuotaResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationApi.OrgIdQuota`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 

### Other Parameters

Other parameters are passed through a pointer to a apiOrgIdQuotaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

[**GetOrgQuotaResponse**](GetOrgQuotaResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrgSingle

> GetOrgResponse OrgSingle(ctx, identifier).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Retrieves information about the organization specified by short name or UUID (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    identifier := "identifier_example" // string | The shortname or UUID of the organization
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationApi.OrgSingle(context.Background(), identifier).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationApi.OrgSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrgSingle`: GetOrgResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationApi.OrgSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**identifier** | **string** | The shortname or UUID of the organization | 

### Other Parameters

Other parameters are passed through a pointer to a apiOrgSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

[**GetOrgResponse**](GetOrgResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrgUpdateSingle

> UpdateOrgResponse OrgUpdateSingle(ctx, shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).IdQuota(idQuota).Name(name).NewShortName(newShortName).ActiveRolesAdd(activeRolesAdd).ActiveRolesRemove(activeRolesRemove).Execute()

Updates information about the organization specified by short name (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    idQuota := int32(56) // int32 | The new number of CVE IDs the organization is allowed to have in the RESERVED state at one time (optional)
    name := "name_example" // string | The new name for the organization (optional)
    newShortName := "newShortName_example" // string | The new shortname for the organization (optional)
    activeRolesAdd := "activeRolesAdd_example" // string | Add an active role to the organization (optional)
    activeRolesRemove := "activeRolesRemove_example" // string | Remove an active role from the organization (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationApi.OrgUpdateSingle(context.Background(), shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).IdQuota(idQuota).Name(name).NewShortName(newShortName).ActiveRolesAdd(activeRolesAdd).ActiveRolesRemove(activeRolesRemove).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationApi.OrgUpdateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrgUpdateSingle`: UpdateOrgResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationApi.OrgUpdateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 

### Other Parameters

Other parameters are passed through a pointer to a apiOrgUpdateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **idQuota** | **int32** | The new number of CVE IDs the organization is allowed to have in the RESERVED state at one time | 
 **name** | **string** | The new name for the organization | 
 **newShortName** | **string** | The new shortname for the organization | 
 **activeRolesAdd** | **string** | Add an active role to the organization | 
 **activeRolesRemove** | **string** | Remove an active role from the organization | 

### Return type

[**UpdateOrgResponse**](UpdateOrgResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

