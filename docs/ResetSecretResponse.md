# ResetSecretResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**APISecret** | Pointer to **string** |  | [optional] 

## Methods

### NewResetSecretResponse

`func NewResetSecretResponse() *ResetSecretResponse`

NewResetSecretResponse instantiates a new ResetSecretResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewResetSecretResponseWithDefaults

`func NewResetSecretResponseWithDefaults() *ResetSecretResponse`

NewResetSecretResponseWithDefaults instantiates a new ResetSecretResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAPISecret

`func (o *ResetSecretResponse) GetAPISecret() string`

GetAPISecret returns the APISecret field if non-nil, zero value otherwise.

### GetAPISecretOk

`func (o *ResetSecretResponse) GetAPISecretOk() (*string, bool)`

GetAPISecretOk returns a tuple with the APISecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAPISecret

`func (o *ResetSecretResponse) SetAPISecret(v string)`

SetAPISecret sets APISecret field to given value.

### HasAPISecret

`func (o *ResetSecretResponse) HasAPISecret() bool`

HasAPISecret returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


