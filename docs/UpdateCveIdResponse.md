# UpdateCveIdResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Updated** | Pointer to [**ListCveIdsResponseCveIdsInner**](ListCveIdsResponseCveIdsInner.md) |  | [optional] 

## Methods

### NewUpdateCveIdResponse

`func NewUpdateCveIdResponse() *UpdateCveIdResponse`

NewUpdateCveIdResponse instantiates a new UpdateCveIdResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateCveIdResponseWithDefaults

`func NewUpdateCveIdResponseWithDefaults() *UpdateCveIdResponse`

NewUpdateCveIdResponseWithDefaults instantiates a new UpdateCveIdResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateCveIdResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateCveIdResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateCveIdResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateCveIdResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetUpdated

`func (o *UpdateCveIdResponse) GetUpdated() ListCveIdsResponseCveIdsInner`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *UpdateCveIdResponse) GetUpdatedOk() (*ListCveIdsResponseCveIdsInner, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *UpdateCveIdResponse) SetUpdated(v ListCveIdsResponseCveIdsInner)`

SetUpdated sets Updated field to given value.

### HasUpdated

`func (o *UpdateCveIdResponse) HasUpdated() bool`

HasUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


