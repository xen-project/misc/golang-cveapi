# UpdateCveRecordRejectionRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CnaContainer** | [**UpdateCveRecordRejectionRequestCnaContainer**](UpdateCveRecordRejectionRequestCnaContainer.md) |  | 

## Methods

### NewUpdateCveRecordRejectionRequest

`func NewUpdateCveRecordRejectionRequest(cnaContainer UpdateCveRecordRejectionRequestCnaContainer, ) *UpdateCveRecordRejectionRequest`

NewUpdateCveRecordRejectionRequest instantiates a new UpdateCveRecordRejectionRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateCveRecordRejectionRequestWithDefaults

`func NewUpdateCveRecordRejectionRequestWithDefaults() *UpdateCveRecordRejectionRequest`

NewUpdateCveRecordRejectionRequestWithDefaults instantiates a new UpdateCveRecordRejectionRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCnaContainer

`func (o *UpdateCveRecordRejectionRequest) GetCnaContainer() UpdateCveRecordRejectionRequestCnaContainer`

GetCnaContainer returns the CnaContainer field if non-nil, zero value otherwise.

### GetCnaContainerOk

`func (o *UpdateCveRecordRejectionRequest) GetCnaContainerOk() (*UpdateCveRecordRejectionRequestCnaContainer, bool)`

GetCnaContainerOk returns a tuple with the CnaContainer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCnaContainer

`func (o *UpdateCveRecordRejectionRequest) SetCnaContainer(v UpdateCveRecordRejectionRequestCnaContainer)`

SetCnaContainer sets CnaContainer field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


