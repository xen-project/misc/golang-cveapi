# UpdateCveRecordRejectionRequestCnaContainer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProviderMetadata** | Pointer to [**GetCveRecordResponseContainersCnaProviderMetadata**](GetCveRecordResponseContainersCnaProviderMetadata.md) |  | [optional] 
**RejectedReasons** | [**[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner**](CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner.md) |  | 
**ReplacedBy** | Pointer to **[]string** |  | [optional] 

## Methods

### NewUpdateCveRecordRejectionRequestCnaContainer

`func NewUpdateCveRecordRejectionRequestCnaContainer(rejectedReasons []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner, ) *UpdateCveRecordRejectionRequestCnaContainer`

NewUpdateCveRecordRejectionRequestCnaContainer instantiates a new UpdateCveRecordRejectionRequestCnaContainer object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateCveRecordRejectionRequestCnaContainerWithDefaults

`func NewUpdateCveRecordRejectionRequestCnaContainerWithDefaults() *UpdateCveRecordRejectionRequestCnaContainer`

NewUpdateCveRecordRejectionRequestCnaContainerWithDefaults instantiates a new UpdateCveRecordRejectionRequestCnaContainer object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProviderMetadata

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetProviderMetadata() GetCveRecordResponseContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetProviderMetadataOk() (*GetCveRecordResponseContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *UpdateCveRecordRejectionRequestCnaContainer) SetProviderMetadata(v GetCveRecordResponseContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *UpdateCveRecordRejectionRequestCnaContainer) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.

### GetRejectedReasons

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetRejectedReasons() []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner`

GetRejectedReasons returns the RejectedReasons field if non-nil, zero value otherwise.

### GetRejectedReasonsOk

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetRejectedReasonsOk() (*[]CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner, bool)`

GetRejectedReasonsOk returns a tuple with the RejectedReasons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedReasons

`func (o *UpdateCveRecordRejectionRequestCnaContainer) SetRejectedReasons(v []CreateCveRecordRejectionResponseCreatedContainersCnaRejectedReasonsInner)`

SetRejectedReasons sets RejectedReasons field to given value.


### GetReplacedBy

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetReplacedBy() []string`

GetReplacedBy returns the ReplacedBy field if non-nil, zero value otherwise.

### GetReplacedByOk

`func (o *UpdateCveRecordRejectionRequestCnaContainer) GetReplacedByOk() (*[]string, bool)`

GetReplacedByOk returns a tuple with the ReplacedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReplacedBy

`func (o *UpdateCveRecordRejectionRequestCnaContainer) SetReplacedBy(v []string)`

SetReplacedBy sets ReplacedBy field to given value.

### HasReplacedBy

`func (o *UpdateCveRecordRejectionRequestCnaContainer) HasReplacedBy() bool`

HasReplacedBy returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


