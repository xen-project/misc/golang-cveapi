# UpdateCveRecordRejectionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Updated** | Pointer to [**CreateCveRecordRejectionResponseCreated**](CreateCveRecordRejectionResponseCreated.md) |  | [optional] 

## Methods

### NewUpdateCveRecordRejectionResponse

`func NewUpdateCveRecordRejectionResponse() *UpdateCveRecordRejectionResponse`

NewUpdateCveRecordRejectionResponse instantiates a new UpdateCveRecordRejectionResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateCveRecordRejectionResponseWithDefaults

`func NewUpdateCveRecordRejectionResponseWithDefaults() *UpdateCveRecordRejectionResponse`

NewUpdateCveRecordRejectionResponseWithDefaults instantiates a new UpdateCveRecordRejectionResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateCveRecordRejectionResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateCveRecordRejectionResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateCveRecordRejectionResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateCveRecordRejectionResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetUpdated

`func (o *UpdateCveRecordRejectionResponse) GetUpdated() CreateCveRecordRejectionResponseCreated`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *UpdateCveRecordRejectionResponse) GetUpdatedOk() (*CreateCveRecordRejectionResponseCreated, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *UpdateCveRecordRejectionResponse) SetUpdated(v CreateCveRecordRejectionResponseCreated)`

SetUpdated sets Updated field to given value.

### HasUpdated

`func (o *UpdateCveRecordRejectionResponse) HasUpdated() bool`

HasUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


