# UpdateFullCveRecordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Updated** | Pointer to [**UpdateFullCveRecordResponseUpdated**](UpdateFullCveRecordResponseUpdated.md) |  | [optional] 

## Methods

### NewUpdateFullCveRecordResponse

`func NewUpdateFullCveRecordResponse() *UpdateFullCveRecordResponse`

NewUpdateFullCveRecordResponse instantiates a new UpdateFullCveRecordResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateFullCveRecordResponseWithDefaults

`func NewUpdateFullCveRecordResponseWithDefaults() *UpdateFullCveRecordResponse`

NewUpdateFullCveRecordResponseWithDefaults instantiates a new UpdateFullCveRecordResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateFullCveRecordResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateFullCveRecordResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateFullCveRecordResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateFullCveRecordResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetUpdated

`func (o *UpdateFullCveRecordResponse) GetUpdated() UpdateFullCveRecordResponseUpdated`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *UpdateFullCveRecordResponse) GetUpdatedOk() (*UpdateFullCveRecordResponseUpdated, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *UpdateFullCveRecordResponse) SetUpdated(v UpdateFullCveRecordResponseUpdated)`

SetUpdated sets Updated field to given value.

### HasUpdated

`func (o *UpdateFullCveRecordResponse) HasUpdated() bool`

HasUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


