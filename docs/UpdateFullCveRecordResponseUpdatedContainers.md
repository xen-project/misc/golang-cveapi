# UpdateFullCveRecordResponseUpdatedContainers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cna** | Pointer to [**UpdateFullCveRecordResponseUpdatedContainersCna**](UpdateFullCveRecordResponseUpdatedContainersCna.md) |  | [optional] 

## Methods

### NewUpdateFullCveRecordResponseUpdatedContainers

`func NewUpdateFullCveRecordResponseUpdatedContainers() *UpdateFullCveRecordResponseUpdatedContainers`

NewUpdateFullCveRecordResponseUpdatedContainers instantiates a new UpdateFullCveRecordResponseUpdatedContainers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateFullCveRecordResponseUpdatedContainersWithDefaults

`func NewUpdateFullCveRecordResponseUpdatedContainersWithDefaults() *UpdateFullCveRecordResponseUpdatedContainers`

NewUpdateFullCveRecordResponseUpdatedContainersWithDefaults instantiates a new UpdateFullCveRecordResponseUpdatedContainers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCna

`func (o *UpdateFullCveRecordResponseUpdatedContainers) GetCna() UpdateFullCveRecordResponseUpdatedContainersCna`

GetCna returns the Cna field if non-nil, zero value otherwise.

### GetCnaOk

`func (o *UpdateFullCveRecordResponseUpdatedContainers) GetCnaOk() (*UpdateFullCveRecordResponseUpdatedContainersCna, bool)`

GetCnaOk returns a tuple with the Cna field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCna

`func (o *UpdateFullCveRecordResponseUpdatedContainers) SetCna(v UpdateFullCveRecordResponseUpdatedContainersCna)`

SetCna sets Cna field to given value.

### HasCna

`func (o *UpdateFullCveRecordResponseUpdatedContainers) HasCna() bool`

HasCna returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


