# UpdateFullCveRecordResponseUpdatedContainersCna

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Affected** | Pointer to [**CreateFullCveRecordRequestContainersCnaAffectedInner**](CreateFullCveRecordRequestContainersCnaAffectedInner.md) |  | [optional] 
**Descriptions** | Pointer to [**[]GetCveRecordResponseContainersCnaDescriptionsInner**](GetCveRecordResponseContainersCnaDescriptionsInner.md) |  | [optional] 
**ProblemTypes** | Pointer to [**[]GetCveRecordResponseContainersCnaProblemTypesInner**](GetCveRecordResponseContainersCnaProblemTypesInner.md) |  | [optional] 
**ProviderMetadata** | Pointer to [**GetCveRecordResponseContainersCnaProviderMetadata**](GetCveRecordResponseContainersCnaProviderMetadata.md) |  | [optional] 
**References** | Pointer to [**[]UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner**](UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner.md) |  | [optional] 
**DataType** | Pointer to **string** |  | [optional] 
**DataVersion** | Pointer to **string** |  | [optional] 

## Methods

### NewUpdateFullCveRecordResponseUpdatedContainersCna

`func NewUpdateFullCveRecordResponseUpdatedContainersCna() *UpdateFullCveRecordResponseUpdatedContainersCna`

NewUpdateFullCveRecordResponseUpdatedContainersCna instantiates a new UpdateFullCveRecordResponseUpdatedContainersCna object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateFullCveRecordResponseUpdatedContainersCnaWithDefaults

`func NewUpdateFullCveRecordResponseUpdatedContainersCnaWithDefaults() *UpdateFullCveRecordResponseUpdatedContainersCna`

NewUpdateFullCveRecordResponseUpdatedContainersCnaWithDefaults instantiates a new UpdateFullCveRecordResponseUpdatedContainersCna object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffected

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetAffected() CreateFullCveRecordRequestContainersCnaAffectedInner`

GetAffected returns the Affected field if non-nil, zero value otherwise.

### GetAffectedOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetAffectedOk() (*CreateFullCveRecordRequestContainersCnaAffectedInner, bool)`

GetAffectedOk returns a tuple with the Affected field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffected

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetAffected(v CreateFullCveRecordRequestContainersCnaAffectedInner)`

SetAffected sets Affected field to given value.

### HasAffected

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasAffected() bool`

HasAffected returns a boolean if a field has been set.

### GetDescriptions

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner`

GetDescriptions returns the Descriptions field if non-nil, zero value otherwise.

### GetDescriptionsOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDescriptionsOk() (*[]GetCveRecordResponseContainersCnaDescriptionsInner, bool)`

GetDescriptionsOk returns a tuple with the Descriptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptions

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner)`

SetDescriptions sets Descriptions field to given value.

### HasDescriptions

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasDescriptions() bool`

HasDescriptions returns a boolean if a field has been set.

### GetProblemTypes

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner`

GetProblemTypes returns the ProblemTypes field if non-nil, zero value otherwise.

### GetProblemTypesOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetProblemTypesOk() (*[]GetCveRecordResponseContainersCnaProblemTypesInner, bool)`

GetProblemTypesOk returns a tuple with the ProblemTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProblemTypes

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner)`

SetProblemTypes sets ProblemTypes field to given value.

### HasProblemTypes

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasProblemTypes() bool`

HasProblemTypes returns a boolean if a field has been set.

### GetProviderMetadata

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetProviderMetadata() GetCveRecordResponseContainersCnaProviderMetadata`

GetProviderMetadata returns the ProviderMetadata field if non-nil, zero value otherwise.

### GetProviderMetadataOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetProviderMetadataOk() (*GetCveRecordResponseContainersCnaProviderMetadata, bool)`

GetProviderMetadataOk returns a tuple with the ProviderMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProviderMetadata

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetProviderMetadata(v GetCveRecordResponseContainersCnaProviderMetadata)`

SetProviderMetadata sets ProviderMetadata field to given value.

### HasProviderMetadata

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasProviderMetadata() bool`

HasProviderMetadata returns a boolean if a field has been set.

### GetReferences

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetReferences() []UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner`

GetReferences returns the References field if non-nil, zero value otherwise.

### GetReferencesOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetReferencesOk() (*[]UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner, bool)`

GetReferencesOk returns a tuple with the References field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferences

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetReferences(v []UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner)`

SetReferences sets References field to given value.

### HasReferences

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasReferences() bool`

HasReferences returns a boolean if a field has been set.

### GetDataType

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetDataVersion

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDataVersion() string`

GetDataVersion returns the DataVersion field if non-nil, zero value otherwise.

### GetDataVersionOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) GetDataVersionOk() (*string, bool)`

GetDataVersionOk returns a tuple with the DataVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataVersion

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) SetDataVersion(v string)`

SetDataVersion sets DataVersion field to given value.

### HasDataVersion

`func (o *UpdateFullCveRecordResponseUpdatedContainersCna) HasDataVersion() bool`

HasDataVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


