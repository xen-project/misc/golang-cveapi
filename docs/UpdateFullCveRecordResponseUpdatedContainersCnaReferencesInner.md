# UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Refsource** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 

## Methods

### NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner

`func NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner() *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner`

NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner instantiates a new UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInnerWithDefaults

`func NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInnerWithDefaults() *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner`

NewUpdateFullCveRecordResponseUpdatedContainersCnaReferencesInnerWithDefaults instantiates a new UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetRefsource

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetRefsource() string`

GetRefsource returns the Refsource field if non-nil, zero value otherwise.

### GetRefsourceOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetRefsourceOk() (*string, bool)`

GetRefsourceOk returns a tuple with the Refsource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRefsource

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) SetRefsource(v string)`

SetRefsource sets Refsource field to given value.

### HasRefsource

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) HasRefsource() bool`

HasRefsource returns a boolean if a field has been set.

### GetUrl

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *UpdateFullCveRecordResponseUpdatedContainersCnaReferencesInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


