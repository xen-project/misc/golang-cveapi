# UpdateOrgResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** | Success description | [optional] 
**Updated** | Pointer to [**CreateOrgResponseCreated**](CreateOrgResponseCreated.md) |  | [optional] 

## Methods

### NewUpdateOrgResponse

`func NewUpdateOrgResponse() *UpdateOrgResponse`

NewUpdateOrgResponse instantiates a new UpdateOrgResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateOrgResponseWithDefaults

`func NewUpdateOrgResponseWithDefaults() *UpdateOrgResponse`

NewUpdateOrgResponseWithDefaults instantiates a new UpdateOrgResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateOrgResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateOrgResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateOrgResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateOrgResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetUpdated

`func (o *UpdateOrgResponse) GetUpdated() CreateOrgResponseCreated`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *UpdateOrgResponse) GetUpdatedOk() (*CreateOrgResponseCreated, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *UpdateOrgResponse) SetUpdated(v CreateOrgResponseCreated)`

SetUpdated sets Updated field to given value.

### HasUpdated

`func (o *UpdateOrgResponse) HasUpdated() bool`

HasUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


