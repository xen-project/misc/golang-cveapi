# UpdateUserResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** | Success description | [optional] 
**Updated** | Pointer to [**ListUsersResponseUsersInner**](ListUsersResponseUsersInner.md) |  | [optional] 

## Methods

### NewUpdateUserResponse

`func NewUpdateUserResponse() *UpdateUserResponse`

NewUpdateUserResponse instantiates a new UpdateUserResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateUserResponseWithDefaults

`func NewUpdateUserResponseWithDefaults() *UpdateUserResponse`

NewUpdateUserResponseWithDefaults instantiates a new UpdateUserResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateUserResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateUserResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateUserResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateUserResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetUpdated

`func (o *UpdateUserResponse) GetUpdated() ListUsersResponseUsersInner`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *UpdateUserResponse) GetUpdatedOk() (*ListUsersResponseUsersInner, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *UpdateUserResponse) SetUpdated(v ListUsersResponseUsersInner)`

SetUpdated sets Updated field to given value.

### HasUpdated

`func (o *UpdateUserResponse) HasUpdated() bool`

HasUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


