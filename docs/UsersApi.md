# \UsersApi

All URIs are relative to *https://cveawg.mitre.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UserAll**](UsersApi.md#UserAll) | **Get** /users | Retrieves information about all registered users (accessible to Secretariat)
[**UserCreateSingle**](UsersApi.md#UserCreateSingle) | **Post** /org/{shortname}/user | Create a user with the provided short name as the owning organization (accessible to Admins and Secretariats)
[**UserOrgAll**](UsersApi.md#UserOrgAll) | **Get** /org/{shortname}/users | Retrieves all users for the organization with the specified short name (accessible to all registered users)
[**UserResetSecret**](UsersApi.md#UserResetSecret) | **Put** /org/{shortname}/user/{username}/reset_secret | Reset the API key for a user (accessible to all registered users)
[**UserSingle**](UsersApi.md#UserSingle) | **Get** /org/{shortname}/user/{username} | Retrieves information about a user for the specified username and organization short name (accessible to all registered users)
[**UserUpdateSingle**](UsersApi.md#UserUpdateSingle) | **Put** /org/{shortname}/user/{username} | Updates information about a user for the specified username and organization shortname (accessible to all registered users)



## UserAll

> ListUsersResponse UserAll(ctx).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()

Retrieves information about all registered users (accessible to Secretariat)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    page := int32(56) // int32 | The current page in the paginator (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserAll(context.Background()).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserAll``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserAll`: ListUsersResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserAll`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUserAllRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **page** | **int32** | The current page in the paginator | 

### Return type

[**ListUsersResponse**](ListUsersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserCreateSingle

> CreateUserResponse UserCreateSingle(ctx, shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateUserRequest(createUserRequest).Execute()

Create a user with the provided short name as the owning organization (accessible to Admins and Secretariats)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    createUserRequest := *openapiclient.NewCreateUserRequest("Username_example") // CreateUserRequest | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserCreateSingle(context.Background(), shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).CreateUserRequest(createUserRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserCreateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserCreateSingle`: CreateUserResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserCreateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 

### Other Parameters

Other parameters are passed through a pointer to a apiUserCreateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **createUserRequest** | [**CreateUserRequest**](CreateUserRequest.md) |  | 

### Return type

[**CreateUserResponse**](CreateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserOrgAll

> ListUsersResponse UserOrgAll(ctx, shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()

Retrieves all users for the organization with the specified short name (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    page := int32(56) // int32 | The current page in the paginator (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserOrgAll(context.Background(), shortname).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Page(page).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserOrgAll``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserOrgAll`: ListUsersResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserOrgAll`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 

### Other Parameters

Other parameters are passed through a pointer to a apiUserOrgAllRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **page** | **int32** | The current page in the paginator | 

### Return type

[**ListUsersResponse**](ListUsersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserResetSecret

> ResetSecretResponse UserResetSecret(ctx, shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Reset the API key for a user (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    username := "username_example" // string | The username of the user
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserResetSecret(context.Background(), shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserResetSecret``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserResetSecret`: ResetSecretResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserResetSecret`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 
**username** | **string** | The username of the user | 

### Other Parameters

Other parameters are passed through a pointer to a apiUserResetSecretRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

[**ResetSecretResponse**](ResetSecretResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserSingle

> GetUserResponse UserSingle(ctx, shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()

Retrieves information about a user for the specified username and organization short name (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    username := "username_example" // string | The username of the user
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserSingle(context.Background(), shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserSingle`: GetUserResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 
**username** | **string** | The username of the user | 

### Other Parameters

Other parameters are passed through a pointer to a apiUserSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 

### Return type

[**GetUserResponse**](GetUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserUpdateSingle

> UpdateUserResponse UserUpdateSingle(ctx, shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Active(active).ActiveRolesAdd(activeRolesAdd).ActiveRolesRemove(activeRolesRemove).NameFirst(nameFirst).NameLast(nameLast).NameMiddle(nameMiddle).NameSuffix(nameSuffix).NewUsername(newUsername).OrgShortName(orgShortName).Execute()

Updates information about a user for the specified username and organization shortname (accessible to all registered users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {
    shortname := "shortname_example" // string | The shortname of the organization
    username := "username_example" // string | The username of the user
    cVEAPIORG := "cVEAPIORG_example" // string | The shortname for the organization associated with the user requesting authentication
    cVEAPIUSER := "cVEAPIUSER_example" // string | The username for the account making the request
    cVEAPIKEY := "cVEAPIKEY_example" // string | The user's API key
    active := true // bool | The new active state for the user entry.  Accepted values are 1, true, or yes to indicate true, and 0, false, or no to indicate false (optional)
    activeRolesAdd := "activeRolesAdd_example" // string | Add an active role to the user (optional)
    activeRolesRemove := "activeRolesRemove_example" // string | Remove an active role from the user (optional)
    nameFirst := "nameFirst_example" // string | The new first name for the user entry (optional)
    nameLast := "nameLast_example" // string | The new last name for the user entry (optional)
    nameMiddle := "nameMiddle_example" // string | The new middle name for the user entry (optional)
    nameSuffix := "nameSuffix_example" // string | The new suffix for the user entry (optional)
    newUsername := "newUsername_example" // string | The new username for the user, preferably the user's email address. Must be 3-128 characters in length; allowed characters are alphanumeric and -_@. (optional)
    orgShortName := "orgShortName_example" // string | The new organization for the user (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UsersApi.UserUpdateSingle(context.Background(), shortname, username).CVEAPIORG(cVEAPIORG).CVEAPIUSER(cVEAPIUSER).CVEAPIKEY(cVEAPIKEY).Active(active).ActiveRolesAdd(activeRolesAdd).ActiveRolesRemove(activeRolesRemove).NameFirst(nameFirst).NameLast(nameLast).NameMiddle(nameMiddle).NameSuffix(nameSuffix).NewUsername(newUsername).OrgShortName(orgShortName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UsersApi.UserUpdateSingle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UserUpdateSingle`: UpdateUserResponse
    fmt.Fprintf(os.Stdout, "Response from `UsersApi.UserUpdateSingle`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**shortname** | **string** | The shortname of the organization | 
**username** | **string** | The username of the user | 

### Other Parameters

Other parameters are passed through a pointer to a apiUserUpdateSingleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cVEAPIORG** | **string** | The shortname for the organization associated with the user requesting authentication | 
 **cVEAPIUSER** | **string** | The username for the account making the request | 
 **cVEAPIKEY** | **string** | The user&#39;s API key | 
 **active** | **bool** | The new active state for the user entry.  Accepted values are 1, true, or yes to indicate true, and 0, false, or no to indicate false | 
 **activeRolesAdd** | **string** | Add an active role to the user | 
 **activeRolesRemove** | **string** | Remove an active role from the user | 
 **nameFirst** | **string** | The new first name for the user entry | 
 **nameLast** | **string** | The new last name for the user entry | 
 **nameMiddle** | **string** | The new middle name for the user entry | 
 **nameSuffix** | **string** | The new suffix for the user entry | 
 **newUsername** | **string** | The new username for the user, preferably the user&#39;s email address. Must be 3-128 characters in length; allowed characters are alphanumeric and -_@. | 
 **orgShortName** | **string** | The new organization for the user | 

### Return type

[**UpdateUserResponse**](UpdateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

