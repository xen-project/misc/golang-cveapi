# \UtilitiesApi

All URIs are relative to *https://cveawg.mitre.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**HealthCheck**](UtilitiesApi.md#HealthCheck) | **Get** /health-check | Checks that the system is running (accessible to all users)



## HealthCheck

> HealthCheck(ctx).Execute()

Checks that the system is running (accessible to all users)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "gitlab.com/xen-project/misc/go-cveapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UtilitiesApi.HealthCheck(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UtilitiesApi.HealthCheck``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiHealthCheckRequest struct via the builder pattern


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

