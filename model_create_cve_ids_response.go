/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the CreateCveIdsResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateCveIdsResponse{}

// CreateCveIdsResponse struct for CreateCveIdsResponse
type CreateCveIdsResponse struct {
	CveIds []CreateCveIdsResponseCveIdsInner `json:"cve_ids,omitempty"`
	Meta *CreateCveIdsResponseMeta `json:"meta,omitempty"`
}

// NewCreateCveIdsResponse instantiates a new CreateCveIdsResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateCveIdsResponse() *CreateCveIdsResponse {
	this := CreateCveIdsResponse{}
	return &this
}

// NewCreateCveIdsResponseWithDefaults instantiates a new CreateCveIdsResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateCveIdsResponseWithDefaults() *CreateCveIdsResponse {
	this := CreateCveIdsResponse{}
	return &this
}

// GetCveIds returns the CveIds field value if set, zero value otherwise.
func (o *CreateCveIdsResponse) GetCveIds() []CreateCveIdsResponseCveIdsInner {
	if o == nil || IsNil(o.CveIds) {
		var ret []CreateCveIdsResponseCveIdsInner
		return ret
	}
	return o.CveIds
}

// GetCveIdsOk returns a tuple with the CveIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveIdsResponse) GetCveIdsOk() ([]CreateCveIdsResponseCveIdsInner, bool) {
	if o == nil || IsNil(o.CveIds) {
		return nil, false
	}
	return o.CveIds, true
}

// HasCveIds returns a boolean if a field has been set.
func (o *CreateCveIdsResponse) HasCveIds() bool {
	if o != nil && !IsNil(o.CveIds) {
		return true
	}

	return false
}

// SetCveIds gets a reference to the given []CreateCveIdsResponseCveIdsInner and assigns it to the CveIds field.
func (o *CreateCveIdsResponse) SetCveIds(v []CreateCveIdsResponseCveIdsInner) {
	o.CveIds = v
}

// GetMeta returns the Meta field value if set, zero value otherwise.
func (o *CreateCveIdsResponse) GetMeta() CreateCveIdsResponseMeta {
	if o == nil || IsNil(o.Meta) {
		var ret CreateCveIdsResponseMeta
		return ret
	}
	return *o.Meta
}

// GetMetaOk returns a tuple with the Meta field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveIdsResponse) GetMetaOk() (*CreateCveIdsResponseMeta, bool) {
	if o == nil || IsNil(o.Meta) {
		return nil, false
	}
	return o.Meta, true
}

// HasMeta returns a boolean if a field has been set.
func (o *CreateCveIdsResponse) HasMeta() bool {
	if o != nil && !IsNil(o.Meta) {
		return true
	}

	return false
}

// SetMeta gets a reference to the given CreateCveIdsResponseMeta and assigns it to the Meta field.
func (o *CreateCveIdsResponse) SetMeta(v CreateCveIdsResponseMeta) {
	o.Meta = &v
}

func (o CreateCveIdsResponse) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateCveIdsResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.CveIds) {
		toSerialize["cve_ids"] = o.CveIds
	}
	if !IsNil(o.Meta) {
		toSerialize["meta"] = o.Meta
	}
	return toSerialize, nil
}

type NullableCreateCveIdsResponse struct {
	value *CreateCveIdsResponse
	isSet bool
}

func (v NullableCreateCveIdsResponse) Get() *CreateCveIdsResponse {
	return v.value
}

func (v *NullableCreateCveIdsResponse) Set(val *CreateCveIdsResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateCveIdsResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateCveIdsResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateCveIdsResponse(val *CreateCveIdsResponse) *NullableCreateCveIdsResponse {
	return &NullableCreateCveIdsResponse{value: val, isSet: true}
}

func (v NullableCreateCveIdsResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateCveIdsResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


