/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the CreateCveRecordRejectionResponseCreated type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateCveRecordRejectionResponseCreated{}

// CreateCveRecordRejectionResponseCreated struct for CreateCveRecordRejectionResponseCreated
type CreateCveRecordRejectionResponseCreated struct {
	Containers *CreateCveRecordRejectionResponseCreatedContainers `json:"containers,omitempty"`
	CveMetadata *GetCveRecordResponseCveMetadata `json:"cveMetadata,omitempty"`
	DataType *string `json:"dataType,omitempty"`
	DataVersion *string `json:"dataVersion,omitempty"`
}

// NewCreateCveRecordRejectionResponseCreated instantiates a new CreateCveRecordRejectionResponseCreated object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateCveRecordRejectionResponseCreated() *CreateCveRecordRejectionResponseCreated {
	this := CreateCveRecordRejectionResponseCreated{}
	return &this
}

// NewCreateCveRecordRejectionResponseCreatedWithDefaults instantiates a new CreateCveRecordRejectionResponseCreated object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateCveRecordRejectionResponseCreatedWithDefaults() *CreateCveRecordRejectionResponseCreated {
	this := CreateCveRecordRejectionResponseCreated{}
	return &this
}

// GetContainers returns the Containers field value if set, zero value otherwise.
func (o *CreateCveRecordRejectionResponseCreated) GetContainers() CreateCveRecordRejectionResponseCreatedContainers {
	if o == nil || IsNil(o.Containers) {
		var ret CreateCveRecordRejectionResponseCreatedContainers
		return ret
	}
	return *o.Containers
}

// GetContainersOk returns a tuple with the Containers field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordRejectionResponseCreated) GetContainersOk() (*CreateCveRecordRejectionResponseCreatedContainers, bool) {
	if o == nil || IsNil(o.Containers) {
		return nil, false
	}
	return o.Containers, true
}

// HasContainers returns a boolean if a field has been set.
func (o *CreateCveRecordRejectionResponseCreated) HasContainers() bool {
	if o != nil && !IsNil(o.Containers) {
		return true
	}

	return false
}

// SetContainers gets a reference to the given CreateCveRecordRejectionResponseCreatedContainers and assigns it to the Containers field.
func (o *CreateCveRecordRejectionResponseCreated) SetContainers(v CreateCveRecordRejectionResponseCreatedContainers) {
	o.Containers = &v
}

// GetCveMetadata returns the CveMetadata field value if set, zero value otherwise.
func (o *CreateCveRecordRejectionResponseCreated) GetCveMetadata() GetCveRecordResponseCveMetadata {
	if o == nil || IsNil(o.CveMetadata) {
		var ret GetCveRecordResponseCveMetadata
		return ret
	}
	return *o.CveMetadata
}

// GetCveMetadataOk returns a tuple with the CveMetadata field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordRejectionResponseCreated) GetCveMetadataOk() (*GetCveRecordResponseCveMetadata, bool) {
	if o == nil || IsNil(o.CveMetadata) {
		return nil, false
	}
	return o.CveMetadata, true
}

// HasCveMetadata returns a boolean if a field has been set.
func (o *CreateCveRecordRejectionResponseCreated) HasCveMetadata() bool {
	if o != nil && !IsNil(o.CveMetadata) {
		return true
	}

	return false
}

// SetCveMetadata gets a reference to the given GetCveRecordResponseCveMetadata and assigns it to the CveMetadata field.
func (o *CreateCveRecordRejectionResponseCreated) SetCveMetadata(v GetCveRecordResponseCveMetadata) {
	o.CveMetadata = &v
}

// GetDataType returns the DataType field value if set, zero value otherwise.
func (o *CreateCveRecordRejectionResponseCreated) GetDataType() string {
	if o == nil || IsNil(o.DataType) {
		var ret string
		return ret
	}
	return *o.DataType
}

// GetDataTypeOk returns a tuple with the DataType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordRejectionResponseCreated) GetDataTypeOk() (*string, bool) {
	if o == nil || IsNil(o.DataType) {
		return nil, false
	}
	return o.DataType, true
}

// HasDataType returns a boolean if a field has been set.
func (o *CreateCveRecordRejectionResponseCreated) HasDataType() bool {
	if o != nil && !IsNil(o.DataType) {
		return true
	}

	return false
}

// SetDataType gets a reference to the given string and assigns it to the DataType field.
func (o *CreateCveRecordRejectionResponseCreated) SetDataType(v string) {
	o.DataType = &v
}

// GetDataVersion returns the DataVersion field value if set, zero value otherwise.
func (o *CreateCveRecordRejectionResponseCreated) GetDataVersion() string {
	if o == nil || IsNil(o.DataVersion) {
		var ret string
		return ret
	}
	return *o.DataVersion
}

// GetDataVersionOk returns a tuple with the DataVersion field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordRejectionResponseCreated) GetDataVersionOk() (*string, bool) {
	if o == nil || IsNil(o.DataVersion) {
		return nil, false
	}
	return o.DataVersion, true
}

// HasDataVersion returns a boolean if a field has been set.
func (o *CreateCveRecordRejectionResponseCreated) HasDataVersion() bool {
	if o != nil && !IsNil(o.DataVersion) {
		return true
	}

	return false
}

// SetDataVersion gets a reference to the given string and assigns it to the DataVersion field.
func (o *CreateCveRecordRejectionResponseCreated) SetDataVersion(v string) {
	o.DataVersion = &v
}

func (o CreateCveRecordRejectionResponseCreated) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateCveRecordRejectionResponseCreated) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Containers) {
		toSerialize["containers"] = o.Containers
	}
	if !IsNil(o.CveMetadata) {
		toSerialize["cveMetadata"] = o.CveMetadata
	}
	if !IsNil(o.DataType) {
		toSerialize["dataType"] = o.DataType
	}
	if !IsNil(o.DataVersion) {
		toSerialize["dataVersion"] = o.DataVersion
	}
	return toSerialize, nil
}

type NullableCreateCveRecordRejectionResponseCreated struct {
	value *CreateCveRecordRejectionResponseCreated
	isSet bool
}

func (v NullableCreateCveRecordRejectionResponseCreated) Get() *CreateCveRecordRejectionResponseCreated {
	return v.value
}

func (v *NullableCreateCveRecordRejectionResponseCreated) Set(val *CreateCveRecordRejectionResponseCreated) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateCveRecordRejectionResponseCreated) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateCveRecordRejectionResponseCreated) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateCveRecordRejectionResponseCreated(val *CreateCveRecordRejectionResponseCreated) *NullableCreateCveRecordRejectionResponseCreated {
	return &NullableCreateCveRecordRejectionResponseCreated{value: val, isSet: true}
}

func (v NullableCreateCveRecordRejectionResponseCreated) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateCveRecordRejectionResponseCreated) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


