/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the CreateCveRecordResponseCreatedContainersCna type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateCveRecordResponseCreatedContainersCna{}

// CreateCveRecordResponseCreatedContainersCna struct for CreateCveRecordResponseCreatedContainersCna
type CreateCveRecordResponseCreatedContainersCna struct {
	Affected *CreateFullCveRecordRequestContainersCnaAffectedInner `json:"affected,omitempty"`
	Descriptions []GetCveRecordResponseContainersCnaDescriptionsInner `json:"descriptions,omitempty"`
	ProblemTypes []GetCveRecordResponseContainersCnaProblemTypesInner `json:"problemTypes,omitempty"`
	ProviderMetadata *CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata `json:"providerMetadata,omitempty"`
	References []GetCveRecordResponseContainersCnaReferencesInner `json:"references,omitempty"`
}

// NewCreateCveRecordResponseCreatedContainersCna instantiates a new CreateCveRecordResponseCreatedContainersCna object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateCveRecordResponseCreatedContainersCna() *CreateCveRecordResponseCreatedContainersCna {
	this := CreateCveRecordResponseCreatedContainersCna{}
	return &this
}

// NewCreateCveRecordResponseCreatedContainersCnaWithDefaults instantiates a new CreateCveRecordResponseCreatedContainersCna object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateCveRecordResponseCreatedContainersCnaWithDefaults() *CreateCveRecordResponseCreatedContainersCna {
	this := CreateCveRecordResponseCreatedContainersCna{}
	return &this
}

// GetAffected returns the Affected field value if set, zero value otherwise.
func (o *CreateCveRecordResponseCreatedContainersCna) GetAffected() CreateFullCveRecordRequestContainersCnaAffectedInner {
	if o == nil || IsNil(o.Affected) {
		var ret CreateFullCveRecordRequestContainersCnaAffectedInner
		return ret
	}
	return *o.Affected
}

// GetAffectedOk returns a tuple with the Affected field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) GetAffectedOk() (*CreateFullCveRecordRequestContainersCnaAffectedInner, bool) {
	if o == nil || IsNil(o.Affected) {
		return nil, false
	}
	return o.Affected, true
}

// HasAffected returns a boolean if a field has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) HasAffected() bool {
	if o != nil && !IsNil(o.Affected) {
		return true
	}

	return false
}

// SetAffected gets a reference to the given CreateFullCveRecordRequestContainersCnaAffectedInner and assigns it to the Affected field.
func (o *CreateCveRecordResponseCreatedContainersCna) SetAffected(v CreateFullCveRecordRequestContainersCnaAffectedInner) {
	o.Affected = &v
}

// GetDescriptions returns the Descriptions field value if set, zero value otherwise.
func (o *CreateCveRecordResponseCreatedContainersCna) GetDescriptions() []GetCveRecordResponseContainersCnaDescriptionsInner {
	if o == nil || IsNil(o.Descriptions) {
		var ret []GetCveRecordResponseContainersCnaDescriptionsInner
		return ret
	}
	return o.Descriptions
}

// GetDescriptionsOk returns a tuple with the Descriptions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) GetDescriptionsOk() ([]GetCveRecordResponseContainersCnaDescriptionsInner, bool) {
	if o == nil || IsNil(o.Descriptions) {
		return nil, false
	}
	return o.Descriptions, true
}

// HasDescriptions returns a boolean if a field has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) HasDescriptions() bool {
	if o != nil && !IsNil(o.Descriptions) {
		return true
	}

	return false
}

// SetDescriptions gets a reference to the given []GetCveRecordResponseContainersCnaDescriptionsInner and assigns it to the Descriptions field.
func (o *CreateCveRecordResponseCreatedContainersCna) SetDescriptions(v []GetCveRecordResponseContainersCnaDescriptionsInner) {
	o.Descriptions = v
}

// GetProblemTypes returns the ProblemTypes field value if set, zero value otherwise.
func (o *CreateCveRecordResponseCreatedContainersCna) GetProblemTypes() []GetCveRecordResponseContainersCnaProblemTypesInner {
	if o == nil || IsNil(o.ProblemTypes) {
		var ret []GetCveRecordResponseContainersCnaProblemTypesInner
		return ret
	}
	return o.ProblemTypes
}

// GetProblemTypesOk returns a tuple with the ProblemTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) GetProblemTypesOk() ([]GetCveRecordResponseContainersCnaProblemTypesInner, bool) {
	if o == nil || IsNil(o.ProblemTypes) {
		return nil, false
	}
	return o.ProblemTypes, true
}

// HasProblemTypes returns a boolean if a field has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) HasProblemTypes() bool {
	if o != nil && !IsNil(o.ProblemTypes) {
		return true
	}

	return false
}

// SetProblemTypes gets a reference to the given []GetCveRecordResponseContainersCnaProblemTypesInner and assigns it to the ProblemTypes field.
func (o *CreateCveRecordResponseCreatedContainersCna) SetProblemTypes(v []GetCveRecordResponseContainersCnaProblemTypesInner) {
	o.ProblemTypes = v
}

// GetProviderMetadata returns the ProviderMetadata field value if set, zero value otherwise.
func (o *CreateCveRecordResponseCreatedContainersCna) GetProviderMetadata() CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata {
	if o == nil || IsNil(o.ProviderMetadata) {
		var ret CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata
		return ret
	}
	return *o.ProviderMetadata
}

// GetProviderMetadataOk returns a tuple with the ProviderMetadata field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) GetProviderMetadataOk() (*CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata, bool) {
	if o == nil || IsNil(o.ProviderMetadata) {
		return nil, false
	}
	return o.ProviderMetadata, true
}

// HasProviderMetadata returns a boolean if a field has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) HasProviderMetadata() bool {
	if o != nil && !IsNil(o.ProviderMetadata) {
		return true
	}

	return false
}

// SetProviderMetadata gets a reference to the given CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata and assigns it to the ProviderMetadata field.
func (o *CreateCveRecordResponseCreatedContainersCna) SetProviderMetadata(v CreateCveRecordRejectionResponseCreatedContainersCnaProviderMetadata) {
	o.ProviderMetadata = &v
}

// GetReferences returns the References field value if set, zero value otherwise.
func (o *CreateCveRecordResponseCreatedContainersCna) GetReferences() []GetCveRecordResponseContainersCnaReferencesInner {
	if o == nil || IsNil(o.References) {
		var ret []GetCveRecordResponseContainersCnaReferencesInner
		return ret
	}
	return o.References
}

// GetReferencesOk returns a tuple with the References field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) GetReferencesOk() ([]GetCveRecordResponseContainersCnaReferencesInner, bool) {
	if o == nil || IsNil(o.References) {
		return nil, false
	}
	return o.References, true
}

// HasReferences returns a boolean if a field has been set.
func (o *CreateCveRecordResponseCreatedContainersCna) HasReferences() bool {
	if o != nil && !IsNil(o.References) {
		return true
	}

	return false
}

// SetReferences gets a reference to the given []GetCveRecordResponseContainersCnaReferencesInner and assigns it to the References field.
func (o *CreateCveRecordResponseCreatedContainersCna) SetReferences(v []GetCveRecordResponseContainersCnaReferencesInner) {
	o.References = v
}

func (o CreateCveRecordResponseCreatedContainersCna) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateCveRecordResponseCreatedContainersCna) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Affected) {
		toSerialize["affected"] = o.Affected
	}
	if !IsNil(o.Descriptions) {
		toSerialize["descriptions"] = o.Descriptions
	}
	if !IsNil(o.ProblemTypes) {
		toSerialize["problemTypes"] = o.ProblemTypes
	}
	if !IsNil(o.ProviderMetadata) {
		toSerialize["providerMetadata"] = o.ProviderMetadata
	}
	if !IsNil(o.References) {
		toSerialize["references"] = o.References
	}
	return toSerialize, nil
}

type NullableCreateCveRecordResponseCreatedContainersCna struct {
	value *CreateCveRecordResponseCreatedContainersCna
	isSet bool
}

func (v NullableCreateCveRecordResponseCreatedContainersCna) Get() *CreateCveRecordResponseCreatedContainersCna {
	return v.value
}

func (v *NullableCreateCveRecordResponseCreatedContainersCna) Set(val *CreateCveRecordResponseCreatedContainersCna) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateCveRecordResponseCreatedContainersCna) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateCveRecordResponseCreatedContainersCna) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateCveRecordResponseCreatedContainersCna(val *CreateCveRecordResponseCreatedContainersCna) *NullableCreateCveRecordResponseCreatedContainersCna {
	return &NullableCreateCveRecordResponseCreatedContainersCna{value: val, isSet: true}
}

func (v NullableCreateCveRecordResponseCreatedContainersCna) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateCveRecordResponseCreatedContainersCna) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


