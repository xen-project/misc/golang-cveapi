/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the GetCveRecordResponseContainers type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetCveRecordResponseContainers{}

// GetCveRecordResponseContainers struct for GetCveRecordResponseContainers
type GetCveRecordResponseContainers struct {
	Cna *GetCveRecordResponseContainersCna `json:"cna,omitempty"`
}

// NewGetCveRecordResponseContainers instantiates a new GetCveRecordResponseContainers object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetCveRecordResponseContainers() *GetCveRecordResponseContainers {
	this := GetCveRecordResponseContainers{}
	return &this
}

// NewGetCveRecordResponseContainersWithDefaults instantiates a new GetCveRecordResponseContainers object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetCveRecordResponseContainersWithDefaults() *GetCveRecordResponseContainers {
	this := GetCveRecordResponseContainers{}
	return &this
}

// GetCna returns the Cna field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainers) GetCna() GetCveRecordResponseContainersCna {
	if o == nil || IsNil(o.Cna) {
		var ret GetCveRecordResponseContainersCna
		return ret
	}
	return *o.Cna
}

// GetCnaOk returns a tuple with the Cna field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainers) GetCnaOk() (*GetCveRecordResponseContainersCna, bool) {
	if o == nil || IsNil(o.Cna) {
		return nil, false
	}
	return o.Cna, true
}

// HasCna returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainers) HasCna() bool {
	if o != nil && !IsNil(o.Cna) {
		return true
	}

	return false
}

// SetCna gets a reference to the given GetCveRecordResponseContainersCna and assigns it to the Cna field.
func (o *GetCveRecordResponseContainers) SetCna(v GetCveRecordResponseContainersCna) {
	o.Cna = &v
}

func (o GetCveRecordResponseContainers) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetCveRecordResponseContainers) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Cna) {
		toSerialize["cna"] = o.Cna
	}
	return toSerialize, nil
}

type NullableGetCveRecordResponseContainers struct {
	value *GetCveRecordResponseContainers
	isSet bool
}

func (v NullableGetCveRecordResponseContainers) Get() *GetCveRecordResponseContainers {
	return v.value
}

func (v *NullableGetCveRecordResponseContainers) Set(val *GetCveRecordResponseContainers) {
	v.value = val
	v.isSet = true
}

func (v NullableGetCveRecordResponseContainers) IsSet() bool {
	return v.isSet
}

func (v *NullableGetCveRecordResponseContainers) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetCveRecordResponseContainers(val *GetCveRecordResponseContainers) *NullableGetCveRecordResponseContainers {
	return &NullableGetCveRecordResponseContainers{value: val, isSet: true}
}

func (v NullableGetCveRecordResponseContainers) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetCveRecordResponseContainers) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


