/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the GetCveRecordResponseContainersCnaDescriptionsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetCveRecordResponseContainersCnaDescriptionsInner{}

// GetCveRecordResponseContainersCnaDescriptionsInner struct for GetCveRecordResponseContainersCnaDescriptionsInner
type GetCveRecordResponseContainersCnaDescriptionsInner struct {
	Lang *string `json:"lang,omitempty"`
	Value *string `json:"value,omitempty"`
}

// NewGetCveRecordResponseContainersCnaDescriptionsInner instantiates a new GetCveRecordResponseContainersCnaDescriptionsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetCveRecordResponseContainersCnaDescriptionsInner() *GetCveRecordResponseContainersCnaDescriptionsInner {
	this := GetCveRecordResponseContainersCnaDescriptionsInner{}
	return &this
}

// NewGetCveRecordResponseContainersCnaDescriptionsInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaDescriptionsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetCveRecordResponseContainersCnaDescriptionsInnerWithDefaults() *GetCveRecordResponseContainersCnaDescriptionsInner {
	this := GetCveRecordResponseContainersCnaDescriptionsInner{}
	return &this
}

// GetLang returns the Lang field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) GetLang() string {
	if o == nil || IsNil(o.Lang) {
		var ret string
		return ret
	}
	return *o.Lang
}

// GetLangOk returns a tuple with the Lang field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) GetLangOk() (*string, bool) {
	if o == nil || IsNil(o.Lang) {
		return nil, false
	}
	return o.Lang, true
}

// HasLang returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) HasLang() bool {
	if o != nil && !IsNil(o.Lang) {
		return true
	}

	return false
}

// SetLang gets a reference to the given string and assigns it to the Lang field.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) SetLang(v string) {
	o.Lang = &v
}

// GetValue returns the Value field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) GetValue() string {
	if o == nil || IsNil(o.Value) {
		var ret string
		return ret
	}
	return *o.Value
}

// GetValueOk returns a tuple with the Value field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) GetValueOk() (*string, bool) {
	if o == nil || IsNil(o.Value) {
		return nil, false
	}
	return o.Value, true
}

// HasValue returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) HasValue() bool {
	if o != nil && !IsNil(o.Value) {
		return true
	}

	return false
}

// SetValue gets a reference to the given string and assigns it to the Value field.
func (o *GetCveRecordResponseContainersCnaDescriptionsInner) SetValue(v string) {
	o.Value = &v
}

func (o GetCveRecordResponseContainersCnaDescriptionsInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetCveRecordResponseContainersCnaDescriptionsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Lang) {
		toSerialize["lang"] = o.Lang
	}
	if !IsNil(o.Value) {
		toSerialize["value"] = o.Value
	}
	return toSerialize, nil
}

type NullableGetCveRecordResponseContainersCnaDescriptionsInner struct {
	value *GetCveRecordResponseContainersCnaDescriptionsInner
	isSet bool
}

func (v NullableGetCveRecordResponseContainersCnaDescriptionsInner) Get() *GetCveRecordResponseContainersCnaDescriptionsInner {
	return v.value
}

func (v *NullableGetCveRecordResponseContainersCnaDescriptionsInner) Set(val *GetCveRecordResponseContainersCnaDescriptionsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableGetCveRecordResponseContainersCnaDescriptionsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableGetCveRecordResponseContainersCnaDescriptionsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetCveRecordResponseContainersCnaDescriptionsInner(val *GetCveRecordResponseContainersCnaDescriptionsInner) *NullableGetCveRecordResponseContainersCnaDescriptionsInner {
	return &NullableGetCveRecordResponseContainersCnaDescriptionsInner{value: val, isSet: true}
}

func (v NullableGetCveRecordResponseContainersCnaDescriptionsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetCveRecordResponseContainersCnaDescriptionsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


