/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the GetCveRecordResponseContainersCnaReferencesInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetCveRecordResponseContainersCnaReferencesInner{}

// GetCveRecordResponseContainersCnaReferencesInner struct for GetCveRecordResponseContainersCnaReferencesInner
type GetCveRecordResponseContainersCnaReferencesInner struct {
	Name *string `json:"name,omitempty"`
	Tags []string `json:"tags,omitempty"`
	Url *string `json:"url,omitempty"`
}

// NewGetCveRecordResponseContainersCnaReferencesInner instantiates a new GetCveRecordResponseContainersCnaReferencesInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetCveRecordResponseContainersCnaReferencesInner() *GetCveRecordResponseContainersCnaReferencesInner {
	this := GetCveRecordResponseContainersCnaReferencesInner{}
	return &this
}

// NewGetCveRecordResponseContainersCnaReferencesInnerWithDefaults instantiates a new GetCveRecordResponseContainersCnaReferencesInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetCveRecordResponseContainersCnaReferencesInnerWithDefaults() *GetCveRecordResponseContainersCnaReferencesInner {
	this := GetCveRecordResponseContainersCnaReferencesInner{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *GetCveRecordResponseContainersCnaReferencesInner) SetName(v string) {
	o.Name = &v
}

// GetTags returns the Tags field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetTags() []string {
	if o == nil || IsNil(o.Tags) {
		var ret []string
		return ret
	}
	return o.Tags
}

// GetTagsOk returns a tuple with the Tags field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetTagsOk() ([]string, bool) {
	if o == nil || IsNil(o.Tags) {
		return nil, false
	}
	return o.Tags, true
}

// HasTags returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) HasTags() bool {
	if o != nil && !IsNil(o.Tags) {
		return true
	}

	return false
}

// SetTags gets a reference to the given []string and assigns it to the Tags field.
func (o *GetCveRecordResponseContainersCnaReferencesInner) SetTags(v []string) {
	o.Tags = v
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetUrl() string {
	if o == nil || IsNil(o.Url) {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) GetUrlOk() (*string, bool) {
	if o == nil || IsNil(o.Url) {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *GetCveRecordResponseContainersCnaReferencesInner) HasUrl() bool {
	if o != nil && !IsNil(o.Url) {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *GetCveRecordResponseContainersCnaReferencesInner) SetUrl(v string) {
	o.Url = &v
}

func (o GetCveRecordResponseContainersCnaReferencesInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetCveRecordResponseContainersCnaReferencesInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.Tags) {
		toSerialize["tags"] = o.Tags
	}
	if !IsNil(o.Url) {
		toSerialize["url"] = o.Url
	}
	return toSerialize, nil
}

type NullableGetCveRecordResponseContainersCnaReferencesInner struct {
	value *GetCveRecordResponseContainersCnaReferencesInner
	isSet bool
}

func (v NullableGetCveRecordResponseContainersCnaReferencesInner) Get() *GetCveRecordResponseContainersCnaReferencesInner {
	return v.value
}

func (v *NullableGetCveRecordResponseContainersCnaReferencesInner) Set(val *GetCveRecordResponseContainersCnaReferencesInner) {
	v.value = val
	v.isSet = true
}

func (v NullableGetCveRecordResponseContainersCnaReferencesInner) IsSet() bool {
	return v.isSet
}

func (v *NullableGetCveRecordResponseContainersCnaReferencesInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetCveRecordResponseContainersCnaReferencesInner(val *GetCveRecordResponseContainersCnaReferencesInner) *NullableGetCveRecordResponseContainersCnaReferencesInner {
	return &NullableGetCveRecordResponseContainersCnaReferencesInner{value: val, isSet: true}
}

func (v NullableGetCveRecordResponseContainersCnaReferencesInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetCveRecordResponseContainersCnaReferencesInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


