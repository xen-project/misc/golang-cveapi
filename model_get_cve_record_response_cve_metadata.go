/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the GetCveRecordResponseCveMetadata type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetCveRecordResponseCveMetadata{}

// GetCveRecordResponseCveMetadata struct for GetCveRecordResponseCveMetadata
type GetCveRecordResponseCveMetadata struct {
	AssignerOrgId *string `json:"assignerOrgId,omitempty"`
	CveId *string `json:"cveId,omitempty"`
	State *string `json:"state,omitempty"`
	AssignerShortName *string `json:"assignerShortName,omitempty"`
	RequesterUserId *string `json:"requesterUserId,omitempty"`
	DateReserved *string `json:"dateReserved,omitempty"`
	DatePublished *string `json:"datePublished,omitempty"`
}

// NewGetCveRecordResponseCveMetadata instantiates a new GetCveRecordResponseCveMetadata object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetCveRecordResponseCveMetadata() *GetCveRecordResponseCveMetadata {
	this := GetCveRecordResponseCveMetadata{}
	return &this
}

// NewGetCveRecordResponseCveMetadataWithDefaults instantiates a new GetCveRecordResponseCveMetadata object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetCveRecordResponseCveMetadataWithDefaults() *GetCveRecordResponseCveMetadata {
	this := GetCveRecordResponseCveMetadata{}
	return &this
}

// GetAssignerOrgId returns the AssignerOrgId field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetAssignerOrgId() string {
	if o == nil || IsNil(o.AssignerOrgId) {
		var ret string
		return ret
	}
	return *o.AssignerOrgId
}

// GetAssignerOrgIdOk returns a tuple with the AssignerOrgId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetAssignerOrgIdOk() (*string, bool) {
	if o == nil || IsNil(o.AssignerOrgId) {
		return nil, false
	}
	return o.AssignerOrgId, true
}

// HasAssignerOrgId returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasAssignerOrgId() bool {
	if o != nil && !IsNil(o.AssignerOrgId) {
		return true
	}

	return false
}

// SetAssignerOrgId gets a reference to the given string and assigns it to the AssignerOrgId field.
func (o *GetCveRecordResponseCveMetadata) SetAssignerOrgId(v string) {
	o.AssignerOrgId = &v
}

// GetCveId returns the CveId field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetCveId() string {
	if o == nil || IsNil(o.CveId) {
		var ret string
		return ret
	}
	return *o.CveId
}

// GetCveIdOk returns a tuple with the CveId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetCveIdOk() (*string, bool) {
	if o == nil || IsNil(o.CveId) {
		return nil, false
	}
	return o.CveId, true
}

// HasCveId returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasCveId() bool {
	if o != nil && !IsNil(o.CveId) {
		return true
	}

	return false
}

// SetCveId gets a reference to the given string and assigns it to the CveId field.
func (o *GetCveRecordResponseCveMetadata) SetCveId(v string) {
	o.CveId = &v
}

// GetState returns the State field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetState() string {
	if o == nil || IsNil(o.State) {
		var ret string
		return ret
	}
	return *o.State
}

// GetStateOk returns a tuple with the State field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetStateOk() (*string, bool) {
	if o == nil || IsNil(o.State) {
		return nil, false
	}
	return o.State, true
}

// HasState returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasState() bool {
	if o != nil && !IsNil(o.State) {
		return true
	}

	return false
}

// SetState gets a reference to the given string and assigns it to the State field.
func (o *GetCveRecordResponseCveMetadata) SetState(v string) {
	o.State = &v
}

// GetAssignerShortName returns the AssignerShortName field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetAssignerShortName() string {
	if o == nil || IsNil(o.AssignerShortName) {
		var ret string
		return ret
	}
	return *o.AssignerShortName
}

// GetAssignerShortNameOk returns a tuple with the AssignerShortName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetAssignerShortNameOk() (*string, bool) {
	if o == nil || IsNil(o.AssignerShortName) {
		return nil, false
	}
	return o.AssignerShortName, true
}

// HasAssignerShortName returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasAssignerShortName() bool {
	if o != nil && !IsNil(o.AssignerShortName) {
		return true
	}

	return false
}

// SetAssignerShortName gets a reference to the given string and assigns it to the AssignerShortName field.
func (o *GetCveRecordResponseCveMetadata) SetAssignerShortName(v string) {
	o.AssignerShortName = &v
}

// GetRequesterUserId returns the RequesterUserId field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetRequesterUserId() string {
	if o == nil || IsNil(o.RequesterUserId) {
		var ret string
		return ret
	}
	return *o.RequesterUserId
}

// GetRequesterUserIdOk returns a tuple with the RequesterUserId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetRequesterUserIdOk() (*string, bool) {
	if o == nil || IsNil(o.RequesterUserId) {
		return nil, false
	}
	return o.RequesterUserId, true
}

// HasRequesterUserId returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasRequesterUserId() bool {
	if o != nil && !IsNil(o.RequesterUserId) {
		return true
	}

	return false
}

// SetRequesterUserId gets a reference to the given string and assigns it to the RequesterUserId field.
func (o *GetCveRecordResponseCveMetadata) SetRequesterUserId(v string) {
	o.RequesterUserId = &v
}

// GetDateReserved returns the DateReserved field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetDateReserved() string {
	if o == nil || IsNil(o.DateReserved) {
		var ret string
		return ret
	}
	return *o.DateReserved
}

// GetDateReservedOk returns a tuple with the DateReserved field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetDateReservedOk() (*string, bool) {
	if o == nil || IsNil(o.DateReserved) {
		return nil, false
	}
	return o.DateReserved, true
}

// HasDateReserved returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasDateReserved() bool {
	if o != nil && !IsNil(o.DateReserved) {
		return true
	}

	return false
}

// SetDateReserved gets a reference to the given string and assigns it to the DateReserved field.
func (o *GetCveRecordResponseCveMetadata) SetDateReserved(v string) {
	o.DateReserved = &v
}

// GetDatePublished returns the DatePublished field value if set, zero value otherwise.
func (o *GetCveRecordResponseCveMetadata) GetDatePublished() string {
	if o == nil || IsNil(o.DatePublished) {
		var ret string
		return ret
	}
	return *o.DatePublished
}

// GetDatePublishedOk returns a tuple with the DatePublished field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetCveRecordResponseCveMetadata) GetDatePublishedOk() (*string, bool) {
	if o == nil || IsNil(o.DatePublished) {
		return nil, false
	}
	return o.DatePublished, true
}

// HasDatePublished returns a boolean if a field has been set.
func (o *GetCveRecordResponseCveMetadata) HasDatePublished() bool {
	if o != nil && !IsNil(o.DatePublished) {
		return true
	}

	return false
}

// SetDatePublished gets a reference to the given string and assigns it to the DatePublished field.
func (o *GetCveRecordResponseCveMetadata) SetDatePublished(v string) {
	o.DatePublished = &v
}

func (o GetCveRecordResponseCveMetadata) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetCveRecordResponseCveMetadata) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.AssignerOrgId) {
		toSerialize["assignerOrgId"] = o.AssignerOrgId
	}
	if !IsNil(o.CveId) {
		toSerialize["cveId"] = o.CveId
	}
	if !IsNil(o.State) {
		toSerialize["state"] = o.State
	}
	if !IsNil(o.AssignerShortName) {
		toSerialize["assignerShortName"] = o.AssignerShortName
	}
	if !IsNil(o.RequesterUserId) {
		toSerialize["requesterUserId"] = o.RequesterUserId
	}
	if !IsNil(o.DateReserved) {
		toSerialize["dateReserved"] = o.DateReserved
	}
	if !IsNil(o.DatePublished) {
		toSerialize["datePublished"] = o.DatePublished
	}
	return toSerialize, nil
}

type NullableGetCveRecordResponseCveMetadata struct {
	value *GetCveRecordResponseCveMetadata
	isSet bool
}

func (v NullableGetCveRecordResponseCveMetadata) Get() *GetCveRecordResponseCveMetadata {
	return v.value
}

func (v *NullableGetCveRecordResponseCveMetadata) Set(val *GetCveRecordResponseCveMetadata) {
	v.value = val
	v.isSet = true
}

func (v NullableGetCveRecordResponseCveMetadata) IsSet() bool {
	return v.isSet
}

func (v *NullableGetCveRecordResponseCveMetadata) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetCveRecordResponseCveMetadata(val *GetCveRecordResponseCveMetadata) *NullableGetCveRecordResponseCveMetadata {
	return &NullableGetCveRecordResponseCveMetadata{value: val, isSet: true}
}

func (v NullableGetCveRecordResponseCveMetadata) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetCveRecordResponseCveMetadata) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


