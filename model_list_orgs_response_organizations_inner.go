/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the ListOrgsResponseOrganizationsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ListOrgsResponseOrganizationsInner{}

// ListOrgsResponseOrganizationsInner struct for ListOrgsResponseOrganizationsInner
type ListOrgsResponseOrganizationsInner struct {
	// The name of the organization
	Name *string `json:"name,omitempty"`
	// The short name of the organization
	ShortName *string `json:"short_name,omitempty"`
	// The identifier of the organization
	UUID *string `json:"UUID,omitempty"`
	Policies *ListOrgsResponseOrganizationsInnerPolicies `json:"policies,omitempty"`
	Authority *ListOrgsResponseOrganizationsInnerAuthority `json:"authority,omitempty"`
	Time *ListOrgsResponseOrganizationsInnerTime `json:"time,omitempty"`
}

// NewListOrgsResponseOrganizationsInner instantiates a new ListOrgsResponseOrganizationsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewListOrgsResponseOrganizationsInner() *ListOrgsResponseOrganizationsInner {
	this := ListOrgsResponseOrganizationsInner{}
	return &this
}

// NewListOrgsResponseOrganizationsInnerWithDefaults instantiates a new ListOrgsResponseOrganizationsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewListOrgsResponseOrganizationsInnerWithDefaults() *ListOrgsResponseOrganizationsInner {
	this := ListOrgsResponseOrganizationsInner{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ListOrgsResponseOrganizationsInner) SetName(v string) {
	o.Name = &v
}

// GetShortName returns the ShortName field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetShortName() string {
	if o == nil || IsNil(o.ShortName) {
		var ret string
		return ret
	}
	return *o.ShortName
}

// GetShortNameOk returns a tuple with the ShortName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetShortNameOk() (*string, bool) {
	if o == nil || IsNil(o.ShortName) {
		return nil, false
	}
	return o.ShortName, true
}

// HasShortName returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasShortName() bool {
	if o != nil && !IsNil(o.ShortName) {
		return true
	}

	return false
}

// SetShortName gets a reference to the given string and assigns it to the ShortName field.
func (o *ListOrgsResponseOrganizationsInner) SetShortName(v string) {
	o.ShortName = &v
}

// GetUUID returns the UUID field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetUUID() string {
	if o == nil || IsNil(o.UUID) {
		var ret string
		return ret
	}
	return *o.UUID
}

// GetUUIDOk returns a tuple with the UUID field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetUUIDOk() (*string, bool) {
	if o == nil || IsNil(o.UUID) {
		return nil, false
	}
	return o.UUID, true
}

// HasUUID returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasUUID() bool {
	if o != nil && !IsNil(o.UUID) {
		return true
	}

	return false
}

// SetUUID gets a reference to the given string and assigns it to the UUID field.
func (o *ListOrgsResponseOrganizationsInner) SetUUID(v string) {
	o.UUID = &v
}

// GetPolicies returns the Policies field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetPolicies() ListOrgsResponseOrganizationsInnerPolicies {
	if o == nil || IsNil(o.Policies) {
		var ret ListOrgsResponseOrganizationsInnerPolicies
		return ret
	}
	return *o.Policies
}

// GetPoliciesOk returns a tuple with the Policies field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetPoliciesOk() (*ListOrgsResponseOrganizationsInnerPolicies, bool) {
	if o == nil || IsNil(o.Policies) {
		return nil, false
	}
	return o.Policies, true
}

// HasPolicies returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasPolicies() bool {
	if o != nil && !IsNil(o.Policies) {
		return true
	}

	return false
}

// SetPolicies gets a reference to the given ListOrgsResponseOrganizationsInnerPolicies and assigns it to the Policies field.
func (o *ListOrgsResponseOrganizationsInner) SetPolicies(v ListOrgsResponseOrganizationsInnerPolicies) {
	o.Policies = &v
}

// GetAuthority returns the Authority field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetAuthority() ListOrgsResponseOrganizationsInnerAuthority {
	if o == nil || IsNil(o.Authority) {
		var ret ListOrgsResponseOrganizationsInnerAuthority
		return ret
	}
	return *o.Authority
}

// GetAuthorityOk returns a tuple with the Authority field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetAuthorityOk() (*ListOrgsResponseOrganizationsInnerAuthority, bool) {
	if o == nil || IsNil(o.Authority) {
		return nil, false
	}
	return o.Authority, true
}

// HasAuthority returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasAuthority() bool {
	if o != nil && !IsNil(o.Authority) {
		return true
	}

	return false
}

// SetAuthority gets a reference to the given ListOrgsResponseOrganizationsInnerAuthority and assigns it to the Authority field.
func (o *ListOrgsResponseOrganizationsInner) SetAuthority(v ListOrgsResponseOrganizationsInnerAuthority) {
	o.Authority = &v
}

// GetTime returns the Time field value if set, zero value otherwise.
func (o *ListOrgsResponseOrganizationsInner) GetTime() ListOrgsResponseOrganizationsInnerTime {
	if o == nil || IsNil(o.Time) {
		var ret ListOrgsResponseOrganizationsInnerTime
		return ret
	}
	return *o.Time
}

// GetTimeOk returns a tuple with the Time field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOrgsResponseOrganizationsInner) GetTimeOk() (*ListOrgsResponseOrganizationsInnerTime, bool) {
	if o == nil || IsNil(o.Time) {
		return nil, false
	}
	return o.Time, true
}

// HasTime returns a boolean if a field has been set.
func (o *ListOrgsResponseOrganizationsInner) HasTime() bool {
	if o != nil && !IsNil(o.Time) {
		return true
	}

	return false
}

// SetTime gets a reference to the given ListOrgsResponseOrganizationsInnerTime and assigns it to the Time field.
func (o *ListOrgsResponseOrganizationsInner) SetTime(v ListOrgsResponseOrganizationsInnerTime) {
	o.Time = &v
}

func (o ListOrgsResponseOrganizationsInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ListOrgsResponseOrganizationsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.ShortName) {
		toSerialize["short_name"] = o.ShortName
	}
	if !IsNil(o.UUID) {
		toSerialize["UUID"] = o.UUID
	}
	if !IsNil(o.Policies) {
		toSerialize["policies"] = o.Policies
	}
	if !IsNil(o.Authority) {
		toSerialize["authority"] = o.Authority
	}
	if !IsNil(o.Time) {
		toSerialize["time"] = o.Time
	}
	return toSerialize, nil
}

type NullableListOrgsResponseOrganizationsInner struct {
	value *ListOrgsResponseOrganizationsInner
	isSet bool
}

func (v NullableListOrgsResponseOrganizationsInner) Get() *ListOrgsResponseOrganizationsInner {
	return v.value
}

func (v *NullableListOrgsResponseOrganizationsInner) Set(val *ListOrgsResponseOrganizationsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableListOrgsResponseOrganizationsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableListOrgsResponseOrganizationsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableListOrgsResponseOrganizationsInner(val *ListOrgsResponseOrganizationsInner) *NullableListOrgsResponseOrganizationsInner {
	return &NullableListOrgsResponseOrganizationsInner{value: val, isSet: true}
}

func (v NullableListOrgsResponseOrganizationsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableListOrgsResponseOrganizationsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


