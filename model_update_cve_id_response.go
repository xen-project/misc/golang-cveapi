/*
CVE Services API

The CVE Services API supports automation tooling for the CVE Program. Credentials are     required for most service endpoints. Representatives of     <a href='https://www.cve.org/ProgramOrganization/CNAs'>CVE Numbering Authorities (CNAs)</a> should     use one of the methods below to obtain credentials:    <ul><li>If your organization already has an Organizational Administrator (OA) account for the CVE     Services, ask your admin for credentials</li>     <li>Contact your Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/Google'>Google</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/INCIBE'>INCIBE</a>,     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/jpcert'>JPCERT/CC</a>, or     <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/redhat'>Red Hat</a>) or     Top-Level Root (<a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/icscert'>CISA ICS</a>     or <a href='https://www.cve.org/PartnerInformation/ListofPartners/partner/mitre'>MITRE</a>) to request credentials     </ul>     <p>CVE data is to be in the JSON 5.0 CVE Record format. Details of the JSON 5.0 schema are     located <a href='https://github.com/CVEProject/cve-schema/tree/master/schema/v5.0' target='_blank'>here</a>.</p>    <a href='https://cveform.mitre.org/' class='link' target='_blank'>Contact the CVE Services team</a>

API version: 2.1.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package cveapi

import (
	"encoding/json"
)

// checks if the UpdateCveIdResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UpdateCveIdResponse{}

// UpdateCveIdResponse struct for UpdateCveIdResponse
type UpdateCveIdResponse struct {
	Message *string `json:"message,omitempty"`
	Updated *ListCveIdsResponseCveIdsInner `json:"updated,omitempty"`
}

// NewUpdateCveIdResponse instantiates a new UpdateCveIdResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUpdateCveIdResponse() *UpdateCveIdResponse {
	this := UpdateCveIdResponse{}
	return &this
}

// NewUpdateCveIdResponseWithDefaults instantiates a new UpdateCveIdResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUpdateCveIdResponseWithDefaults() *UpdateCveIdResponse {
	this := UpdateCveIdResponse{}
	return &this
}

// GetMessage returns the Message field value if set, zero value otherwise.
func (o *UpdateCveIdResponse) GetMessage() string {
	if o == nil || IsNil(o.Message) {
		var ret string
		return ret
	}
	return *o.Message
}

// GetMessageOk returns a tuple with the Message field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateCveIdResponse) GetMessageOk() (*string, bool) {
	if o == nil || IsNil(o.Message) {
		return nil, false
	}
	return o.Message, true
}

// HasMessage returns a boolean if a field has been set.
func (o *UpdateCveIdResponse) HasMessage() bool {
	if o != nil && !IsNil(o.Message) {
		return true
	}

	return false
}

// SetMessage gets a reference to the given string and assigns it to the Message field.
func (o *UpdateCveIdResponse) SetMessage(v string) {
	o.Message = &v
}

// GetUpdated returns the Updated field value if set, zero value otherwise.
func (o *UpdateCveIdResponse) GetUpdated() ListCveIdsResponseCveIdsInner {
	if o == nil || IsNil(o.Updated) {
		var ret ListCveIdsResponseCveIdsInner
		return ret
	}
	return *o.Updated
}

// GetUpdatedOk returns a tuple with the Updated field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateCveIdResponse) GetUpdatedOk() (*ListCveIdsResponseCveIdsInner, bool) {
	if o == nil || IsNil(o.Updated) {
		return nil, false
	}
	return o.Updated, true
}

// HasUpdated returns a boolean if a field has been set.
func (o *UpdateCveIdResponse) HasUpdated() bool {
	if o != nil && !IsNil(o.Updated) {
		return true
	}

	return false
}

// SetUpdated gets a reference to the given ListCveIdsResponseCveIdsInner and assigns it to the Updated field.
func (o *UpdateCveIdResponse) SetUpdated(v ListCveIdsResponseCveIdsInner) {
	o.Updated = &v
}

func (o UpdateCveIdResponse) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UpdateCveIdResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Message) {
		toSerialize["message"] = o.Message
	}
	if !IsNil(o.Updated) {
		toSerialize["updated"] = o.Updated
	}
	return toSerialize, nil
}

type NullableUpdateCveIdResponse struct {
	value *UpdateCveIdResponse
	isSet bool
}

func (v NullableUpdateCveIdResponse) Get() *UpdateCveIdResponse {
	return v.value
}

func (v *NullableUpdateCveIdResponse) Set(val *UpdateCveIdResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableUpdateCveIdResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableUpdateCveIdResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUpdateCveIdResponse(val *UpdateCveIdResponse) *NullableUpdateCveIdResponse {
	return &NullableUpdateCveIdResponse{value: val, isSet: true}
}

func (v NullableUpdateCveIdResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUpdateCveIdResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


